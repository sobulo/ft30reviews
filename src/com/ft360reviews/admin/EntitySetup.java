/**
 * 
 */
package com.ft360reviews.admin;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.logging.Logger;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.ft360reviews.entities.InetCompany;
import com.ft360reviews.entities.InetConstants;
import com.ft360reviews.entities.InetDAO4CommonReads;
import com.ft360reviews.entities.InetDAO4CommonWrites;
import com.ft360reviews.entities.InetEmployee;
import com.ft360reviews.entities.InetMultiAnswers;
import com.ft360reviews.entities.InetPreferences;
import com.ft360reviews.entities.InetQuestions;
import com.ft360reviews.entities.InetReview;
import com.ft360reviews.entities.InetReviewSummary;
import com.ft360reviews.gui.shared.PanelServiceConstants;
import com.ft360reviews.gui.shared.exceptions.DuplicateEntitiesException;
import com.ft360reviews.gui.shared.exceptions.MissingEntitiesException;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.Objectify;
import com.googlecode.objectify.ObjectifyService;


/**
 * @author Segun Razaq Sobulo
 *
 */
public class EntitySetup extends HttpServlet{
	private static final Logger log = Logger.getLogger(EntitySetup.class.getName());
	
	@Override
	public void doGet(HttpServletRequest req, HttpServletResponse res) throws IOException
	{
		ServletOutputStream out = res.getOutputStream();
		res.setContentType("text/html");
		try
		{
			String type = req.getParameter("type");
			if(type == null)
			{
				out.println("<b><font color='red'>Please specify a type</font></b>");
				return;
			}
			out.println("<b>setup starting</b>");
			if(type.equals("countsubmitted"))
			{
				out.println(reviewCounts(true, InetReview.class));
			}
			else if(type.equals("count"))
			{
				out.println(reviewCounts(false, InetReview.class));
			}
			if(type.equals("countsummarysubmitted"))
			{
				out.println(reviewCounts(true, InetReviewSummary.class));
			}
			else if(type.equals("countsummary"))
			{
				out.println(reviewCounts(false, InetReviewSummary.class));
			}			
			else if(type.equals("summary"))
			{
				//out.println("Creating questions ...:<br/>" + createQuestions() + "<br/>");
				//out.println("Created management summary: " + createManagementTeamSummary() + "....<br/>");
			}
			else if(type.equals("check"))
			{
				out.println(checkEmployeeSummarySetup());
			}
			else if(type.equals("modify"))
			{
				out.println(modifySummarizer());
			}
			else if(type.equals("start"))
			{				
				out.println("<b><font color='green'>setup completed ok!</font></b>");
				log.warning("starting entity setup");
				//general entity setup ... company, prefs, emply				
				/*out.println("Creating notices ...: <br/>" +  createNotices() + "<br/>" );
				out.println("Creating company ...: <br/>" +  createCompany() + "<br/>" );
				out.println("Creating preferences ...:<br/>" + createPreferences() + "<br/>");*/
				out.println("Creating employees ...:<br/>" + createEmployees() + "<br/>");
				
				//question setup
				/*out.println("Creating mappings ...:<br/>" + createAnswerMappings() + "<br/>");			
				out.println("Creating questions ...:<br/>" + createQuestions() + "<br/>");*/
				
				//create reviews
				/*out.println("Created general employee reviews: " + createGeneralEmployeeReviews() + "....<br/>");
				
				//delete reviews
				out.println("Created supervisor reviews: " + createSupervisorReviews() + "....<br/>");
				out.println("Created management reviews: " + createManagementTeamReview() + "....<br/>");		
				//out.println("Created self reviews: " + createSelfReviews() + "....<br/>");
				
				//create summaries
				out.println("Created empl summaries: " + createEmployeeSummaries() + "....<br/>");
				//out.println("Created management summary: " + createManagementTeamSummary() + "....<br/>");*/
				out.println("COMPLETED");
				log.warning("setup completed succesfully");
			}
			else
			{
				out.println("Nothing to do");
				//InetDAO4CommonWrites.deleteMultiAnswers("generalchoices");
			}
			
		}
		catch(DuplicateEntitiesException ex)
		{
			out.println("An error occured when creating objects: " + ex.getMessage());
		}
		catch(MissingEntitiesException ex)
		{
			out.println("An error occured when creating objects: " + ex.getMessage());
		}
	}
	
	private String reviewCounts(boolean submitted, Class<? extends InetReview> reviewOrSummary) throws IOException
	{
		Objectify ofy = ObjectifyService.begin();
		List<? extends InetReview> counts = ofy.query(reviewOrSummary).list();
		StringBuilder sb = new StringBuilder("Found " + counts.size() + " results <br/>" + "Reviewer, Reviewee, Type, Last Updated <br/>");
		for(InetReview rev : counts)
		{
			String reviewee = rev.getReviewee() == null? rev.getName() : rev.getReviewee().getName();
			String date = DateFormat.getDateInstance(DateFormat.MEDIUM, Locale.UK).format(rev.getLastUpdated());
			if(submitted && rev.isLocked())
			{			 
				sb.append(rev.getReviewer().getName()).append(",").append(reviewee).append(",").
					append(rev.getQuestionSetKey().getName()).append(",").append(date).append("<br/>");
			}
			else if(!submitted && !rev.isLocked())
			{
				sb.append(rev.getReviewer().getName()).append(",").append(reviewee).append(",").
				append(rev.getQuestionSetKey().getName()).append(",").append(date).append("<br/>");				
			}
		}
		return sb.toString();
	}
	
	private static String createCompany() throws DuplicateEntitiesException
	{
		InetCompany c = InetDAO4CommonWrites.createCompany(SetupConstants.COMPANY_INFO[0], 
							SetupConstants.COMPANY_INFO[1], null, null, null, null);
		log.warning("Created Company: " + c.getKey());
		return "Company: " + c.getCompanyName() + "Accronym: " + c.getAccronym();
	}
	
	private static String createEmployees() throws DuplicateEntitiesException
	{
		log.warning("Attempting to create: " + SetupConstants.EMPLOYEE_NAMES.length + " employees");
		String csvLog = "<b>FNAME, LNAME, LOGIN, PWD</b><br/>";
		int count = 0;
		
		for(String[] employeeInfo : SetupConstants.EMPLOYEE_NAMES)
		{
			String[] fnameParts  = employeeInfo[0].split(" ");
			String calculatedLogin = fnameParts[0].toLowerCase() + "." + employeeInfo[1].toLowerCase();
			
			//TODO ... fix this to check for Fieldco Format
			if(!calculatedLogin.equals(employeeInfo[2]) && !employeeInfo[2].equalsIgnoreCase(calculatedLogin + "@fieldcolimited.com"))
				log.severe("Config file doesn't seem to be setup properly: " + calculatedLogin + " vs " + employeeInfo[2]);
		}
		
		for(String[] employeeInfo : SetupConstants.EMPLOYEE_NAMES)
		{
			String email = null;
			if(employeeInfo[2].endsWith("@fieldcolimited.com"))
				email = employeeInfo[2];
			InetEmployee e = InetDAO4CommonWrites.createEmployee(employeeInfo[0], employeeInfo[1], 
					email, null, employeeInfo[2], employeeInfo[3]);
			csvLog += e.getFname() +"," + e.getLname() + "," + e.getLoginName() + "," + employeeInfo[3] + "<br/>";
		}
		log.warning("Created " + count + " employees");
		return csvLog;
	}
	
	private static String createAnswerMappings() throws DuplicateEntitiesException
	{
		String result = "";
		log.warning("creating answer maping objects");
		HashMap<Integer, String> tempMapping = new HashMap<Integer, String>();
		for(String ansMapId : SetupConstants.ANS_SETUP.keySet())
		{
			tempMapping.clear();
			String[] mapValues = SetupConstants.ANS_SETUP.get(ansMapId);
			int count = 1;
			for(String val : mapValues)
				tempMapping.put(count++, val);
			InetMultiAnswers ansMap = InetDAO4CommonWrites.createMultiAnswers(ansMapId, tempMapping);
			log.warning("Created: " + ansMap);
			result += " [AnsMap: " + ansMap.getKey().getName() + "] ";
		}	
		return result;
	}
	
	private static String createQuestions() throws DuplicateEntitiesException, MissingEntitiesException
	{
		log.warning("Creating objects for questions");
		String result = "";
		HashSet<Key<InetMultiAnswers>> ansKeysCheck = new HashSet<Key<InetMultiAnswers>> ();
		for(int i = 0; i < SetupConstants.QUESTION_SETUP.length; i++)
		{
			HashMap<String, String[]> config = SetupConstants.QUESTION_SETUP[i];
			String ansId = config.get(SetupConstants.SCALARS_KEY)[1];
			Key<InetMultiAnswers> ak = new Key<InetMultiAnswers>(InetMultiAnswers.class, ansId);
			ansKeysCheck.add(ak);
		}
		
		InetDAO4CommonReads.getEntities(ansKeysCheck, ObjectifyService.begin()); //ensure ansmap objs exist
		
		//now lets go create the question objs
		for(int i = 0; i < SetupConstants.QUESTION_SETUP.length; i++)
		{				
			HashMap<String, String[]> config = SetupConstants.QUESTION_SETUP[i];
			String questId = config.get(SetupConstants.SCALARS_KEY)[0];
			String ansId = config.get(SetupConstants.SCALARS_KEY)[1];
			String[] textQs = config.get(SetupConstants.TEXT_KEY);
			String[] choiceQs = config.get(SetupConstants.CHOICE_KEY);	
			HashMap<Integer, String> questionSet = new HashMap<Integer, String>();
			if(choiceQs != null)
				for(int j = 0; j < choiceQs.length; j++)
					questionSet.put(j + 1, choiceQs[j]);
			if(textQs != null)
				for(int j = 0; j < textQs.length; j++)
					questionSet.put(j + PanelServiceConstants.FREE_FORM_MARKER + 1, textQs[j]);
					
			Key<InetMultiAnswers> ak = new Key<InetMultiAnswers>(InetMultiAnswers.class, ansId);
			
			InetQuestions question = InetDAO4CommonWrites.createQuestions(questId, questionSet, ak);
			log.warning("Created: " + question.getKey());
			result += " [Created: " + question.getKey().getName() + "] ";
		}		
		return result;
	}
	
	private static String createGeneralEmployeeReviews() throws DuplicateEntitiesException, MissingEntitiesException
	{
		log.warning("Attempting to create reviews");
		Key<InetQuestions> generalQuestKey = new Key<InetQuestions>(InetQuestions.class, SetupConstants.GENERAL_EMPLOYEE_QUESTIONS_NAME);
	
		int reviewCount = 0;
		HashSet<Key<InetEmployee>> emplKeys = new HashSet<Key<InetEmployee>>();
		for(int i = 0; i < SetupConstants.REVIEW_INFO.length; i++)
		{
			String[] reviewers = SetupConstants.REVIEW_INFO[i]; 
			for(int j = 0; j < reviewers.length; j++)
				emplKeys.add(new Key<InetEmployee>(InetEmployee.class, reviewers[j]));
		}
		
		InetDAO4CommonReads.getEntities(emplKeys, ObjectifyService.begin()); //ensure all employee objects for review_info exist
		
		for(int i = 0; i < SetupConstants.REVIEW_INFO.length; i++)
		{
			if(!isInHackArray(i)) continue;
			String[] reviewers = SetupConstants.REVIEW_INFO[i]; 
			Key<InetEmployee> revieweeKey = new Key<InetEmployee>(InetEmployee.class, SetupConstants.EMPLOYEE_NAMES[i][2]);
			Key<InetQuestions> questKey = generalQuestKey;
			
			
			for(String revwr : reviewers)
			{
				Key<InetEmployee> reviewerKey = new Key<InetEmployee>(InetEmployee.class, revwr);  
				InetReview rev = InetDAO4CommonWrites.createReview(SetupConstants.REVIEW_CYCLE, 
						reviewerKey, revieweeKey, questKey);
				log.warning("Created Review: " + rev.getKey());
				reviewCount++;
			}
		}
		
		HashSet<Key<InetReview>> deletedKeys = new HashSet<Key<InetReview>>();
		for(String key : SetupConstants.DELETE_LIST.keySet())
		{
			Key<InetEmployee> revieweeKey = new Key<InetEmployee>(InetEmployee.class, key);
			Key<InetEmployee> reviewerKey = new Key<InetEmployee>(InetEmployee.class, SetupConstants.DELETE_LIST.get(key));
			Key<InetReview> revKey = new Key<InetReview>(InetReview.class, 
					InetReview.getKeyName(revieweeKey, generalQuestKey, reviewerKey, null, SetupConstants.REVIEW_CYCLE));
			deletedKeys.add(revKey);
		}
		Objectify ofy = ObjectifyService.begin();
		ofy.delete(deletedKeys);
		return "<b>Created " + reviewCount + " reviews successfully and deleted " + deletedKeys.size() + "</b><br/";
	}
	
	private static String createManagementTeamReview() throws MissingEntitiesException, DuplicateEntitiesException
	{			
		int revCount = 0;
		for(int i = 0; i < 24; i++)
		{
			if(SetupConstants.EMPLOYEE_NAMES[i][2].equals("admin@fieldcolimited.com")) continue;
			Key<InetEmployee> reviewerKey = new Key<InetEmployee>(InetEmployee.class, SetupConstants.EMPLOYEE_NAMES[i][2]);
				Key<InetQuestions> questKey = new Key<InetQuestions>(InetQuestions.class, SetupConstants.FIELDCO_QUESTIONS_NAME);
				InetReview rev = InetDAO4CommonWrites.createReview(SetupConstants.REVIEW_CYCLE, reviewerKey, "Company Review", questKey, false);
				log.warning("Created company review: " + rev.getKey());
				revCount++;
		}
		return revCount + " Company reviews";
	}
	
	private static String createSupervisorReviews() throws DuplicateEntitiesException, MissingEntitiesException
	{
		HashSet<Key<InetEmployee>> emplKeys = new HashSet<Key<InetEmployee>>();
		for(String[] supervisors: SetupConstants.SUPERVISOR_INFO)
		{
			if(supervisors == null) continue;
			for(String emp : supervisors)
				emplKeys.add(new Key<InetEmployee>(InetEmployee.class, emp));
		}
		
		InetDAO4CommonReads.getEntities(emplKeys, ObjectifyService.begin()); //ensure all employee objects for management team exists	
		
		int revCount = 0;
		for(int i = 0; i < SetupConstants.EMPLOYEE_NAMES.length; i++)
		{
			Key<InetEmployee> reviewerKey = new Key<InetEmployee>(InetEmployee.class, SetupConstants.EMPLOYEE_NAMES[i][2]);
			String[] supervisors = SetupConstants.SUPERVISOR_INFO[i];
			if(supervisors == null)
				continue;
			for(String sup : supervisors)
			{
				Key<InetQuestions> questKey = new Key<InetQuestions>(InetQuestions.class, SetupConstants.SUPERVISOR_QUESTIONS_NAME);
				Key<InetEmployee> revieweeKey = new Key<InetEmployee>(InetEmployee.class, sup);
				InetReview rev = InetDAO4CommonWrites.createReview(SetupConstants.REVIEW_CYCLE, reviewerKey, revieweeKey, questKey);
				log.warning("Created supervisor review: " + rev.getKey());
				revCount++;
			}
		}
		return revCount + " supervisor reviews";		
	}
	
	private static String createSelfReviews() throws DuplicateEntitiesException
	{
		int revCount = 0;
		log.warning("Attempting to create reviews");
		Key<InetQuestions> generalQuestKey = new Key<InetQuestions>(InetQuestions.class, SetupConstants.GENERAL_EMPLOYEE_QUESTIONS_NAME);
		Key<InetQuestions> questKey;
		
		for(int i = 0; i < 24; i++)
		{	
			if(SetupConstants.EMPLOYEE_NAMES[i][2].equals("admin@fieldcolimited.com")) continue;
			Key<InetEmployee> reviewerKey = new Key<InetEmployee>(InetEmployee.class, SetupConstants.EMPLOYEE_NAMES[i][2]);
			
			questKey = generalQuestKey;			
			
			InetReview rev = InetDAO4CommonWrites.createReview(SetupConstants.REVIEW_CYCLE, reviewerKey, reviewerKey, questKey);
			log.warning("Created self review: " + rev.getKey());
			revCount++;
		}
		return revCount + " self reviews";		
	}
	
	private static String modifySummarizer() throws MissingEntitiesException
	{
		String result = "";
		String summarySuffix = "2010 Year End.Review Summary";
		String[] summaries = {"ajibola.abudu", "festus.samuel", "akintola.aliu", "onasanya.moshood"};
		String[] currentSummarizers = {"theresa.obuye", "ibukun.paulissen", "ibukun.paulissen", "ibukun.paulissen"};
		String[] newSummarizers = {"blessing.ejiogu", "theresa.obuye", "theresa.obuye", "theresa.obuye"};
		
		Objectify ofy = ObjectifyService.begin();
		HashSet<Key<InetEmployee>> emplKeys = new HashSet<Key<InetEmployee>>();
		for(String keyName : newSummarizers)
			emplKeys.add(new Key<InetEmployee>(InetEmployee.class, keyName));
		
		InetDAO4CommonReads.getEntities(emplKeys, ofy); //validate empl counts
		
		ArrayList<Key<InetReviewSummary>> summaryKeys = new ArrayList<Key<InetReviewSummary>>();
		for(String keyName : summaries)
			summaryKeys.add(new Key<InetReviewSummary>(InetReviewSummary.class, keyName + "." + summarySuffix));
		
		Map<Key<InetReviewSummary>, InetReviewSummary> summaryObjs = InetDAO4CommonReads.getEntities(summaryKeys, ofy);
		for(int i = 0; i < summaryKeys.size(); i++)
		{
			InetReviewSummary summ = summaryObjs.get(summaryKeys.get(i));
			if(!summ.getReviewer().getName().equals(currentSummarizers[i]))
				throw new IllegalArgumentException("Existing summarizer check failed " + summ.getReviewer() + " vs " + currentSummarizers[i]);
			
			summ.setReviewer(new Key<InetEmployee>(InetEmployee.class, newSummarizers[i]));
			result += "Successfully modified " + summ.getKey().getName() + " to reviewer " + summ.getReviewer() + "<br/>";
		}
		ofy.put(summaryObjs.values());		
		return result;
	}
	
	private static String checkEmployeeSummarySetup()
	{
		String[] reviewers = SetupConstants.SUMMARY_INFO;
		String result = "Results below<br/>";
		
		HashMap<String, String[]> supervisorIndex = new HashMap<String, String[]>(SetupConstants.EMPLOYEE_NAMES.length-1); 
		//build supervisor index
		for(int i = 1; i < SetupConstants.EMPLOYEE_NAMES.length; i++)
			supervisorIndex.put(SetupConstants.EMPLOYEE_NAMES[i][2], SetupConstants.SUPERVISOR_INFO[i-1]);
			
		
		for(int i = 0; i < reviewers.length; i++)
		{
			String[] genReviewers = SetupConstants.REVIEW_INFO[i];
			for(int j = 0; j < genReviewers.length; j++)
				if(reviewers[i].equalsIgnoreCase(genReviewers[j]))
					result += ("Summarizer: " + reviewers[i] + "already reviewed " + SetupConstants.EMPLOYEE_NAMES[i][2]) + "<br/>";
			
			//if(i == 0) continue;
			log.warning("Summarizer is: " + reviewers[i]);
			String[] supervisors = supervisorIndex.get(reviewers[i]);
			log.warning("Summarizer's supervisors are: " + Arrays.toString(supervisors));
			for(int j = 0; j < supervisors.length; j++)
				if(SetupConstants.EMPLOYEE_NAMES[i][2].equalsIgnoreCase(supervisors[j]))
					result += "Summarizer: " + reviewers[i] + "already did a supervisor review for " + supervisors[j] + "<br/>";
		}
		return result;
	}
	
	private static String createEmployeeSummaries() throws MissingEntitiesException, DuplicateEntitiesException
	{
		//TODO replace logic below for checking summarizer/reviewed already with checkEmployeeSummarySetup
		int reviewCount = 0;
		HashSet<Key<InetEmployee>> emplKeys = new HashSet<Key<InetEmployee>>();
		String[] reviewers = SetupConstants.SUMMARY_INFO;
		for(int i = 0; i < reviewers.length; i++)
		{
			String[] genReviewers = SetupConstants.REVIEW_INFO[i];
			for(int j = 0; j < genReviewers.length; j++)
				if(reviewers[i].equalsIgnoreCase(genReviewers[j]))
					log.severe("Summarizer: " + reviewers[i] + "already reviewed " + SetupConstants.EMPLOYEE_NAMES[i][2]);
			emplKeys.add(new Key<InetEmployee>(InetEmployee.class, genReviewers[0]));
		}
		
		InetDAO4CommonReads.getEntities(emplKeys, ObjectifyService.begin()); //ensure all employee objects for review_info exist
		HashSet<Key<InetReviewSummary>> summaryKeys = new HashSet<Key<InetReviewSummary>>();
		for(String key : SetupConstants.DELETE_LIST.keySet())
		{
			if(key.equals("yewande.llewellyn@fieldcolimited.com")) continue;
			Key<InetReviewSummary> sumKey = new Key<InetReviewSummary>(InetReviewSummary.class, InetReviewSummary.getKeyName(key, SetupConstants.REVIEW_CYCLE));
			summaryKeys.add(sumKey);
		}
		Objectify ofy = ObjectifyService.begin();
		ofy.delete(summaryKeys);
		for(int i = 0; i < reviewers.length; i++)
		{
			if(i < 24) continue;
			if(!isInHackArray(i)) continue;
			
			if(SetupConstants.EMPLOYEE_NAMES[i][2].equals("admin@fieldcolimited.com")) continue;
			Key<InetEmployee> revieweeKey = new Key<InetEmployee>(InetEmployee.class, SetupConstants.EMPLOYEE_NAMES[i][2]);
			Key<InetQuestions> questKey = new Key<InetQuestions>(InetQuestions.class, SetupConstants.GENERAL_SUMMARY_QUESTIONS_NAME);
			
			Key<InetEmployee> reviewerKey = new Key<InetEmployee>(InetEmployee.class, SetupConstants.REVIEW_INFO[i][0]);  
			InetReview rev = InetDAO4CommonWrites.createReviewSummary(SetupConstants.REVIEW_CYCLE, reviewerKey, revieweeKey, questKey);
			log.warning("Created summary: " + rev.getKey());
			reviewCount++;
		}		
		return reviewCount + " summary reviews as well as " + summaryKeys.size();
	}
	
	private static boolean isInHackArray(int key)
	{
		int[] srchArr = SetupConstants.HACK_IDX;
		boolean result = false;
		for(int j : srchArr)
			if(j == key)
				return true;
		return result;
	}
	
	
	private static String createManagementTeamSummary() throws DuplicateEntitiesException
	{
		log.warning("Attempting to create reviews");
		Key<InetEmployee> reviewerKey = new Key<InetEmployee>(InetEmployee.class, "admin@fieldcolimited.com");
		Key<InetQuestions> questKey = new Key<InetQuestions>(InetQuestions.class, SetupConstants.COMPANY_SUMMARY_QUESTIONS_NAME);
		InetReviewSummary rev = InetDAO4CommonWrites.createReviewSummary(SetupConstants.REVIEW_CYCLE, reviewerKey, "Company Review", questKey);
		log.warning("Created Review: " + rev.getKey());
		return "Created " + rev.getKey().getName()+ " management summary successfully";
	}	
	

	
	private static String createPreferences() throws DuplicateEntitiesException, MissingEntitiesException
	{
		InetPreferences pref = InetDAO4CommonWrites.createPreferences(InetConstants.COMPANY_PREFERENCES_ID);
		log.warning("Created preferences: " + pref.getKey());
		HashMap<String, String> prefValues = new HashMap<String, String>();
		prefValues.put(InetConstants.COMPANY_PREFERENCES_CYCLE, SetupConstants.REVIEW_CYCLE);
		prefValues.put(InetConstants.COMPANY_ADMIN_USERS, SetupConstants.ADMINS);
		InetDAO4CommonWrites.updatePreferences(pref.getKey(), prefValues);
		return "<b>Created preferences succesfully</b>";
	}
	
	private static String createNotices() throws DuplicateEntitiesException
	{
		InetDAO4CommonWrites.createNotice(SetupConstants.LOGGEDIN_NOTICE, "Important Notice");
		InetDAO4CommonWrites.createNotice(SetupConstants.LOGGEDOUT_NOTICE, "Notice");
		return "<p><b>Successfully created notices</b></p>";
	}
}
