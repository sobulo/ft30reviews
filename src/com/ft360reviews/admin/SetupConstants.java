/**
 * 
 */
package com.ft360reviews.admin;

import java.util.HashMap;

/**
 * @author Segun Razaq Sobulo
 * 
 */
public class SetupConstants {
	public final static String REVIEW_CYCLE = "2013 August - Mid Year";
	static String GENERAL_EMPLOYEE_QUESTIONS_NAME = "Employee Review Questions (General)";
	static String GENERAL_MANAGER_QUESTIONS_NAME = "Employee Review Questions (Managers)";
	static String SUPERVISOR_QUESTIONS_NAME = "Supervisor Review Questions";
	static String FIELDCO_QUESTIONS_NAME = "Company Review Questions (Fieldco)";
	static String GENERAL_SUMMARY_QUESTIONS_NAME = "General Summary Questions";
	static String COMPANY_SUMMARY_QUESTIONS_NAME = "Company Summary Questions";
	static String ADMINS = "ibukun.paulissen,ajibola.abudu,funmi.adisa";

	public final static String LOGGEDIN_NOTICE = "loggedin-note";
	public final static String LOGGEDOUT_NOTICE = "loggedout-note";

	public final static String[] COMPANY_INFO = { "Fieldco Limited", "fieldco" };

	public final static String[][] EMPLOYEE_NAMES = {
			// this is a comment, below are non technician employees
		{ "Oluyemi", "Adeosun", "oluyemi.adeosun@fieldcolimited.com", "123" }, // ok
/*			{ "Ajibola", "Abudu", "ajibola.abudu@fieldcolimited.com", "123" }, // ok
			{ "Funmi", "Adisa", "funmi.adisa@fieldcolimited.com", "123" },
			{ "Yewande", "Llewellyn", "yewande.llewellyn@fieldcolimited.com",
					"123" }, //2
			{ "Ayodeji", "Bajomo", "ayodeji.bajomo@fieldcolimited.com", "123" },
			{ "Theresa", "Obuye", "theresa.obuye@fieldcolimited.com", "123" },
			{ "Ibukun", "Paulissen", "Ibukun.Paulissen@fieldcolimited.com",
					"123" },
			{ "Kayode", "Ogunbunmi", "kayode.ogunbunmi@fieldcolimited.com",
					"123" },
			{ "Blessing", "Ejiogu", "blessing.ejiogu@fieldcolimited.com", "123" },
			{ "Oladipupo", "Bakare", "oladipupo.bakare@fieldcolimited.com",
					"123" },
			{ "Temitope", "Shoga", "temitope.shoga@fieldcolimited.com", "123" },
			{ "Mojisola", "Bakare", "mojisola.bakare@fieldcolimited.com", "123" }, //10
			{ "Micheal", "Gbadamosi", "michael.gbadamosi@fieldcolimited.com",
					"123" },
			{ "Wakwe", "Osita", "osita.wakwe@fieldcolimited.com", "123" },
			{ "Lilian", "Eleghasim", "Lilian.Eleghasim@fieldcolimited.com",
					"123" },
			{ "Lolia", "Deal", "lolia.deal@fieldcolimited.com", "123" },
			{ "Rachel", "Onyedi", "rachel.onyedi@fieldcolimited.com", "123" },
			{ "Samuel", "Sylvanus", "samuel.sylvanus@fieldcolimited.com", "123" },
			{ "Bashiru", "Ojulari", "bashiru.ojulari@fieldcolimited.com", "123" },
			{ "Olamilekan", "Igbayilola",
					"olamilekan.igbayilola@fieldcolimited.com", "123" },
			{ "Olakunle", "Egbaiyelo", "olukunle.egbaiyelo@fieldcolimited.com",
					"123" },
			{ "Deji", "Opeloyeru", "Deji.Opeloyeru@fieldcolimited.com", "123" },
			{ "Funmilola", "Oyerogba", "Funmilola.Oyerogba@fieldcolimited.com",
					"123" },
			{ "Edward", "Unuane", "Edward.Unuane@fieldcolimited.com", "123" },
			{ "Adedeji", "Adio", "adedeji.adio", "123" },
			// technicians -- Mak start here
			{ "Vincent", "Amaechi", "vincent.amaechi", "4034" },
			{ "Waheed", "Oladapo", "waheed.oladapo", "999" },
			{ "Fanu", "Kehinde", "fanu.kehinde", "999" },

			{ "Festus", "Samuel", "festus.samuel", "1817" },
			{ "James", "Onoja", "james.onoja", "3496" },
			{ "Sylvester", "Ndimele", "sylvester.ndimele", "4354" },

			{ "Alabi", "Abduljeleel", "alabi.abduljeleel", "3453" },
			{ "Ajisafe", "Kazeem", "ajisafe.kazeem", "999" },
			{ "Amen", "Ekpe", "amen.ekpe", "999" },
			{ "Daniel", "Nwakwo", "daniel.nwakwo", "999" },
			{ "Akorede Mojeed", "Kolawole", "akorede.kolawole", "999" },
			{ "Ramon", "Salami", "ramon.salami", "999" }, //35

			{ "Taiwo", "Moses", "taiwo.moses", "999" },
			{ "John", "Ogunwole", "john.ogunwole", "999" },
			{ "Segun", "Sule", "segun.sule", "999" }, //38
			{ "Akintola", "Aliu", "akintola.aliu", "999" },
			{ "Yinka", "Otegbade", "yinka.otegbade", "999" },
			{ "Ogundare", "Sunday", "ogundare.sunday", "999" },
			{ "Adesina", "Babatunde", "adeshina.babatunde", "999" },

			{ "Onasanya", "Moshood", "onasanya.moshood", "1904" },
			{ "Rasheed", "Oguntade", "rasheed.oguntade", "999" },
			{ "Wasiu", "Balogun", "balogun.wasiu", "1289" },
			{ "Akinyemi", "A", "akinyemi.a", "999" },
			{ "Stephen", "Ifemadu", "stephen.ifemadu", "999" },
			{ "Cosmos", "A", "cosmos.a", "999" },//48
			{ "Kunle", "Adegbite", "kunle.adegbite", "999" },//49
			{ "Moses", "Onyelor", "moses.onyelor", "999" },
			{ "Alex", "Ohari", "alex.ohari", "999" },
			{ "Badmus", "Kehinde", "badmus.kehinde", "999" },//52
			{ "Chuka", "M", "chuka.m", "999" }, //53
			{ "Dare", "Olalude", "dare.olalude", "999" },
			{ "Hassan", "Olanrewaju", "hassan.olanrewaju", "999" },
			{ "Idowu", "Segun", "idowu.segun", "999" },
			{ "Shopeyin", "Jamiu", "shopeyin.jamiu", "999" },//57

			{ "Bawa-Allah", "Nurudeen", "bawa-allah.nurudeen", "999" },
			{ "Ajibola", "Sola", "ajibola.sola", "999" },
			{ "Oshun", "Alimi", "oshun.alimi", "999" },
			{ "Augustine", "Oare", "augustine.oare", "999" },

			{ "Oriku", "Benjamin", "oriku.benjamin", "999" },
			{ "Admin", "Account", "admin@fieldcolimited.com", "111" }*/
			};
	
	public final static HashMap<String, String> DELETE_LIST = new HashMap<String, String>();
		static{
			DELETE_LIST.put("yewande.llewellyn@fieldcolimited.com", "ibukun.paulissen@fieldcolimited.com");
			DELETE_LIST.put("segun.sule", "michael.gbadamosi@fieldcolimited.com");
			//DELETE_LIST.put("chuka.m", "rachel.onyedi@fieldcolimited.com");
			DELETE_LIST.put("ramon.salami", "olukunle.egbaiyelo@fieldcolimited.com");
			//DELETE_LIST.put("kunle.adegbite", "rachel.onyedi@fieldcolimited.com");
			//DELETE_LIST.put("badmus.kehinde", "rachel.onyedi@fieldcolimited.com");
			DELETE_LIST.put("shopeyin.jamiu", "rachel.onyedi@fieldcolimited.com");
			//DELETE_LIST.put("cosmos.a", "rachel.onyedi@fieldcolimited.com");
		}	
		
	public final static String[][] REVIEW_INFO = {
		{"yewande.llewellyn@fieldcolimited.com","bashiru.ojulari@fieldcolimited.com","Ibukun.Paulissen@fieldcolimited.com"},
	    {"ayodeji.bajomo@fieldcolimited.com","blessing.ejiogu@fieldcolimited.com","bashiru.ojulari@fieldcolimited.com","oladipupo.bakare@fieldcolimited.com"},
	    {"Lilian.Eleghasim@fieldcolimited.com"}, //2, wendy
	    {"funmi.adisa@fieldcolimited.com","mojisola.bakare@fieldcolimited.com","rachel.onyedi@fieldcolimited.com"},
	    {"yewande.llewellyn@fieldcolimited.com","Lilian.Eleghasim@fieldcolimited.com","Ibukun.Paulissen@fieldcolimited.com","lolia.deal@fieldcolimited.com"},
	    {"ajibola.abudu@fieldcolimited.com","blessing.ejiogu@fieldcolimited.com","oladipupo.bakare@fieldcolimited.com","lolia.deal@fieldcolimited.com"},
	    {"funmi.adisa@fieldcolimited.com","yewande.llewellyn@fieldcolimited.com","blessing.ejiogu@fieldcolimited.com"},
	    {"oladipupo.bakare@fieldcolimited.com","Ibukun.Paulissen@fieldcolimited.com","osita.wakwe@fieldcolimited.com","kayode.ogunbunmi@fieldcolimited.com"},
	    {"funmi.adisa@fieldcolimited.com","blessing.ejiogu@fieldcolimited.com","kayode.ogunbunmi@fieldcolimited.com","temitope.shoga@fieldcolimited.com"},
	    {"kayode.ogunbunmi@fieldcolimited.com","oladipupo.bakare@fieldcolimited.com","Edward.Unuane@fieldcolimited.com"},
	    {"ayodeji.bajomo@fieldcolimited.com"},//10
	    {"bashiru.ojulari@fieldcolimited.com","blessing.ejiogu@fieldcolimited.com","oladipupo.bakare@fieldcolimited.com"},
	    {"kayode.ogunbunmi@fieldcolimited.com","blessing.ejiogu@fieldcolimited.com","Ibukun.Paulissen@fieldcolimited.com"},
	    {"yewande.llewellyn@fieldcolimited.com","theresa.obuye@fieldcolimited.com","lolia.deal@fieldcolimited.com"},
	    {"yewande.llewellyn@fieldcolimited.com","Lilian.Eleghasim@fieldcolimited.com","oladipupo.bakare@fieldcolimited.com"},
	    {"bashiru.ojulari@fieldcolimited.com","ayodeji.bajomo@fieldcolimited.com","oladipupo.bakare@fieldcolimited.com"},
	    {"bashiru.ojulari@fieldcolimited.com","blessing.ejiogu@fieldcolimited.com","oladipupo.bakare@fieldcolimited.com"},
	    {"blessing.ejiogu@fieldcolimited.com","oladipupo.bakare@fieldcolimited.com","funmi.adisa@fieldcolimited.com"},
	    {"bashiru.ojulari@fieldcolimited.com","blessing.ejiogu@fieldcolimited.com","oladipupo.bakare@fieldcolimited.com"},
	    {"bashiru.ojulari@fieldcolimited.com","blessing.ejiogu@fieldcolimited.com","oladipupo.bakare@fieldcolimited.com"},
	    {"bashiru.ojulari@fieldcolimited.com","oladipupo.bakare@fieldcolimited.com","rachel.onyedi@fieldcolimited.com"},
	    {"kayode.ogunbunmi@fieldcolimited.com","osita.wakwe@fieldcolimited.com","Ibukun.Paulissen@fieldcolimited.com"},
	    {"temitope.shoga@fieldcolimited.com","mojisola.bakare@fieldcolimited.com","Funmilola.Oyerogba@fieldcolimited.com"},
	    {"ajibola.abudu@fieldcolimited.com","mojisola.bakare@fieldcolimited.com","temitope.shoga@fieldcolimited.com"},
	    
	    {"michael.gbadamosi@fieldcolimited.com"},
	    {"michael.gbadamosi@fieldcolimited.com"},
	    {"michael.gbadamosi@fieldcolimited.com"},
	    {"samuel.sylvanus@fieldcolimited.com"},
	    {"samuel.sylvanus@fieldcolimited.com"},
	    {"samuel.sylvanus@fieldcolimited.com"},
	    {"olukunle.egbaiyelo@fieldcolimited.com"},
	    {"olukunle.egbaiyelo@fieldcolimited.com"},
	    {"olukunle.egbaiyelo@fieldcolimited.com"},
	    {"olukunle.egbaiyelo@fieldcolimited.com"},
	    {"olukunle.egbaiyelo@fieldcolimited.com"},
	    {"michael.gbadamosi@fieldcolimited.com"},//35
	    {"michael.gbadamosi@fieldcolimited.com"},
	    {"michael.gbadamosi@fieldcolimited.com"},
	    {"samuel.sylvanus@fieldcolimited.com"}, //38
	    {"michael.gbadamosi@fieldcolimited.com"},
	    {"michael.gbadamosi@fieldcolimited.com"},
	    {"michael.gbadamosi@fieldcolimited.com"},
	    {"michael.gbadamosi@fieldcolimited.com"},
	    {"rachel.onyedi@fieldcolimited.com"},
	    {"rachel.onyedi@fieldcolimited.com"},
	    {"rachel.onyedi@fieldcolimited.com"},
	    {"rachel.onyedi@fieldcolimited.com"},
	    {"rachel.onyedi@fieldcolimited.com"},
	    {"bashiru.ojulari@fieldcolimited.com"},//48
	    {"michael.gbadamosi@fieldcolimited.com"},//49
	    {"rachel.onyedi@fieldcolimited.com"},
	    {"rachel.onyedi@fieldcolimited.com"},
	    {"michael.gbadamosi@fieldcolimited.com"}, //52
	    {"olukunle.egbaiyelo@fieldcolimited.com"},//53
	    {"rachel.onyedi@fieldcolimited.com"},
	    {"rachel.onyedi@fieldcolimited.com"},
	    {"rachel.onyedi@fieldcolimited.com"},
	    {"olamilekan.igbayilola@fieldcolimited.com"}, //57
	    {"olamilekan.igbayilola@fieldcolimited.com"},{"olamilekan.igbayilola@fieldcolimited.com"},{"olamilekan.igbayilola@fieldcolimited.com"},
	    {"olamilekan.igbayilola@fieldcolimited.com"},
	    {"bashiru.ojulari@fieldcolimited.com"},
	    {"kayode.ogunbunmi@fieldcolimited.com"}
	};
	public final static int[] HACK_IDX = {2, 10, 35,38, 57};
	public final static String[] SUMMARY_INFO = {
		"admin@fieldcolimited.com","ajibola.abudu@fieldcolimited.com","ajibola.abudu@fieldcolimited.com","funmi.adisa@fieldcolimited.com",
	    "yewande.llewellyn@fieldcolimited.com","ajibola.abudu@fieldcolimited.com","funmi.adisa@fieldcolimited.com","funmi.adisa@fieldcolimited.com",
	    "blessing.ejiogu@fieldcolimited.com","kayode.ogunbunmi@fieldcolimited.com","funmi.adisa@fieldcolimited.com","oladipupo.bakare@fieldcolimited.com",
	    "blessing.ejiogu@fieldcolimited.com","yewande.llewellyn@fieldcolimited.com","yewande.llewellyn@fieldcolimited.com",
	    "oladipupo.bakare@fieldcolimited.com","oladipupo.bakare@fieldcolimited.com","blessing.ejiogu@fieldcolimited.com","oladipupo.bakare@fieldcolimited.com",
	    "oladipupo.bakare@fieldcolimited.com","oladipupo.bakare@fieldcolimited.com","kayode.ogunbunmi@fieldcolimited.com","temitope.shoga@fieldcolimited.com",
	    "mojisola.bakare@fieldcolimited.com",
	    "michael.gbadamosi@fieldcolimited.com","michael.gbadamosi@fieldcolimited.com",
	    "michael.gbadamosi@fieldcolimited.com","samuel.sylvanus@fieldcolimited.com","samuel.sylvanus@fieldcolimited.com","samuel.sylvanus@fieldcolimited.com",
	    "olukunle.egbaiyelo@fieldcolimited.com","olukunle.egbaiyelo@fieldcolimited.com","olukunle.egbaiyelo@fieldcolimited.com","olukunle.egbaiyelo@fieldcolimited.com",
	  "olukunle.egbaiyelo@fieldcolimited.com","olukunle.egbaiyelo@fieldcolimited.com","michael.gbadamosi@fieldcolimited.com","michael.gbadamosi@fieldcolimited.com",
	  "michael.gbadamosi@fieldcolimited.com","michael.gbadamosi@fieldcolimited.com","michael.gbadamosi@fieldcolimited.com","michael.gbadamosi@fieldcolimited.com",
	    "michael.gbadamosi@fieldcolimited.com","rachel.onyedi@fieldcolimited.com","rachel.onyedi@fieldcolimited.com","rachel.onyedi@fieldcolimited.com",
	  "rachel.onyedi@fieldcolimited.com","rachel.onyedi@fieldcolimited.com","rachel.onyedi@fieldcolimited.com","rachel.onyedi@fieldcolimited.com","rachel.onyedi@fieldcolimited.com",
	  "rachel.onyedi@fieldcolimited.com","rachel.onyedi@fieldcolimited.com","rachel.onyedi@fieldcolimited.com","rachel.onyedi@fieldcolimited.com",
	  "rachel.onyedi@fieldcolimited.com","rachel.onyedi@fieldcolimited.com","rachel.onyedi@fieldcolimited.com","olamilekan.igbayilola@fieldcolimited.com","olamilekan.igbayilola@fieldcolimited.com",
	    "olamilekan.igbayilola@fieldcolimited.com","olamilekan.igbayilola@fieldcolimited.com","bashiru.ojulari@fieldcolimited.com", "kayode.ogunbunmi@fieldcolimited.com"		
	};

	public final static String[][] SUPERVISOR_INFO = new String[EMPLOYEE_NAMES.length][];
	public final static HashMap<String, String[]>[] QUESTION_SETUP = new HashMap[5];
	public final static String CHOICE_KEY = "multichoice";
	public final static String TEXT_KEY = "text";
	public final static String SCALARS_KEY = "scalars";

	public final static HashMap<String, String[]> ANS_SETUP = new HashMap<String, String[]>();

	static {
	/*	for(String[] info : EMPLOYEE_NAMES)
			info[2] = info[2].toLowerCase();
		
		for(String[] info : REVIEW_INFO)
			for(int i = 0; i < info.length; i++)
				info[i] = info[i].toLowerCase();
		
		for(int i = 0; i < SUMMARY_INFO.length; i++)
			SUMMARY_INFO[i] = SUMMARY_INFO[i].toLowerCase();
		
		SUPERVISOR_INFO[0] = null;
		for(int i = 1; i < 24; i++)
		{	
			SUPERVISOR_INFO[i] = new String[1];
			SUPERVISOR_INFO[i][0] = SUMMARY_INFO[i].toLowerCase();
		}
		for(int i = 24; i < EMPLOYEE_NAMES.length; i++)
			SUPERVISOR_INFO[i] = null;
		*/
		String[] CHOICE_GENERAL_ALL = {
				"Quality: The ability to plan organise, set priorities and complete high quality work",
				"Productivity: The extent to which an employee produces a significant volume of work in a specified period of time",
				"Knowledge: Degree of technical competence in field, ability to apply knowledge and stay abreast of new developments",
				"Reliability: The extend to which an employee can be depended on to complete assigned tasks",
				"Analytical ability and judgement: Ability to identify a problem, obtain and evaluate facts, reach sound conclusions and present them in a clear and concise manner",
				"Adhere to policy: Follows rules and other regulations and adheres to company policy",
				"Interpesonal relationships: Effectively demonstrate the ability to co-operate, work, and communicate with co-workers, managers, sub-ordinates and display a positive attitude",
				"Creativity/Initiative: The extent to which an employee proposes ideas and ways of doing things as well as assumes additional duties beyond his/her day to day responsibilities",
				"Effectiveness of customer relations: Ability to communicate and understand and respond to the customer consistent with corporate objectives and sound business practices" };

		String[] TEXT_GENERAL_ALL = {
				"Strengths: What does this employee do particularly well?",
				"Development areas: What would you like this employee to start doing, stop doing or improve upon in order to get better at his/her job?" };

		String[] CHOICE_SUMMARY = { "Overall performance rating: This should represent a composite picture that is consistent with ratings provided"
				+ " by various reviewers. If at first glance rating appears inconsistent (e.g. doesn't matchup with calculated"
				+ " avg rating of other reviewers), please provide more context in the text entry sections of the review" };

		String[] TEXT_GENERAL_SUMMARY = { "Recommendation for development:" };

		String[] SUPERVISOR_QUESTIONS = {
				"My supervisor treats me with respect",
				"My supervisor takes initiative when solving problems",
				"My supervisor develops new strategies",
				"My supervisor applies policies and regulations fairly",
				"My supervisor handles my work related issues satisfactorily",
				"My supervisor tells me when my work needs improvement",
				"My supervisor asks me for my input to help make decisions",
				"My supervisor gives good, practical advice",
				"My supervisor is well informed",
				"My supervisor makes good use of my skills and abilities",
				"My supervisor establishes clear expectations",
				"My supervisor provides me with adequate feedback",
				"My supervisor holds me accountable for the work that I do",
				"My supervisor is an effective leader",
				"My supervisor participates in meetings" };

		String[] MANAGEMENT_QUESTIONS = {
				"Fieldco is a place where employees are treated with fairness and respect",
				"Fieldco demonstrates that employees are important to the success of the organization",
				"Fieldco provides a clear picture of where the organization is headed",
				"Fieldco managers practice what they preach",
				"Fieldco is an organization that encourages my development",
				"At fieldco all options are evaluated before action is taken",
				"Fieldco provides the necessary resources to perform my job",
				"At Fieldco, people are open minded",
				"Fieldco recognizes employees for good work",
				"At Fieldco I can explore new and exciting opportunities",
				"Fieldco expects and demands superior job performance",
				"Fieldco considers innovative solutions to problems" };

		String[] MANAGEMENT_TEXT = { "Barriers and or frustrations concerning your job" };

		String[] MANAGEMENT_SUMMARY_TEXT = { "Recommendations based on company survey" };

		String[] MANAGEMENT_SUMMARY_CHOICE = { "Employee's have a good perception of the company" };

		String[] CHOICE_GENERAL = { "Below Expectation", "Partially Meets",
				"Meets Expectation", "Exceeds Expectation" };

		String ansKeyGeneral = "generalchoices";
		ANS_SETUP.put(ansKeyGeneral, CHOICE_GENERAL);

		String[] temp = { GENERAL_EMPLOYEE_QUESTIONS_NAME, ansKeyGeneral };
		QUESTION_SETUP[0] = new HashMap<String, String[]>();
		QUESTION_SETUP[0].put(CHOICE_KEY, CHOICE_GENERAL_ALL);
		QUESTION_SETUP[0].put(TEXT_KEY, TEXT_GENERAL_ALL);
		QUESTION_SETUP[0].put(SCALARS_KEY, temp);

		String[] temp3 = { SUPERVISOR_QUESTIONS_NAME, ansKeyGeneral };
		QUESTION_SETUP[1] = new HashMap<String, String[]>();
		QUESTION_SETUP[1].put(CHOICE_KEY, SUPERVISOR_QUESTIONS);
		QUESTION_SETUP[1].put(SCALARS_KEY, temp3);

		String[] temp4 = { FIELDCO_QUESTIONS_NAME, ansKeyGeneral };
		QUESTION_SETUP[2] = new HashMap<String, String[]>();
		QUESTION_SETUP[2].put(CHOICE_KEY, MANAGEMENT_QUESTIONS);
		QUESTION_SETUP[2].put(TEXT_KEY, MANAGEMENT_TEXT);
		QUESTION_SETUP[2].put(SCALARS_KEY, temp4);

		String[] generalSummaryQuestions = new String[TEXT_GENERAL_ALL.length
				+ TEXT_GENERAL_SUMMARY.length];
		int count = 0;
		for (String s : TEXT_GENERAL_ALL)
			generalSummaryQuestions[count++] = s;

		for (String s : TEXT_GENERAL_SUMMARY)
			generalSummaryQuestions[count++] = s;
		String[] temp5 = { GENERAL_SUMMARY_QUESTIONS_NAME, ansKeyGeneral };
		QUESTION_SETUP[3] = new HashMap<String, String[]>();
		QUESTION_SETUP[3].put(CHOICE_KEY, CHOICE_SUMMARY);
		QUESTION_SETUP[3].put(TEXT_KEY, generalSummaryQuestions);
		QUESTION_SETUP[3].put(SCALARS_KEY, temp5);

		String[] temp6 = { COMPANY_SUMMARY_QUESTIONS_NAME, ansKeyGeneral };
		QUESTION_SETUP[4] = new HashMap<String, String[]>();
		QUESTION_SETUP[4].put(CHOICE_KEY, MANAGEMENT_SUMMARY_CHOICE);
		QUESTION_SETUP[4].put(TEXT_KEY, MANAGEMENT_SUMMARY_TEXT);
		QUESTION_SETUP[4].put(SCALARS_KEY, temp6);
	}

	/*
	 * public static void main(String[] args) {
	 * System.out.println("FName, LName, Login, Password"); for(String[] row :
	 * EMPLOYEE_NAMES) { for(String entry : row) { System.out.print(entry +
	 * ","); } System.out.println(); } }
	 */

}
