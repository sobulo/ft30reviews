/**
 * 
 */
package com.ft360reviews.entities;

import com.googlecode.objectify.Key;

/**
 * @author Segun Razaq Sobulo
 *
 */
public interface GAEPrimaryEntity {
    public <T> Key<T> getKey();
}
