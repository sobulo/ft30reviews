/**
 * 
 */
package com.ft360reviews.entities;

import java.util.HashSet;

import javax.persistence.Id;

import com.googlecode.objectify.Key;
import com.googlecode.objectify.annotation.Cached;
import com.googlecode.objectify.annotation.Unindexed;

/**
 * @author Segun Razaq Sobulo
 *
 */
@Unindexed
@Cached
public class InetCompany implements GAEPrimaryEntity{
    @Id
    private String key;

    private String companyName;

    private String accronym;

    private String email;

    private String address;

    private HashSet<String> phoneNumbers;

    private String webAddress;
    
    InetCompany(){}

    InetCompany(String companyName, String accronym, String email, String webAddress,
            String address, HashSet<String> phoneNumbers)
    {
        this();
        this.companyName = companyName;
        this.accronym = accronym;
        this.email = email;
        this.address = address;
        this.phoneNumbers = phoneNumbers;
        this.webAddress = webAddress;

        this.key = InetConstants.COMPANY_ID;
    }

    public String getWebAddress() {
        return webAddress;
    }

    public void setWebAddress(String webAddress) {
        this.webAddress = webAddress;
    }

    public String getAccronym() {
        return accronym;
    }

    void setAccronym(String accronym) {
        this.accronym = accronym;
    }

    public String getAddress() {
        return address;
    }

    void setAddress(String address) {
        this.address = address;
    }

    public String getEmail() {
        return email;
    }

    void setEmail(String email) {
        this.email = email;
    }

    public Key<InetCompany> getKey() {
        return new Key<InetCompany>(InetCompany.class, key);
    }

    void setPhoneNumbers(HashSet<String> pn)
    {
        phoneNumbers = pn;
    }

    public HashSet<String> getPhoneNumbers()
    {
        return phoneNumbers;
    }

    public String getCompanyName() {
        return companyName;
    }

    void setSchoolName(String schoolName) {
        this.companyName = schoolName;
    }
}
