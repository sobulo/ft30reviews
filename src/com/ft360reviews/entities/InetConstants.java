/**
 * 
 */
package com.ft360reviews.entities;

/**
 * @author Segun Razaq Sobulo
 *
 */
public class InetConstants {
	public final static String COMPANY_ID = "company";
	public final static String COMPANY_PREFERENCES_ID = "companypref";
	public final static String COMPANY_PREFERENCES_CYCLE = "reviewcycle";
	public final static String COMPANY_ADMIN_USERS = "admins";
	public final static String LOGGEDIN_NOTICE = "loggedin-note";
	public final static String LOGGEDOUT_NOTICE = "loggedout-note";
}
