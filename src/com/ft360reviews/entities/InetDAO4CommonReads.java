/**
 * 
 */
package com.ft360reviews.entities;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import com.ft360reviews.gui.shared.exceptions.MissingEntitiesException;
import com.ft360reviews.gui.shared.exceptions.MultipleEntitiesException;
import com.ft360reviews.utils.GeneralFuncs;
import com.google.appengine.api.datastore.QueryResultIterable;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.Objectify;
import com.googlecode.objectify.ObjectifyService;

/**
 * @author Segun Razaq Sobulo
 *
 */
public class InetDAO4CommonReads {
    private static final Logger log =
        Logger.getLogger(InetDAO4CommonReads.class.getName());

    private static boolean isOfyRegistered = false;

	//register classes being managed
	public static void registerClassesWithObjectify()
	{
	    if(isOfyRegistered)
	        return;
	
	    log.warning("Registering services with Objectify");
	    ObjectifyService.register(InetCompany.class);
	    ObjectifyService.register(InetEmployee.class);
	    ObjectifyService.register(InetReview.class);
	    ObjectifyService.register(InetReviewSummary.class);
	    ObjectifyService.register(InetMultiAnswers.class);
	    ObjectifyService.register(InetQuestions.class);
	    ObjectifyService.register(InetPreferences.class);
	    ObjectifyService.register(InetNotice.class);
	    isOfyRegistered = true;
	}
	
	public static InetCompany getCompany(Objectify ofy)
	{
	    return ofy.get(InetCompany.class, InetConstants.COMPANY_ID);
	}
	
	public static InetPreferences getCompanyPreferences(Objectify ofy) throws MissingEntitiesException
	{
	    InetPreferences pref = ofy.find(InetPreferences.class, InetConstants.COMPANY_PREFERENCES_ID);
	    if(pref == null)
	    {
	    	String msg = "Missing company preferences: " + InetConstants.COMPANY_PREFERENCES_ID;
	    	log.severe(msg);
	    	throw new MissingEntitiesException(msg);
	    }
	    return pref;
	}	
	
    public static InetEmployee getEmployeeByEmail(Objectify ofy, String userEmail) throws MultipleEntitiesException
    {
        //we limit result set to 2 because anything more than 1 is an issue.i.e.
        //same headache whether 2 users share same email or 20 users share it
        List<InetEmployee> results = ofy.query(InetEmployee.class).filter("email", userEmail).limit(2).list();

        if (results.size() == 1)
            return results.get(0);
        else if(results.size() > 1)
        {
            String msg = "Email: " + userEmail + " matched more than" +
                    " 1 entity. Showing a couple below";
            for(InetEmployee user : results)
                msg += user.getKey().getName() + ", ";
            throw new MultipleEntitiesException(msg);
        }
        return null;
    }
    
    public static <T extends InetReview> List<T> getReviews(Objectify ofy, String reviewCycle, 
    		String queryParam, Object paramArg, Class<T> clss)
    {
        return ofy.query(clss).filter(queryParam, paramArg).
        			filter(InetReview.QUERY_PARAM_CYCLE, reviewCycle).list();
    }
    
    public static <T extends InetReview> HashSet<Key<T>> getReviewKeys(Objectify ofy, String reviewCycle, 
    		String queryParam, Object paramArg, Class<T> clss)
    {
        QueryResultIterable<Key<T>> keyItr = ofy.query(clss).filter(queryParam, paramArg).
        			filter(InetReview.QUERY_PARAM_CYCLE, reviewCycle).fetchKeys();
        HashSet<Key<T>> result = new HashSet<Key<T>>();
        for(Key<T> key : keyItr)
        	result.add(key);
        return result;
    }
    
    public static <T extends InetReview> HashSet<Key<T>> getReviewKeys(Objectify ofy,  
    		String queryParam, Object paramArg, Class<T> clss)
    {
        QueryResultIterable<Key<T>> keyItr = ofy.query(clss).filter(queryParam, paramArg).fetchKeys();
        HashSet<Key<T>> result = new HashSet<Key<T>>();
        for(Key<T> key : keyItr)
        	result.add(key);
        return result;
    }    
    
    public static <T extends GAEPrimaryEntity> Map<Key<T>, T> 
    	getMixedEntities(Collection<Key<? extends T>> entityKeys, Objectify ofy) throws MissingEntitiesException
    {
        Map<Key<T>, T> entityData = ofy.get(entityKeys);
        if(entityData.size() != entityKeys.size())
        {
            log.severe("Unable to load some entites from database: ");
            log.severe("Printing out all requested keys");
            for(Key<? extends T> errKey : entityKeys)
            {
                log.severe("Requested Key: " + errKey);
            }
            log.severe("Printing out retrieved keys");
            for(Key<? extends T> errKey : entityData.keySet())
                log.severe("Retrieved Key: " + errKey);
            throw new MissingEntitiesException("Batch get failed, see server log for details");
        }
        return entityData;
    }    

    public static <T> Map<Key<T>, T> getEntities(Collection<Key<T>> entityKeys, Objectify ofy) throws MissingEntitiesException
    {
        Map<Key<T>, T> entityData = ofy.get(entityKeys);
        if(entityData.size() != entityKeys.size())
        {

            log.severe("Unable to load some entites from database ....");
            
            printKeyDifferences(entityKeys, entityData.keySet());
            
            log.severe("Keys initially requested are ...... ");
            for(Key<T> errKey : entityKeys)
            {
                log.severe("Requested Key: " + errKey);
            }
            
            throw new MissingEntitiesException("Batch get failed, see server log for details");
        }
        return entityData;
    }
    
    public static <T> void printKeyDifferences(Collection<Key<T>> requestedCollection, Collection<Key<T>> retrievedCollection)
    {
    	Key<T>[] requestedKeys = new Key[requestedCollection.size()];
    	requestedKeys = requestedCollection.toArray(requestedKeys);
    	Key<T>[] retrievedKeys = new Key[retrievedCollection.size()];
    	retrievedKeys = retrievedCollection.toArray(retrievedKeys);
    	ArrayList<Key<T>> retrievedOnly, requestedOnly, intersect;
    	retrievedOnly = new ArrayList<Key<T>>();
    	requestedOnly = new ArrayList<Key<T>>();
    	intersect = new ArrayList<Key<T>>();
    	GeneralFuncs.arrayDiff(requestedKeys, retrievedKeys, intersect, requestedOnly, retrievedOnly);
    	for(Key<T> k : requestedOnly)
    		log.severe("Unable to retrieve: " + k);
    }
    
    
    public final static int REVIEW_IDX = 0;
    public final static int QUESTION_SET_IDX = 1;
    public final static int ANS_SET_IDX = 2;
    public static GAEPrimaryEntity[] getReviewObjects(Objectify ofy, Key<? extends InetReview> reviewKey) throws MissingEntitiesException
    {
		GAEPrimaryEntity[] result = new GAEPrimaryEntity[3];
	
		InetReview rev = ofy.find(reviewKey);
		if(rev == null)
		{
			String msgFmt = "Unable to find review data for: ";
			log.severe(msgFmt + reviewKey);
			throw new MissingEntitiesException(msgFmt + reviewKey.getName()); 
		}
		
		InetQuestions quest = ofy.find(rev.getQuestionSetKey());
		if(quest == null)
		{
			String msgFmt = "Unable to find review questions for: ";
			log.severe(msgFmt + rev.getQuestionSetKey());
			throw new MissingEntitiesException(msgFmt + rev.getQuestionSetKey().getName()); 
		}
		
		InetMultiAnswers ans = ofy.find(quest.getAnswerKey());
		if(ans == null)
		{
			String msgFmt = "Unable to find answer mapping for: ";
			log.severe(msgFmt + quest.getAnswerKey());
			throw new MissingEntitiesException(msgFmt + quest.getAnswerKey().getName()); 
		}
		
		result[REVIEW_IDX] = rev;
		result[QUESTION_SET_IDX] = quest;
		result[ANS_SET_IDX] = ans;
		return result;
    }
    
	public static List<InetEmployee> fetchAllEmployees(Objectify ofy)
	{
		List<InetEmployee> result = ofy.query(InetEmployee.class).list();
		log.info("Retrieved: " + result.size());
		return result;
	}
	
	public static HashMap<String, String> fetchAllQuestions(Objectify ofy)
	{
		HashMap<String, String> result = new HashMap<String, String>();
		QueryResultIterable<Key<InetQuestions>> questKeys = ofy.query(InetQuestions.class).fetchKeys();
		for(Key<InetQuestions> key : questKeys)
			result.put(ofy.getFactory().keyToString(key), key.getName());
		return result;
	}
    
}
