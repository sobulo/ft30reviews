/**
 * 
 */
package com.ft360reviews.entities;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Logger;

import com.ft360reviews.gui.shared.exceptions.DuplicateEntitiesException;
import com.ft360reviews.gui.shared.exceptions.MissingEntitiesException;
import com.ft360reviews.login.BCrypt;
import com.google.appengine.api.datastore.Text;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.Objectify;
import com.googlecode.objectify.ObjectifyService;

/**
 * @author Segun Razaq Sobulo
 *
 */
public class InetDAO4CommonWrites {
    private static final Logger log =
        Logger.getLogger(InetDAO4CommonReads.class.getName());	

	public static InetCompany createCompany(String companyName, String accronym, 
			String email, String webAddress, String address, HashSet<String> phoneNumbers) throws DuplicateEntitiesException
	{
		InetCompany comp = new InetCompany(companyName, accronym, email, webAddress, address, phoneNumbers);
		Objectify ofy = ObjectifyService.beginTransaction();
		try
		{
			InetCompany c = ofy.find(comp.getKey());
			if(c != null)
			{
				String msgFmt = "Company already exists for this domain: ";
				log.severe(msgFmt + c.getKey());
				throw new DuplicateEntitiesException(msgFmt + c.getCompanyName());
			}
			ofy.put(comp);
			ofy.getTxn().commit();
		}
		finally
		{
			if(ofy.getTxn().isActive())
				ofy.getTxn().rollback();
		}
		return comp;
	}
	
	public static InetPreferences createPreferences(String id) throws DuplicateEntitiesException
	{
		InetPreferences comp = new InetPreferences(id);
		Objectify ofy = ObjectifyService.beginTransaction();
		try
		{
			InetPreferences c = ofy.find(comp.getKey());
			if(c != null)
			{
				String msgFmt = "Company preferences already exists for this domain: ";
				log.severe(msgFmt + c.getKey());
				throw new DuplicateEntitiesException(msgFmt + c.getKey().getName());
			}
			ofy.put(comp);
			ofy.getTxn().commit();
		}
		finally
		{
			if(ofy.getTxn().isActive())
				ofy.getTxn().rollback();
		}
		return comp;
	}
	
	public static InetNotice createNotice(String id, String title) throws DuplicateEntitiesException
	{
		InetNotice note = new InetNotice(id, title, "");
		Objectify ofy = ObjectifyService.beginTransaction();
		try
		{
			InetNotice n = ofy.find(note.getKey());
			if(n != null)
			{
				String msgFmt = "Notice exists already: ";
				log.severe(msgFmt + n.getKey());
				throw new DuplicateEntitiesException(msgFmt + n.getKey().getName());
			}
			ofy.put(note);
			ofy.getTxn().commit();
		}
		finally
		{
			if(ofy.getTxn().isActive())
				ofy.getTxn().rollback();
		}
		return note;
	}	
	
	public static void updatePreferences(Key<InetPreferences> prefKey, HashMap<String, String> values) throws MissingEntitiesException
	{
		Objectify ofy = ObjectifyService.beginTransaction();
		try
		{
			InetPreferences c = ofy.find(prefKey);
			if(c == null)
			{
				String msgFmt = "Company preferences missing for this domain: ";
				log.severe(msgFmt + prefKey);
				throw new MissingEntitiesException(msgFmt + prefKey.getName());
			}
			c.setPreferences(values);
			ofy.put(c);
			ofy.getTxn().commit();
		}
		finally
		{
			if(ofy.getTxn().isActive())
				ofy.getTxn().rollback();
		}
	}	
	
	public static InetEmployee createEmployee(String fname, String lname, String email,
			HashSet<String> phoneNumbers, String loginName, String password) throws DuplicateEntitiesException
	{
		InetEmployee empl = new InetEmployee(fname, lname, email, phoneNumbers, loginName, getHashPwd(password));
		Objectify ofy = ObjectifyService.beginTransaction();
		try
		{
			InetEmployee e = ofy.find(empl.getKey());
			if(e != null)
			{
				String msgFmt = "Employee already exists: ";
				log.severe(msgFmt + e.getKey());
				throw new DuplicateEntitiesException(msgFmt + e.getKey().getName());
			}
			ofy.put(empl);
			ofy.getTxn().commit();
		}
		finally
		{
			if(ofy.getTxn().isActive())
				ofy.getTxn().rollback();
		}
		return empl;
	}
	
	public static InetMultiAnswers createMultiAnswers(String id, HashMap<Integer, 
			String> answerMapping) throws DuplicateEntitiesException
	{
		InetMultiAnswers ans = new InetMultiAnswers(id, answerMapping);
		Objectify ofy = ObjectifyService.beginTransaction();
		try
		{
			InetMultiAnswers a = ofy.find(ans.getKey());
			if(a != null)
			{
				String msgFmt = "MultiAnswer already exists: ";
				log.severe(msgFmt + a.getKey());
				throw new DuplicateEntitiesException(msgFmt + a.getKey().getName());
			}
			ofy.put(ans);
			ofy.getTxn().commit();
		}
		finally
		{
			if(ofy.getTxn().isActive())
				ofy.getTxn().rollback();
		}		
		return ans;
	}
	
	public static void deleteMultiAnswers(String id) throws DuplicateEntitiesException
	{
		Objectify ofy = ObjectifyService.beginTransaction();
		try
		{
			InetMultiAnswers a = ofy.find(new Key<InetMultiAnswers>(InetMultiAnswers.class, id));
			if(a == null)
			{
				String msgFmt = "MultiAnswer doesn't exists: ";
				log.severe(msgFmt + a.getKey());
				throw new RuntimeException(msgFmt + a.getKey().getName());
			}
			ofy.delete(a);
			ofy.getTxn().commit();
		}
		finally
		{
			if(ofy.getTxn().isActive())
				ofy.getTxn().rollback();
		}		
	}
	
	
	public static InetQuestions createQuestions(String id, HashMap<Integer, String> questionSet, 
			Key<InetMultiAnswers> answerKey) throws DuplicateEntitiesException
	{
		InetQuestions quest = new InetQuestions(id, questionSet, answerKey);
		Objectify ofy = ObjectifyService.beginTransaction();
		try
		{
			InetQuestions q = ofy.find(quest.getKey());
			if(q != null)
			{
				String msgFmt = "Question set already exists: ";
				log.severe(msgFmt + q.getKey());
				throw new DuplicateEntitiesException(msgFmt + q.getKey().getName());
			}
			ofy.put(quest);
			ofy.getTxn().commit();
		}
		finally
		{
			if(ofy.getTxn().isActive())
				ofy.getTxn().rollback();
		}		
		return quest;		
	}
	
	public static InetReview createReview(String cycleId, Key<InetEmployee> reviewer, 
			Key<InetEmployee> reviewee, Key<InetQuestions> questionSetKey) throws DuplicateEntitiesException
	{
		InetReview review = new InetReview(cycleId, reviewer, reviewee, questionSetKey);
		Objectify ofy = ObjectifyService.beginTransaction();
		try
		{
			InetReview r = ofy.find(review.getKey());
			if(r != null)
			{
				String msgFmt = "Review already exists: ";
				log.severe(msgFmt + r.getKey());
				throw new DuplicateEntitiesException(msgFmt + r.getKey().getName());
			}
			ofy.put(review);
			ofy.getTxn().commit();
		}
		finally
		{
			if(ofy.getTxn().isActive())
				ofy.getTxn().rollback();
		}		
		return review;		
	}
	
	public static InetReview createReview(String cycleId, Key<InetEmployee> reviewer, 
			String name, Key<InetQuestions> questionSetKey, boolean allowRevieweeEdit) throws DuplicateEntitiesException
	{
		InetReview review = new InetReview(cycleId, reviewer, name, questionSetKey, allowRevieweeEdit);
		Objectify ofy = ObjectifyService.beginTransaction();
		try
		{
			InetReview r = ofy.find(review.getKey());
			if(r != null)
			{
				String msgFmt = "Review already exists: ";
				log.severe(msgFmt + r.getKey());
				throw new DuplicateEntitiesException(msgFmt + r.getKey().getName());
			}
			ofy.put(review);
			ofy.getTxn().commit();
		}
		finally
		{
			if(ofy.getTxn().isActive())
				ofy.getTxn().rollback();
		}		
		return review;		
	}	
	
	public static InetReviewSummary createReviewSummary(String cycleId, Key<InetEmployee> reviewer, 
			Key<InetEmployee> reviewee, Key<InetQuestions> questionSetKey) throws DuplicateEntitiesException
	{
		InetReviewSummary revsum = new InetReviewSummary(cycleId, reviewer, reviewee, questionSetKey);
		Objectify ofy = ObjectifyService.beginTransaction();
		try
		{
			InetReviewSummary r = (InetReviewSummary) ofy.find(revsum.getKey());
			if(r != null)
			{
				String msgFmt = "Review summary already exists: ";
				log.severe(msgFmt + r.getKey());
				throw new DuplicateEntitiesException(msgFmt + r.getKey().getName());
			}
			ofy.put(revsum);
			ofy.getTxn().commit();
		}
		finally
		{
			if(ofy.getTxn().isActive())
				ofy.getTxn().rollback();
		}		
		return revsum;		
	}
	
	public static InetReviewSummary createReviewSummary(String cycleId, Key<InetEmployee> reviewer, 
			String name, Key<InetQuestions> questionSetKey) throws DuplicateEntitiesException
	{
		InetReview review = new InetReviewSummary(cycleId, reviewer, name, questionSetKey);
		Objectify ofy = ObjectifyService.beginTransaction();
		try
		{
			InetReview r = ofy.find(review.getKey());
			if(r != null)
			{
				String msgFmt = "Review already exists: ";
				log.severe(msgFmt + r.getKey());
				throw new DuplicateEntitiesException(msgFmt + r.getKey().getName());
			}
			ofy.put(review);
			ofy.getTxn().commit();
		}
		finally
		{
			if(ofy.getTxn().isActive())
				ofy.getTxn().rollback();
		}		
		return (InetReviewSummary) review;		
	}		
	
	public static InetReview updateReview(Key<? extends InetReview> reviewKey, Key<InetEmployee> revieweeKey,
			HashMap<Integer, Integer> multichoiceAnswers, HashMap<Integer, Text> textAnswers,
			boolean isLocked, String adminComments) throws MissingEntitiesException, DuplicateEntitiesException
	{
		Objectify ofy = ObjectifyService.begin();
		
		//validate entities
		GAEPrimaryEntity[] revObjs = InetDAO4CommonReads.getReviewObjects(ofy, reviewKey);
		InetReview rev = (InetReview) revObjs[InetDAO4CommonReads.REVIEW_IDX];
		InetQuestions quest = (InetQuestions) revObjs[InetDAO4CommonReads.QUESTION_SET_IDX];
		InetMultiAnswers ans =  (InetMultiAnswers) revObjs[InetDAO4CommonReads.ANS_SET_IDX];
		
		//validate that supplied answers matchup with questions
		Set<Integer> ansKeyset = multichoiceAnswers.keySet();
		HashMap<Integer, String> questions = quest.getQuestionSet();
		HashMap<Integer, String> ansMapping = ans.getAnswerMapping();
		for(int i : ansKeyset)
		{
			if(!questions.containsKey(i))
			{
				String msgFmt = "Question %s does not contain mulitchoice question number " + i;
				log.severe(String.format(msgFmt, quest.getKey().toString()));
				throw new MissingEntitiesException(String.format(msgFmt, quest.getKey().getName()));
			}
			
			if(!ansMapping.containsKey(multichoiceAnswers.get(i)))
			{
				String msgFmt = "Answer mapping %s does not contain an entry for option " + multichoiceAnswers.get(i);
				log.severe(String.format(msgFmt, ans.getKey().toString()));
				throw new MissingEntitiesException(String.format(msgFmt, ans.getKey().getName()));
			}
		}
		
		for(int i : textAnswers.keySet())
		{
			if(!questions.containsKey(i))
			{
				String msgFmt = "Question %s does not contain freeform question number " + i;
				log.severe(String.format(msgFmt, quest.getKey().toString()));
				throw new MissingEntitiesException(String.format(msgFmt, quest.getKey().getName()));
			}			
		}
		
		ofy = ObjectifyService.beginTransaction();
		try
		{
			if(revieweeKey != null)
			{
				if(rev.reviewee != null && !rev.reviewee.equals(revieweeKey))
				{
					String msgFmt = "Attempting to update existing reviewee %s with %s. Reviewee update supported only if currently blank";
					log.severe(String.format(msgFmt, rev.reviewee.toString(), revieweeKey.toString()));
					throw new DuplicateEntitiesException(String.format(msgFmt, rev.reviewee.getName(), revieweeKey.getName()));
				}
				else
					rev.setReviewee(revieweeKey);					
			}
	
			rev.setMulitchoiceAnswers(multichoiceAnswers);
			rev.setTextAnswers(textAnswers);
			rev.setLocked(isLocked);
			rev.setAdminComments(adminComments);
			ofy.put(rev);
			ofy.getTxn().commit();
		}
		finally
		{
			if(ofy.getTxn().isActive())
				ofy.getTxn().rollback();
		}
		return rev;
	}
		
    public static String getHashPwd(String pwd)
    {
    	return BCrypt.hashpw(pwd, BCrypt.gensalt());
    }
    
    public static void updateEmployee(String fname, String lname, String email, 
    		HashSet<String> phoneNums, Key<InetEmployee> empKey) 
    	throws MissingEntitiesException
    {
        Objectify ofy = ObjectifyService.beginTransaction();
        try
        {
            //confirm new student object doesn't exist already
            InetEmployee emp = ofy.find(empKey);
            if (emp == null) {
                MissingEntitiesException ex = new MissingEntitiesException(
                        "Unable to find info for employee: " + empKey.getName());
                log.severe(ex.getMessage());
                throw ex;
            }
                        
            updateUserInfo(fname, lname, email, phoneNums, emp);

            //persist student object
            ofy.put(emp);
            ofy.getTxn().commit();
        } finally {
            if (ofy.getTxn().isActive()) {
                ofy.getTxn().rollback();
            }
        }
        log.warning("Updated user: " + empKey);
    }
    
    public static void changePassword(Key<InetEmployee> empKey, String pwd) 
	throws MissingEntitiesException
{
    Objectify ofy = ObjectifyService.beginTransaction();
    try
    {
        //confirm new student obj doesn't exist already
        InetEmployee user = ofy.find(empKey);
        if (user == null) {
            MissingEntitiesException ex = new MissingEntitiesException(
                    "Unable to find info for: " + empKey);
            log.severe(ex.getMessage());
            throw ex;
        }
        
        user.setPassword(getHashPwd(pwd));

        //persist student object
        ofy.put(user);
        ofy.getTxn().commit();
    } finally {
        if (ofy.getTxn().isActive()) {
            ofy.getTxn().rollback();
        }
    }
}    
    
	private static void updateUserInfo(String fname, String lname, String email, 
			HashSet<String> phoneNums, InetEmployee user)
	{
		user.setFname(fname);
		user.setLname(lname);
		user.setEmail(email);
		user.setPhoneNumbers(phoneNums);
	}    
}
