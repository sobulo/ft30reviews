/**
 * 
 */
package com.ft360reviews.entities;

import java.util.HashSet;

import javax.persistence.Id;

import com.googlecode.objectify.Key;
import com.googlecode.objectify.annotation.Cached;
import com.googlecode.objectify.annotation.Indexed;
import com.googlecode.objectify.annotation.Unindexed;

/**
 * @author Segun Razaq Sobulo
 *
 */
@Unindexed
@Cached
public class InetEmployee implements GAEPrimaryEntity{
    @Id
    protected String key;

    private String fname;
    
    private String lname;

    @Indexed private String email;

    private HashSet<String> phoneNumbers;

    private String password;

    InetEmployee(){}

    InetEmployee(String fname, String lname, String email, HashSet<String> phoneNumbers,
            String loginName, String password)
    {
        setFname(fname);
        setLname(lname);
        setEmail(email == null? null :email.toLowerCase());
        setPassword(password);
        key = loginName.toLowerCase();
    }

    public String getPassword() {
        return password;
    }

    void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString()
    {
        return fname + " " + lname;
    }

    @Override
    public Key<InetEmployee> getKey() {
        return new Key<InetEmployee>(InetEmployee.class, key);
    }

    public String getLoginName()
    {
        return key;
    }

    public String getEmail() {
        return email;
    }

    void setEmail(String email) 
    {
    	if(email != null)
    		this.email = email.toLowerCase();
    }

    public String getFname() {
        return fname;
    }

    void setFname(String fname) {
        this.fname = fname;
    }

    public String getLname() {
        return lname;
    }

    void setLname(String lname) {
        this.lname = lname;
    }

    void setPhoneNumbers(HashSet<String> pn)
    {
        phoneNumbers = pn;
    }

    public HashSet<String> getPhoneNumbers()
    {
        return phoneNumbers;
    }
}
