/**
 * 
 */
package com.ft360reviews.entities;

import java.util.HashMap;

import javax.persistence.Id;

import com.googlecode.objectify.Key;
import com.googlecode.objectify.annotation.Cached;
import com.googlecode.objectify.annotation.Serialized;
import com.googlecode.objectify.annotation.Unindexed;

/**
 * @author Segun Razaq Sobulo
 *
 */
@Cached
@Unindexed
public class InetPreferences implements GAEPrimaryEntity{

	@Id
	String key;
	
	@Serialized
	HashMap<String, String> preferences;
	
	InetPreferences(){};
	
	InetPreferences(String key)
	{
		this.key = key;
	}
	
	public Key<InetPreferences> getKey()
	{
		return new Key<InetPreferences>(InetPreferences.class, key);
	}

	public HashMap<String, String> getPreferences() {
		return preferences;
	}

	public void setPreferences(HashMap<String, String> preferences) {
		this.preferences = preferences;
	}
}
