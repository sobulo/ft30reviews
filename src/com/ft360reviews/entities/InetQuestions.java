/**
 * 
 */
package com.ft360reviews.entities;

import java.util.HashMap;

import javax.persistence.Id;

import com.googlecode.objectify.Key;
import com.googlecode.objectify.annotation.Cached;
import com.googlecode.objectify.annotation.Serialized;
import com.googlecode.objectify.annotation.Unindexed;

/**
 * @author Segun Razaq Sobulo
 *
 */
@Unindexed
@Cached
public class InetQuestions implements GAEPrimaryEntity{
	@Id
	String key;
	
	@Serialized
	private HashMap<Integer, String> questionSet;
	
	Key<InetMultiAnswers> answerKey;
	
	InetQuestions(){}
	
	InetQuestions(String key, HashMap<Integer, String> questionSet, Key<InetMultiAnswers> answerKey)
	{
		this.key = key;
		this.questionSet = questionSet;
		this.answerKey = answerKey;
	}
	
	@SuppressWarnings("unchecked")
	public Key<InetQuestions> getKey()
	{
		return new Key<InetQuestions>(InetQuestions.class, key);
	}
	
	public HashMap<Integer, String> getQuestionSet()
	{
		return questionSet;
	}
	
	void setQuestionSet(HashMap<Integer, String> questionSet)
	{
		this.questionSet = questionSet;
	}
	
	public Key<InetMultiAnswers> getAnswerKey()
	{
		return answerKey;
	}
}
