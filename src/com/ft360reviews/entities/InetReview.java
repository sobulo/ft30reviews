/**
 * 
 */
package com.ft360reviews.entities;

import java.util.Date;
import java.util.HashMap;

import javax.persistence.Id;
import javax.persistence.PrePersist;

import com.google.appengine.api.datastore.Text;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.annotation.Cached;
import com.googlecode.objectify.annotation.Indexed;
import com.googlecode.objectify.annotation.Serialized;
import com.googlecode.objectify.annotation.Unindexed;

/**
 * @author Segun Razaq Sobulo
 *
 */
@Unindexed
@Cached
public class InetReview implements GAEPrimaryEntity{
	
	@Id
	String key;
	
	@Indexed 
	Key<InetEmployee> reviewer;	
	
	@Indexed
	Key<InetEmployee> reviewee;

	@Indexed
	Key<InetEmployee> writer;
	
	@Indexed
	String reviewCycle;
	
	@Indexed 
	String name;
	
	boolean allowRevieweeEdit; //used in cases where reviewee is initially null
	
	@Serialized
	HashMap<Integer, Integer> mulitchoiceAnswers;
	
	@Serialized
	HashMap<Integer, Text> textAnswers;
	
	Key<InetQuestions> questionSetKey;
	
	boolean locked;
	
	String adminComments;
	
	Date lastUpdated;
	@PrePersist void maintainLastUpdate() { lastUpdated = new Date(); }
	
	public final static String QUERY_PARAM_REVIEWER = "reviewer";
	public final static String QUERY_PARAM_REVIEWEE = "reviewee";
	public final static String QUERY_PARAM_WRITER = "writer";
	public final static String QUERY_PARAM_CYCLE = "reviewCycle";
	public final static String QUERY_PARAM_NAME = "name";
	
	InetReview(){}
	
	InetReview(String reviewCycle, Key<InetEmployee> reviewer, Key<InetEmployee> reviewee, 
				Key<InetQuestions> questionSetKey)
	{
		this.reviewer = reviewer;
		this.reviewee = reviewee;
		this.questionSetKey = questionSetKey;
		this.reviewCycle = reviewCycle;
		key = getKeyName();
	}
	
	InetReview(String reviewCycle, Key<InetEmployee> reviewer, String name, 
				Key<InetQuestions> questionSetKey, boolean allowRevieweeEdit)
	{
		this(reviewCycle, reviewer, null, questionSetKey);
		this.name = name;
		this.allowRevieweeEdit = allowRevieweeEdit;
		key = getKeyName();
	}
	
	public boolean isAllowRevieweeEdit()
	{
		return allowRevieweeEdit;
	}
	
	protected String getKeyName()
	{
		return (reviewee == null ? "" :reviewee.getName() + ".") + questionSetKey.getName() + "." + 
		reviewer.getName() + "." + (name == null? "" : name + ".") + reviewCycle;
	}
	public static String getKeyName(Key<InetEmployee> reviewee, Key<InetQuestions> questionSetKey,
			Key<InetEmployee> reviewer, String name, String reviewCycle)
	{
		return (reviewee == null ? "" :reviewee.getName() + ".") + questionSetKey.getName() + "." + 
		reviewer.getName() + "." + (name == null? "" : name + ".") + reviewCycle;		
	}
	public String getName()
	{
		return name;
	}
	
	public boolean isLocked()
	{
		return locked;
	}
	
	void setLocked(boolean locked)
	{
		this.locked = locked;
	}
	
	public String getAdminComments()
	{
		return adminComments;
	}
	
	void setAdminComments(String comments)
	{
		this.adminComments = comments;
	}

	/* (non-Javadoc)
	 * @see com.ft360reviews.entities.GAEPrimaryEntity#getKey()
	 */
	@Override
	public Key<? extends InetReview> getKey() {
		return new Key(this.getClass(), key);
	}

	public Key<InetEmployee> getReviewer() {
		return reviewer;
	}

	public void setReviewer(Key<InetEmployee> reviewer) {
		this.reviewer = reviewer;
	}

	public Key<InetEmployee> getReviewee() {
		return reviewee;
	}

	public void setReviewee(Key<InetEmployee> reviewee) {
		this.reviewee = reviewee;
	}

	public Key<InetEmployee> getWriter() {
		return writer;
	}

	public void setWriter(Key<InetEmployee> writer) {
		this.writer = writer;
	}

	public String getReviewCycle() {
		return reviewCycle;
	}

	public HashMap<Integer, Integer> getMulitchoiceAnswers() {
		return mulitchoiceAnswers;
	}

	void setMulitchoiceAnswers(HashMap<Integer, Integer> mulitchoiceAnswers) {
		this.mulitchoiceAnswers = mulitchoiceAnswers;
	}

	public HashMap<Integer, Text> getTextAnswers() {
		return textAnswers;
	}

	void setTextAnswers(HashMap<Integer, Text> textAnswers) {
		this.textAnswers = textAnswers;
	}

	public Key<InetQuestions> getQuestionSetKey() {
		return questionSetKey;
	}

	void setQuestionSetKey(Key<InetQuestions> questionSetKey) {
		this.questionSetKey = questionSetKey;
	}
	
	public Date getLastUpdated()
	{
		return lastUpdated;
	}
}
