/**
 * 
 */
package com.ft360reviews.entities;

import com.googlecode.objectify.Key;

/**
 * @author Segun Razaq Sobulo
 *
 */
public class InetReviewSummary extends InetReview{
	
	InetReviewSummary(){}
	
	InetReviewSummary(String reviewCycle, Key<InetEmployee> reviewer, Key<InetEmployee> reviewee, Key<InetQuestions> questionSetKey)
	{
		super(reviewCycle, reviewer, reviewee, questionSetKey);
	}
	
	InetReviewSummary(String reviewCycle, Key<InetEmployee> reviewer, String name, Key<InetQuestions> questionSetKey)
	{
		super(reviewCycle, reviewer, name, questionSetKey, false);		
	}
	
	@Override
	protected String getKeyName()
	{
		return reviewee == null? getKeyName(name, reviewCycle) : getKeyName(reviewee.getName(), reviewCycle);
	}
	
	public static String getKeyName(String name, String reviewCycle)
	{
		return name + "." + reviewCycle + ".Review Summary";
	}
}
