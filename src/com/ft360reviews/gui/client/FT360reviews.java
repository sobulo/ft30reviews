package com.ft360reviews.gui.client;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.user.client.ui.RootLayoutPanel;

/**
 * Entry point classes define <code>onModuleLoad()</code>.
 */
public class FT360reviews implements EntryPoint {
    /** 
     * Creates a new instance of WebAppEntryPoint
     */
    public FT360reviews() {
    }

    /** 
     * The entry point method, called automatically by loading a module
     * that declares an implementing class as an entry-point
     */
    public void onModuleLoad() {
        RootLayoutPanel.get().add(new WebAppPanel());
    }
}

