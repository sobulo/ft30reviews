/**
 * 
 */
package com.ft360reviews.gui.client;

import com.google.gwt.core.client.GWT;

/**
 * @author Segun Razaq Sobulo
 *
 */
public class GUIConstants {
    //Images Folder
    public final static String MODULE_IMAGE_FOLDER = GWT.getModuleBaseURL() + "images/";
    public final static String DP_CLOSED_IMG = MODULE_IMAGE_FOLDER + "dpclose.jpg";
    public final static String DP_OPENED_IMG = MODULE_IMAGE_FOLDER + "dpopen.jpg";
    public final static String COMPANY_IMAGE_FOLDER = MODULE_IMAGE_FOLDER + "company/";
    //public final static String IMG_DEFAULT_EXT = ".gif"; //TODO, upload school logs to blob-store once billing enabled
    public final static String[] FT360_LOGOS = {"ft360alternatesmall.jpg", "appengine.gif"};
    
    public final static int DEFAULT_PHONE_PER_USER = 3;
    public final static String DEFAULT_LB_WIDTH = "250px";
    public final static int DEFAULT_LB_VISIBLE_COUNT = 10; 
    
    public final static String DEFAULT_STACK_WIDTH = "100%";
    public final static String DEFAULT_STACK_HEIGHT = "100%";  
    public final static Double DEFAULT_STACK_HEADER = 30.0;
    
    public final static String DEFAULT_TAB_WIDTH = "100%";
    public final static String DEFAULT_TAB_HEIGHT = "1200px";  
    public final static Double DEFAULT_TAB_HEADER = 40.0; 
    
    //styles
    public final static String STYLE_STND_TABLE_HEADER = "standardTableHeader";
    public final static String STYLE_STND_TABLE = "standardTable";
    public final static String STYLE_CENTER = "centerElements";
}
