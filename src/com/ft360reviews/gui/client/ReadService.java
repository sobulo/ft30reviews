package com.ft360reviews.gui.client;

import java.util.HashMap;

import com.ft360reviews.gui.shared.LoginInfo;
import com.ft360reviews.gui.shared.ReviewDTO;
import com.ft360reviews.gui.shared.ReviewSummaryDTO;
import com.ft360reviews.gui.shared.TableMessage;
import com.ft360reviews.gui.shared.exceptions.DuplicateEntitiesException;
import com.ft360reviews.gui.shared.exceptions.LoginValidationException;
import com.ft360reviews.gui.shared.exceptions.MissingEntitiesException;
import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

/**
 * The client side stub for the RPC service.
 */
@RemoteServiceRelativePath("read")
public interface ReadService extends RemoteService {
    public LoginInfo login(String requestUri) throws MissingEntitiesException,
    LoginValidationException;
    public TableMessage getEmployeeInfo() throws MissingEntitiesException, LoginValidationException;
    public HashMap<String, String> getEmployeeNamesToBeReviewed(boolean isSummary) throws LoginValidationException, MissingEntitiesException;
    public ReviewDTO getReview(String reviewId) throws MissingEntitiesException;
    public HashMap<String, String> getAllEmployeeIDs();
    public HashMap<String, String> getQuestionIDs();
    public ReviewSummaryDTO getReviewSummary(String summaryId) throws MissingEntitiesException, DuplicateEntitiesException;
    public HashMap<String, String> getSummaryIDs(String loginName);
    public boolean isAdmin() throws LoginValidationException;
    public HashMap<String, String> getNoticeIDs();
    public String[] getNotice(String id) throws MissingEntitiesException;
}
