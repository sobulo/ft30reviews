package com.ft360reviews.gui.client;

import java.util.HashMap;

import com.ft360reviews.gui.shared.LoginInfo;
import com.ft360reviews.gui.shared.ReviewDTO;
import com.ft360reviews.gui.shared.ReviewSummaryDTO;
import com.ft360reviews.gui.shared.TableMessage;
import com.google.gwt.user.client.rpc.AsyncCallback;

/**
 * The async counterpart of <code>GreetingService</code>.
 */
public interface ReadServiceAsync {
	void login(String requestUri, AsyncCallback<LoginInfo> callback);

	void getEmployeeInfo(AsyncCallback<TableMessage> callback);

	void getEmployeeNamesToBeReviewed(boolean isSummary,
			AsyncCallback<HashMap<String, String>> callback);

	void getReview(String reviewId, AsyncCallback<ReviewDTO> callback);

	void getAllEmployeeIDs(AsyncCallback<HashMap<String, String>> callback);

	void getReviewSummary(String summaryId,
			AsyncCallback<ReviewSummaryDTO> callback);

	void getQuestionIDs(AsyncCallback<HashMap<String, String>> callback);

	void getSummaryIDs(String loginId, AsyncCallback<HashMap<String, String>> callback);

	void isAdmin(AsyncCallback<Boolean> callback);

	void getNotice(String id, AsyncCallback<String[]> callback);

	void getNoticeIDs(AsyncCallback<HashMap<String, String>> callback);
}
