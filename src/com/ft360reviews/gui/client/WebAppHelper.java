/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ft360reviews.gui.client;

import com.ft360reviews.gui.client.reviews.AdminEditPanel;
import com.ft360reviews.gui.client.reviews.GroupedReviewsPanel;
import com.ft360reviews.gui.client.reviews.GroupedSummaryReviewsPanel;
import com.ft360reviews.gui.client.reviews.SearchSummaryPanel;
import com.ft360reviews.gui.client.reviews.ViewQuestionsPanel;
import com.ft360reviews.gui.client.tools.NoticeBoardPanel;
import com.ft360reviews.gui.client.users.PasswordChangePanel;
import com.ft360reviews.gui.client.users.UpdateEmployeePanel;
import com.ft360reviews.gui.shared.LoginInfo;
import com.ft360reviews.gui.shared.PanelServiceConstants;
import com.ft360reviews.gui.shared.exceptions.LoginValidationException;
import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Anchor;
import com.google.gwt.user.client.ui.Hyperlink;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.ScrollPanel;
import com.google.gwt.user.client.ui.StackLayoutPanel;
import com.google.gwt.user.client.ui.TabLayoutPanel;
import com.google.gwt.user.client.ui.Tree;
import com.google.gwt.user.client.ui.TreeItem;
import com.google.gwt.user.client.ui.Widget;

/**
 *
 * @author Segun Razaq Sobulo
 */
public class WebAppHelper {

    private static int indexCounter = 0;
    private final static int WELCOME_IDX = indexCounter++;
    private final static int MY_ACCT_IDX = indexCounter++;
    public final static int REVIEW_IDX = indexCounter++;
    public final static int SUMMARY_IDX = indexCounter++;    
    public final static int QUESTION_IDX = indexCounter++;
    public final static int SEARCH_SUMMARY_IDX = indexCounter++;
    public final static int NOTICE_BOARD_IDX = indexCounter++;
    public final static int EDIT_SUMMARY_IDX = indexCounter++;    

    private static HyperlinkedPanel[] appPanels = new HyperlinkedPanel[indexCounter];

    //TODO static management of login state seems tacky, might want to replace
    //with gwt event bus logic
    private static LoginInfo loginInfo;
    
    //initiate rpc services
    private static ReadServiceAsync readService = GWT.create(ReadService.class);
    private static WriteServiceAsync writeService = GWT.create(WriteService.class);
    
    public static ReadServiceAsync getReadService()
    {
    	return readService;
    }
    
    public static WriteServiceAsync getWriteService()
    {
    	return writeService;
    }    
    
    static Image getCompanyImage()
    {
    	return new Image(GUIConstants.COMPANY_IMAGE_FOLDER + 
            		PanelServiceConstants.IMAGES_NAMES.get(loginInfo.getCompanyAccronym().toLowerCase().trim()));    		
    }
    
    static String getCompanyAccronym()
    {
    	return loginInfo.getCompanyAccronym();
    }

    static HyperlinkedPanel[] initializePanels() {
        GWT.log("loading static block");
        //welcome panel
        appPanels[WELCOME_IDX] = new HyperlinkedPanel() {

            private Hyperlink link;
            private WelcomePanel welcomePanel;

            @Override
            public Hyperlink getLink() {
                if (link == null) {
                    link = new Hyperlink("Welcome", "welcome");
                }
                return link;
            }

            @Override
            public Widget getPanelWidget() {
                if (welcomePanel == null) {
                    welcomePanel = new WelcomePanel();
                }
                return welcomePanel;
            }
        };
        
        appPanels[MY_ACCT_IDX] = new HyperlinkedPanel() {

            private Hyperlink link;
            private Widget myacctpanel;        

            @Override
            public Hyperlink getLink() {
                if (link == null) {
                    link = new Hyperlink("My Account", "myaccount");
                }
                return link;
            }

            @Override
            public Widget getPanelWidget() {
                if (myacctpanel == null) 
                {
                	/*int numOfWidgets = 2;
                	Widget[] widgets = new Widget[numOfWidgets];
                	String[] headers = new String[numOfWidgets];
                	widgets[0] = new UpdateEmployeePanel();
                    headers[0] = "Information";
                    widgets[1] = new PasswordChangePanel();
                    headers[1] = "Change Password";*/
                    myacctpanel = new UpdateEmployeePanel();
                }
                return myacctpanel;
            }
        };
        
        appPanels[REVIEW_IDX] = new HyperlinkedPanel() {

            private Hyperlink link;
            private Widget reviewPanel;

            @Override
            public Hyperlink getLink() {
                if (link == null) {
                    link = new Hyperlink("Write Reviews", "write_review");
                }
                return link;
            }

            @Override
            public Widget getPanelWidget() {
                if (reviewPanel == null) {
                	reviewPanel = new GroupedReviewsPanel();
                }
                return reviewPanel;
            }
        };

        appPanels[NOTICE_BOARD_IDX] = new HyperlinkedPanel() {

            private Hyperlink link;
            private Widget noticePanel;

            @Override
            public Hyperlink getLink() {
                if (link == null) {
                    link = new Hyperlink("Notice Board", "notice_board");
                }
                return link;
            }

            @Override
            public Widget getPanelWidget() {
                if (noticePanel == null) {
                	noticePanel = new NoticeBoardPanel();
                }
                return noticePanel;
            }
        };        
        
        appPanels[QUESTION_IDX] = new HyperlinkedPanel() {

            private Hyperlink link;
            private Widget questionsPanel;

            @Override
            public Hyperlink getLink() {
                if (link == null) {
                    link = new Hyperlink("View/Print Questions", "view_questions");
                }
                return link;
            }

            @Override
            public Widget getPanelWidget() {
                if (questionsPanel == null) {
                	questionsPanel = new ViewQuestionsPanel();
                }
                return questionsPanel;
            }
        };           
        
        
        appPanels[SUMMARY_IDX] = new HyperlinkedPanel() {

            private Hyperlink link;
            private Widget reviewPanel;

            @Override
            public Hyperlink getLink() {
                if (link == null) {
                    link = new Hyperlink("Write Summaries", "write_summary");
                }
                return link;
            }

            @Override
            public Widget getPanelWidget() {
                if (reviewPanel == null) {
                	reviewPanel = new GroupedSummaryReviewsPanel();
                }
                return reviewPanel;
            }
        };
        
        appPanels[SEARCH_SUMMARY_IDX] = new HyperlinkedPanel() {

            private Hyperlink link;
            private Widget reviewPanel;

            @Override
            public Hyperlink getLink() {
                if (link == null) {
                    link = new Hyperlink("Search Summaries", "search_summary");
                }
                return link;
            }

            @Override
            public Widget getPanelWidget() {
                if (reviewPanel == null) {
                	reviewPanel = new SearchSummaryPanel();
                }
                return reviewPanel;
            }
        };
        
        appPanels[EDIT_SUMMARY_IDX] = new HyperlinkedPanel() {

            private Hyperlink link;
            private Widget reviewPanel;

            @Override
            public Hyperlink getLink() {
                if (link == null) {
                    link = new Hyperlink("Edit Summaries", "admin_edit_summary");
                }
                return link;
            }

            @Override
            public Widget getPanelWidget() {
                if (reviewPanel == null) {
                	reviewPanel = new AdminEditPanel();
                }
                return reviewPanel;
            }
        };         
        
        return appPanels;
    }
    
    static Anchor getLogoutUrl()
    {
    	Anchor loginLink = new Anchor("Logout", loginInfo.getLogoutUrl());
    	return loginLink;
    }

    static Tree getMenuTree() {
        GWT.log("calling gettree");

        
        //logout menu item
        Tree navtree = new Tree();
        navtree.add(getLogoutUrl());
        
        //welcome menu item
        TreeItem welcomenav = new TreeItem(appPanels[WELCOME_IDX].getLink());
        navtree.addItem(welcomenav);
        navtree.addItem(appPanels[MY_ACCT_IDX].getLink());
        navtree.addItem(appPanels[REVIEW_IDX].getLink());
        navtree.addItem(appPanels[SUMMARY_IDX].getLink());
        navtree.addItem(appPanels[QUESTION_IDX].getLink());
        
        return navtree;
    }

    static int getWelcomeScreenIndex() {
        return WELCOME_IDX;
    }

    public static void setLoginInfo(LoginInfo info) {
        loginInfo = info;
    }

    public static void checkForLoginError(Throwable error) {
        if (error instanceof LoginValidationException)
        {
            Window.alert(error.getMessage());
            String url = GWT.getHostPageBaseURL();
            if(loginInfo != null && loginInfo.isLoggedIn())
                url = loginInfo.getLogoutUrl();
            Window.Location.replace(url);
        }
    }
    
	public static StackLayoutPanel getStackLayout(Widget[]widgets, String[] headers)
	{
		StackLayoutPanel result = new StackLayoutPanel(Unit.PX);
		//either this or add to a simple panel with size set
		result.setHeight(GUIConstants.DEFAULT_STACK_HEIGHT);
		result.setWidth(GUIConstants.DEFAULT_STACK_WIDTH);
		
		for(int i = 0; i < widgets.length; i++)
			result.add(new ScrollPanel(widgets[i]), 
					headers[i], GUIConstants.DEFAULT_STACK_HEADER);
		return result;
	}
	
	public static TabLayoutPanel getTabLayout(Widget[] widgets, String[]headers)
	{
		TabLayoutPanel result = new TabLayoutPanel(GUIConstants.DEFAULT_STACK_HEADER, Unit.PX);
		result.setHeight(GUIConstants.DEFAULT_STACK_HEIGHT);
		result.setWidth(GUIConstants.DEFAULT_STACK_WIDTH);		
		for(int i = 0; i < widgets.length; i++)
			result.add(new ScrollPanel(widgets[i]), headers[i]);
		return result;
	}
	
	public static Hyperlink[] getSuperUserLinks()
	{
		//line below doesn't belong here, the one commented out should replace it in new cycle code
		Hyperlink[] result = {appPanels[EDIT_SUMMARY_IDX].getLink()};
		return result;
	}
	
	public static Hyperlink[] getAdminLinks()
	{
		Hyperlink[] result = {appPanels[SEARCH_SUMMARY_IDX].getLink(), appPanels[NOTICE_BOARD_IDX].getLink()};
		return result;
	}
	
	public static boolean isSuperUserLogin()
	{
		//TODO, not the best way to check admin status, should ideally go back to server but this will catch most cases
		return loginInfo.isAdmin();
	}
}
