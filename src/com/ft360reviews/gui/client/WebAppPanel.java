package com.ft360reviews.gui.client;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import com.ft360reviews.gui.shared.LoginInfo;
import com.ft360reviews.gui.shared.exceptions.LoginValidationException;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.History;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Hyperlink;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.LayoutPanel;
import com.google.gwt.user.client.ui.ResizeComposite;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.Tree;
import com.google.gwt.user.client.ui.TreeItem;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;

/**
 *
 * @author Administrator
 */
public class WebAppPanel extends ResizeComposite implements ValueChangeHandler<String> 
{
    @UiField
    SimplePanel navcontent;
    @UiField
    LayoutPanel content;
    @UiField
    VerticalPanel ft360Logos;
    @UiField
    SimplePanel companyLogo;
    
    private static WebAppPanelUiBinder uiBinder = GWT.create(WebAppPanelUiBinder.class);
    private String newToken;


    //different views
    Widget currentContent; //keeps track of view currently displayed
    HyperlinkedPanel[] appPanels;
    WelcomePanel welcomePanel;
    
    private ReadServiceAsync readService = WebAppHelper.getReadService();
    
    // Create an asynchronous callback to handle the result.
    final AsyncCallback<LoginInfo> loginCallback = new AsyncCallback<LoginInfo>() {

        @Override
        public void onSuccess(LoginInfo result) {
            WebAppHelper.setLoginInfo(result);
            Image m = WebAppHelper.getCompanyImage();
            companyLogo.add(m);
            welcomePanel.setSchoolHeader(result.getCompanyName());
            if (result.isLoggedIn()) {
                welcomePanel.setLoginLabel("Welcome: " + result.getName() + ". Click on link below to signout.");
                welcomePanel.setAnchor("Sign Out", result.getLogoutUrl());
                setupMenu();
            } else {
                welcomePanel.setLoginLabel(result.getMessage());
                welcomePanel.setAnchor(result.getLoginUrl());
            }
        }

        @Override
        public void onFailure(Throwable caught) {
            welcomePanel.setLoginLabel("Unable to reach server, click on link" +
                    " to retry. Error was: [" + caught.getMessage() + "]");
            welcomePanel.setAnchor("Retry", GWT.getHostPageBaseURL());
        }
    };

    final AsyncCallback<LoginInfo> ensureLoginCallback = new AsyncCallback<LoginInfo>() {

        @Override
        public void onSuccess(LoginInfo result) {
            WebAppHelper.setLoginInfo(result);
            if (!result.isLoggedIn()) {
                Window.alert("You've been signed out. For security/privacy reasons the system logs you out after " +
                        "an hour of inactivity. You'll need to login again");

                //since logged out on server, immediately clear client state
                Window.Location.replace(GWT.getHostPageBaseURL());
            }
            else
                loadNewPanel();
        }

        @Override
        public void onFailure(Throwable caught) {
            welcomePanel.setLoginLabel("Unable to reach server, click on link" +
                    " to retry. Error was: " + caught.getMessage() + "]");
            welcomePanel.setAnchor("Retry", GWT.getHostPageBaseURL());
        }
    };
    
    final AsyncCallback<Boolean> adminSetupback = new AsyncCallback<Boolean>() {

        @Override
        public void onSuccess(Boolean result) {
        	
        	if(!result && !WebAppHelper.isSuperUserLogin())
        		return;
        	
            Tree navtree = (Tree) navcontent.getWidget();
            TreeItem adminNav  = new TreeItem("Admin");            
        	
            if(result)
            {
                Hyperlink[] adminLinks = WebAppHelper.getAdminLinks();
                for(Hyperlink link : adminLinks)
                	adminNav.addItem(link);
            }
        	
            if(WebAppHelper.isSuperUserLogin())
            {
                Hyperlink[] adminLinks = WebAppHelper.getSuperUserLinks();
                for(Hyperlink link : adminLinks)
                	adminNav.addItem(link);            	
            }
            
            navtree.addItem(adminNav);
        	
        }

        @Override
        public void onFailure(Throwable caught) 
        {
        	if(caught instanceof LoginValidationException)
        	{
                Window.alert("You've been signed out. For security/privacy reasons the system logs you out after " +
                "an hour of inactivity. You'll need to login again");

		        //since logged out on server, immediately clear client state
		        Window.Location.replace(GWT.getHostPageBaseURL());        		
        	}
        	else
        		Window.alert("Navigation bar might not have been completely setup. As per: " + caught.getMessage());
        }
    };    

    interface WebAppPanelUiBinder extends UiBinder<Widget, WebAppPanel> {
    }

    public void setupMenu() {
        GWT.log("adding menu");
        Tree navtree = WebAppHelper.getMenuTree();
        navcontent.add(navtree);
        
        //check whether admin links need to be added to nav panel
        readService.isAdmin(adminSetupback);
        
        //setup hyperlink handler
        int welcomeIndex = WebAppHelper.getWelcomeScreenIndex();
        History.addValueChangeHandler(this);
        String initToken = History.getToken();
        if (initToken.length() == 0) {
            History.newItem(appPanels[welcomeIndex].getLink().
                    getTargetHistoryToken());
        }
        //setup init state
        History.fireCurrentHistoryState();
    }

    public WebAppPanel() {
        //GWT.log("initializing webapp panel");
        initWidget(uiBinder.createAndBindUi(this));
        
        //setup grep logos
        for(String logoUrl : GUIConstants.FT360_LOGOS)
        	ft360Logos.add(new Image(GUIConstants.MODULE_IMAGE_FOLDER + logoUrl));
        
        //setup welcome screen
        int welcomeIndex = WebAppHelper.getWelcomeScreenIndex();

        appPanels = WebAppHelper.initializePanels();

        //GWT.log("initialized panels");
        currentContent = appPanels[welcomeIndex].getPanelWidget();
        
        //GWT.log("retrieved current content");
        welcomePanel = (WelcomePanel) currentContent;

        //GWT.log("referenced welcome panel");

        content.add(currentContent);
        readService.login(GWT.getHostPageBaseURL(), loginCallback);
    }

    @Override
    public void onValueChange(ValueChangeEvent<String> event) {
        newToken = event.getValue();
        readService.login(GWT.getHostPageBaseURL(), ensureLoginCallback);
    }

    private void loadNewPanel()
    {
        Widget newContent = null;
        
        //determine what widget user link points to
        for (HyperlinkedPanel cursorPanel : appPanels) {
            if (newToken.equals(cursorPanel.getLink().getTargetHistoryToken())) {
            		newContent = cursorPanel.getPanelWidget();
            }
        }

        //update current widget to widget specified by user above
        if (newContent != null && currentContent != newContent) {
            content.clear();
            content.add(newContent);
            currentContent = newContent;
        }
    }
}
