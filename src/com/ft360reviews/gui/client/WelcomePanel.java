/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ft360reviews.gui.client;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import com.ft360reviews.gui.shared.PanelServiceConstants;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Anchor;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;

/**
 *
 * @author Administrator
 */
public class WelcomePanel extends Composite{

    @UiField HTML loginLabel;
    @UiField HTML noticePanel;    
    @UiField HorizontalPanel linkPanel;
    @UiField Label header;
    
    private static WelcomePanelUiBinder uiBinder = GWT.create(WelcomePanelUiBinder.class);
    private ReadServiceAsync readService = WebAppHelper.getReadService();
    
    interface WelcomePanelUiBinder extends UiBinder<Widget, WelcomePanel> {
    }

    // Create an asynchronous callback to handle the result.
    final AsyncCallback<String[]> noticeCallback = new AsyncCallback<String[]>() {

        @Override
        public void onSuccess(String[] result) {
        	noticePanel.setHTML("<b><i>" + result[0] + "</i></b>" + result[1]);
        }

        @Override
        public void onFailure(Throwable caught) {
        	String output = "<b>Error loading notice board: </b><br/>" + caught.getMessage();
        	noticePanel.setHTML(output);
        }
    };    
    
    public WelcomePanel() {
        initWidget(uiBinder.createAndBindUi(this));
        linkPanel.setSpacing(10);
    }

    public void setLoginLabel(String label)
    {
        loginLabel.setHTML(label);
    }

    public void setAnchor(String label, String url)
    {
        linkPanel.clear();
        Anchor logoutLink = new Anchor(label, url);
        linkPanel.add(logoutLink);
        readService.getNotice(PanelServiceConstants.LOGGEDIN_NOTICE, noticeCallback);
    }

    public void setAnchor(HashMap<String, String> linkInfo)
    {
        linkPanel.clear();
        Iterator<Map.Entry<String, String>> itr = linkInfo.entrySet().iterator();
        while(itr.hasNext())
        {
            Map.Entry<String, String> linkEntry = itr.next();
            
            VerticalPanel loginLogo = getLoginProviderPanel(linkEntry.getKey(), linkEntry.getValue());
            linkPanel.add(loginLogo);
            linkPanel.setCellVerticalAlignment(loginLogo, VerticalPanel.ALIGN_MIDDLE);
        }
        readService.getNotice(PanelServiceConstants.LOGGEDOUT_NOTICE, noticeCallback);
    }
    
    private VerticalPanel getLoginProviderPanel(final String provider, final String providerUrl)
    {
    	VerticalPanel contentPanel = new VerticalPanel();
    	Image logo;
    	Anchor link;
    	if(provider.equals(PanelServiceConstants.PROVIDER_FT360))
    	{
    		//for grep provider, use the school logo
    		logo = new Image(GUIConstants.MODULE_IMAGE_FOLDER + 
    				PanelServiceConstants.IMAGES_NAMES.get(PanelServiceConstants.PROVIDER_FT360));
    		link = new Anchor(WebAppHelper.getCompanyAccronym().toUpperCase() + 
    				" Login", providerUrl);
    	}
    	else
    	{
    		logo = new Image(GUIConstants.MODULE_IMAGE_FOLDER + 
    				PanelServiceConstants.IMAGES_NAMES.get(PanelServiceConstants.PROVIDER_GOOGLE));
    		link = new Anchor(provider.toUpperCase() + " Login", providerUrl);  		
    	}
    	
    	logo.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				Window.Location.replace(providerUrl);
			}
		});
		contentPanel.add(logo);
		contentPanel.add(link);
		contentPanel.setCellHorizontalAlignment(link, HorizontalPanel.ALIGN_CENTER);		
    	return contentPanel;
    }
    
    public void setSchoolHeader(String schoolName)
    {
    	header.setText(schoolName);
    }
}