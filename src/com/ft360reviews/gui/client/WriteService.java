/**
 * 
 */
package com.ft360reviews.gui.client;



import java.util.HashMap;
import java.util.HashSet;

import com.ft360reviews.gui.shared.exceptions.DuplicateEntitiesException;
import com.ft360reviews.gui.shared.exceptions.LoginValidationException;
import com.ft360reviews.gui.shared.exceptions.MissingEntitiesException;
import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

/**
 * @author Segun Razaq Sobulo
 *
 */
@RemoteServiceRelativePath("write")
public interface WriteService extends RemoteService{
    public void updateEmployee(String fname, String lname, String email, 
    		HashSet<String> phoneNums) throws MissingEntitiesException, LoginValidationException;
    public String changePassword(String oldPassword, String newPassword) 
    	throws MissingEntitiesException, LoginValidationException;
    public String updateReview(String reviewId, String revieweeId, HashMap<Integer, Integer> multiChoiceAns, 
    		HashMap<Integer, String> textAns, boolean locked, String comments) 
    	throws MissingEntitiesException, LoginValidationException, DuplicateEntitiesException;
    public void updateNotice(String id, String title, String content) throws MissingEntitiesException;
}
