/**
 * 
 */
package com.ft360reviews.gui.client;

import java.util.HashMap;
import java.util.HashSet;

import com.google.gwt.user.client.rpc.AsyncCallback;

/**
 * @author Segun Razaq Sobulo
 *
 */
public interface WriteServiceAsync {
	void updateEmployee(String fname, String lname, String email,
			HashSet<String> phoneNums, AsyncCallback<Void> callback);

	void changePassword(String oldPassword, String newPassword,
			AsyncCallback<String> callback);

	void updateReview(String reviewId,String revieweeId,
			HashMap<Integer, Integer> multiChoiceAns,
			HashMap<Integer, String> textAns, boolean locked, String comments,
			AsyncCallback<String> callback);

	void updateNotice(String id, String title, String content,
			AsyncCallback<Void> callback);
}
