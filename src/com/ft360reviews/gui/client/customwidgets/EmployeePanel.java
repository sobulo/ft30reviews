/**
 * 
 */
package com.ft360reviews.gui.client.customwidgets;

import java.util.ArrayList;

import com.ft360reviews.gui.shared.PanelServiceConstants;
import com.google.gwt.core.client.GWT;
import com.google.gwt.regexp.shared.RegExp;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Widget;

/**
 * @author Segun Razaq Sobulo
 *
 */
public class EmployeePanel extends Composite
{
    @UiField TextBox firstName;
    @UiField TextBox lastName;

    private static EmployeePanelUiBinder uiBinder = GWT.create(EmployeePanelUiBinder.class);
    private boolean passwordValidation = true;
    private static final RegExp EMAIL_MATCHER = RegExp.compile(PanelServiceConstants.REGEX_EMAIL, "i");
    private static final RegExp PHONE_NUM_MATCHER = RegExp.compile(PanelServiceConstants.REGEX_NUMS_ONLY);
    
    public EmployeePanel()
    {
        initWidget(uiBinder.createAndBindUi(this));
        disableNameEdits();
    }
    
    
    public void disableNameEdits()
    {
    	firstName.setEnabled(false);
    	lastName.setEnabled(false);
    }

    public String getFirstName() {
        return firstName.getValue().trim();
    }

    public String getLastName() {
        return lastName.getValue().trim();
    }

    public void setFirstName(String fnm) {
        firstName.setValue(fnm);
    }

    public void setLastName(String lnm) {
        lastName.setValue(lnm);
    }
    
    public boolean isValidEmail(String email)
    {
    	return EMAIL_MATCHER.exec(email) != null;
    }
    
    public boolean isValidNum(String num)
    {
    	return PHONE_NUM_MATCHER.exec(num.replaceAll(PanelServiceConstants.REGEX_PHONE_REPLACE, "")) != null;
    }

    public void clear()
    {
        firstName.setValue(null);
        lastName.setValue(null);
    }

    public ArrayList<String> validateFields()
    {
        ArrayList<String> result = new ArrayList<String>();
        
        if(getFirstName().length() == 0)
        	result.add("First name field is blank");
        
        if(getLastName().length() == 0)
        	result.add("Last name field is blank");
                
        return result;
    }

    interface EmployeePanelUiBinder extends UiBinder<Widget, EmployeePanel> {
    }
}
