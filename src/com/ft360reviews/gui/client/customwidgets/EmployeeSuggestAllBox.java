/**
 * 
 */
package com.ft360reviews.gui.client.customwidgets;

import com.ft360reviews.gui.client.WebAppHelper;

/**
 * @author Segun Razaq Sobulo
 *
 */
public class EmployeeSuggestAllBox extends EmployeeSuggestBox{
	public EmployeeSuggestAllBox(String label)
	{
		super();
		this.label.setText(label);
	}
	
	public EmployeeSuggestAllBox()
	{
		super();
		this.label.setText("Select Employee");
	}	

	@Override
	public void populateSuggestBox()
	{
		WebAppHelper.getReadService().getAllEmployeeIDs(getPopulateCallBack());
	}

	/* (non-Javadoc)
	 * @see j9educationgwtgui.client.custom.widgets.UserSuggestBox#getRole()
	 */
	@Override
	public String getRole() 
	{
		return "ALL";
	}
}
