/**
 * 
 */
package com.ft360reviews.gui.client.customwidgets;

import com.ft360reviews.gui.client.customwidgets.richtext.RichTextToolbar;
import com.google.gwt.user.client.ui.RichTextArea;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;

/**
 * @author Segun Razaq Sobulo
 *
 */
public class FreeFormPanelGenerator implements Question{
	RichTextArea area;
	int questionNumber;
	
	public FreeFormPanelGenerator(int questionNumber)
	{	
		this.questionNumber = questionNumber;
	}
	
	public Widget getFreeFormPanel(String question)
	{
		area = new RichTextArea();
	    RichTextToolbar tb = new RichTextToolbar(area, questionNumber + ". " + question);
	    VerticalPanel p = new VerticalPanel();
	    p.add(tb);
	    p.add(area);
	    
	    area.setHeight("14em");
	    area.setWidth("99%");
	    tb.setWidth("100%");
	    p.setWidth("100%");		
	    return p;	
	}
	
	@Override
	public String getAnswer()
	{
		return area.getHTML();
	}
	
	@Override
	public int getQuestionNumber()
	{
		return questionNumber;
	}

	/* (non-Javadoc)
	 * @see com.ft360reviews.gui.client.customwidgets.Question#setAnswer(java.lang.String)
	 */
	@Override
	public void setAnswer(String answer) {
		area.setHTML(answer);
	}
}
