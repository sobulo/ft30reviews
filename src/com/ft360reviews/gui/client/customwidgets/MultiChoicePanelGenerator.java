/**
 * 
 */
package com.ft360reviews.gui.client.customwidgets;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Map;

import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.RadioButton;
import com.google.gwt.user.client.ui.VerticalPanel;

/**
 * @author Segun Razaq Sobulo
 *
 */
public class MultiChoicePanelGenerator implements Question{

	RadioButton[] rbList;
	int questionNumber;
	public MultiChoicePanelGenerator(int questionNum)
	{
		questionNumber = questionNum;
	}
	
	public VerticalPanel getMultiChoicePanel(String groupPrefix, String question, Map<Integer, String> options)
	{
		//setup radio button group
		ArrayList<Integer> keys = new ArrayList<Integer>();
		keys.addAll(options.keySet());
		Collections.sort(keys);
		RadioButton rb;
		int count = 0;
		HorizontalPanel radioGroup = new HorizontalPanel();	
		//radioGroup.setBorderWidth(1);
		radioGroup.setSpacing(5);
		rbList = new RadioButton[options.size()];
		for(int i : keys)
		{
			 rb = new RadioButton(groupPrefix + String.valueOf(questionNumber), options.get(i));
			 rb.setFormValue(String.valueOf(i));
			 rbList[count++] = rb;
			 radioGroup.add(rb);
	
		}		
					
		//combine with question and return
		VerticalPanel choicePanel = new VerticalPanel();
		HTML questLabel = new HTML("<b>" + questionNumber + ". " + question + "</b>");
		choicePanel.add(questLabel);
		choicePanel.add(radioGroup);
		//questLabel.setWidth("100%");
		//radioGroup.setWidth("100%");
		//choicePanel.setWidth("100%");
		return choicePanel;
	}
	
	@Override
	public String getAnswer()
	{
		for(int i = 0; i < rbList.length; i++)
			if(rbList[i].getValue())
				return rbList[i].getFormValue();
		return null;
	}
	
	@Override
	public int getQuestionNumber()
	{
		return questionNumber;
	}

	/* (non-Javadoc)
	 * @see com.ft360reviews.gui.client.customwidgets.Question#setAnswer(java.lang.String)
	 */
	@Override
	public void setAnswer(String answer) {
		for(RadioButton rb : rbList)
			if(rb.getFormValue().equals(answer))
			{
				rb.setValue(true);
				break;
			}
	}
}
