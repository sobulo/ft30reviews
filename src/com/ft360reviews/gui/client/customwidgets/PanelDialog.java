/**
 * 
 */
package com.ft360reviews.gui.client.customwidgets;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.ScrollPanel;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;

/**
 * @author Segun Razaq Sobulo
 *
 */
public class PanelDialog {
	protected DialogBox alertBox;
	protected ScrollPanel display;
	
	public PanelDialog(String title)
	{
		alertBox = new DialogBox();
		display = new ScrollPanel();
		
		//setup alert box
		alertBox = new DialogBox(true);
		alertBox.setHTML("<b>" + title + "</b>");
        alertBox.setAnimationEnabled(true);
        alertBox.setGlassEnabled(true);
        setupContentArea();
        alertBox.hide();
	}
	
	protected void setupContentArea()
	{
		VerticalPanel contentPanel = new VerticalPanel();
		Widget buttonPanel = getButtonPanel();
		contentPanel.add(display);
		contentPanel.add(buttonPanel);
		contentPanel.setCellHorizontalAlignment(buttonPanel, HorizontalPanel.ALIGN_CENTER);
		alertBox.setWidget(contentPanel);
	}
	
	protected Widget getButtonPanel()
	{
		Button ok = new Button("OK");
		ok.addClickHandler( new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				alertBox.hide();
			}
		});
		return ok;
	}
	
	public void show(Widget w)
	{
		display.clear();
		display.add(w);
		alertBox.show();
		alertBox.center();
	}
}
