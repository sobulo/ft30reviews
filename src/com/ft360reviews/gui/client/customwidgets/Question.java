/**
 * 
 */
package com.ft360reviews.gui.client.customwidgets;

/**
 * @author Segun Razaq Sobulo
 *
 */
public interface Question {
	public int getQuestionNumber();
	public String getAnswer();
	public void setAnswer(String answer);
}
