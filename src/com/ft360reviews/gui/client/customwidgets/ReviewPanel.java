/**
 * 
 */
package com.ft360reviews.gui.client.customwidgets;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

import com.ft360reviews.gui.client.GUIConstants;
import com.ft360reviews.gui.client.ReadServiceAsync;
import com.ft360reviews.gui.client.WebAppHelper;
import com.ft360reviews.gui.client.WriteServiceAsync;
import com.ft360reviews.gui.shared.PanelServiceConstants;
import com.ft360reviews.gui.shared.ReviewDTO;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.CloseEvent;
import com.google.gwt.event.logical.shared.CloseHandler;
import com.google.gwt.event.logical.shared.OpenEvent;
import com.google.gwt.event.logical.shared.OpenHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Anchor;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.DisclosurePanel;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;

/**
 * @author Segun Razaq Sobulo
 *
 */
public class ReviewPanel extends Composite implements ClickHandler{
	
	@UiField VerticalPanel disclosureSlot;
	
	private ArrayList<FreeFormPanelGenerator> reviewTextQuestions;
	private ArrayList<MultiChoicePanelGenerator> reviewChoiceQuestions;
	private Button save, lock;
	protected String reviewId;
	private MultipleMessageDialog errorBox;
	private YesNoDialog confirmSubmit;
	private SimpleDialog infoBox;
	private ArrayList<String> errorStrs;
	private WriteServiceAsync writeService = WebAppHelper.getWriteService();
	private ReadServiceAsync readService = WebAppHelper.getReadService();
	private static ReviewPanelUiBinder uiBinder = GWT
			.create(ReviewPanelUiBinder.class);
	private boolean requiresLoad;
	private EmployeeSuggestAllBox revieweeBox;
	protected Label initLabel;
	protected boolean ignoreLock;

	interface ReviewPanelUiBinder extends UiBinder<Widget, ReviewPanel> {
	}
	
	final AsyncCallback<String> saveCallback = new AsyncCallback<String>() {
		@Override
		public void onFailure(Throwable caught) {
			String[] errMsg = {"Request failed: " + caught.getMessage()};
			errorBox.show("Unable to save due to: ", errMsg);
			initLabel.setText("Panel load failed!");
		}

		@Override
		public void onSuccess(String result) {
			if(requiresLoad)
				loadData();
			String message = "Succesfully saved: " + result;
			infoBox.show(message);
		}
	}; 	
	
	final AsyncCallback<ReviewDTO> setupReviewCallback = new AsyncCallback<ReviewDTO>() {
		@Override
		public void onFailure(Throwable caught) {
			String[] errMsg = {"Request failed: " + caught.getMessage()};
			errorBox.show("Unable to load review panel due to: ", errMsg);
		}

		@Override
		public void onSuccess(ReviewDTO result) {
			if(requiresLoad)
			{
				disclosureSlot.clear();
				if(result.locked && !ignoreLock)
					buildDisplayPanel(result);
				else
					buildQuestionPanels(result);
				requiresLoad = false;
			}
		}
	};	

	public ReviewPanel(String reviewId) 
	{
		initWidget(uiBinder.createAndBindUi(this));
		reviewChoiceQuestions = new ArrayList<MultiChoicePanelGenerator>();
		reviewTextQuestions = new ArrayList<FreeFormPanelGenerator>();
		errorBox = new MultipleMessageDialog("ERRORS OCCURRED");
		confirmSubmit = new YesNoDialog("Confirm Submission", "Submit/Lock", "Cancel", "        ");
		infoBox = new SimpleDialog("Information");
		errorStrs = new ArrayList<String>();
		initLabel = new Label("Loading review data ...");
		disclosureSlot.setWidth("100%");
		disclosureSlot.add(initLabel);
		requiresLoad = true;
		this.reviewId = reviewId;
	}
	
	public AsyncCallback<ReviewDTO> getSetupCallback()
	{
		return setupReviewCallback;
	}
	
	public void loadData()
	{
		readService.getReview(reviewId, setupReviewCallback);
	}
	
	public void buildQuestionPanels(ReviewDTO dto)
	{
		ArrayList<Integer> sortedKeySet = new ArrayList<Integer>();
		sortedKeySet.addAll(dto.questionSet.keySet());
		Collections.sort(sortedKeySet);
		HashMap<Integer, String> questionList = dto.questionSet;
		HashMap<Integer, String> options = dto.answerMapping;
		
		VerticalPanel choiceContent = new VerticalPanel();
		VerticalPanel textContent = new VerticalPanel();
		choiceContent.setSpacing(5);
		textContent.setSpacing(10);
		choiceContent.setWidth("100%");
		textContent.setWidth("100%");
		boolean multiColor = true;
		for(int i : sortedKeySet)
		{
			if( i <= PanelServiceConstants.FREE_FORM_MARKER)
			{
				MultiChoicePanelGenerator m = new MultiChoicePanelGenerator(i);
				Widget w = m.getMultiChoicePanel(dto.reviewId, questionList.get(i), options);
				if(dto.multichoiceAnswers != null && dto.multichoiceAnswers.get(i) != null)
					m.setAnswer(String.valueOf(dto.multichoiceAnswers.get(i)));				
				if(multiColor)
					w.addStyleName("questPrimary");
				else
					w.addStyleName("questSecondary");
				multiColor = !multiColor;
				choiceContent.add(w);
				w.setWidth("100%");
				reviewChoiceQuestions.add(m);
			}
			else
			{
				int questNum = i - PanelServiceConstants.FREE_FORM_MARKER;
				FreeFormPanelGenerator g = new FreeFormPanelGenerator(questNum);
				textContent.add(g.getFreeFormPanel(questionList.get(i)));
				if(dto.textAnswers != null && dto.textAnswers.get(i) != null)
					g.setAnswer(dto.textAnswers.get(i));				
				reviewTextQuestions.add(g);
			}
		}
		
		if(reviewChoiceQuestions.size() > 0 || reviewTextQuestions.size() > 0)
			disclosureSlot.add(getSavePanel(dto));
		else
			disclosureSlot.add(new Label("This review doesn't appear to be setup properly as it contains no questions"));
		
		GWT.log("Number of text questions is: " + reviewTextQuestions.size());
		GWT.log("Number of multichoice questions is: " + reviewChoiceQuestions.size());
		
		DisclosurePanel[] additionalData = getSupplementalDisclosurePanels(dto);
		if( additionalData != null)
			for(DisclosurePanel dp : additionalData)
				disclosureSlot.add(dp);
		
		if(reviewChoiceQuestions.size() > 0)
		{
			String openMultiHeader = "Please choose an option for each of the questions below:";
			disclosureSlot.add(setupDisclosurePanel(openMultiHeader, "Click for multichoice survey", choiceContent));
		}
		
		if(reviewTextQuestions.size() > 0)
		{
			String openTextHeader = "Please answer the questions below and give <font color='red'>" +
											"specific examples</font> to support your statements:";
			disclosureSlot.add(setupDisclosurePanel(openTextHeader, "Click for text input survey", textContent));		
		}
	}
	
	public DisclosurePanel[] getSupplementalDisclosurePanels(ReviewDTO dto)
	{
		return null;
	}
	
	protected String getServiceType()
	{
		return PanelServiceConstants.PARAM_FETCH_REVIEW;
	}
	
	private Anchor getViewLink(String serviceType, String id)
	{
		StringBuilder url = new StringBuilder();
		url.append(GWT.getHostPageBaseURL()).append("reports?");
		url.append(PanelServiceConstants.PARAM_SERVICE_TYPE).append("=").append(serviceType);
        url.append("&").append(PanelServiceConstants.PARAM_REVIEW_ID).append("=").append(id);
        Anchor viewLink = new Anchor("Open in new window", url.toString());
        viewLink.setTarget("_blank");
        GWT.log("View Link is: " + url.toString());
        return viewLink;
	}
	
	public void buildDisplayPanel(ReviewDTO dto)
	{ 		
		HTML content = new HTML(dto.getHTML());
		content.addStyleName("themePaddedBorder");
		Anchor viewLink = getViewLink(getServiceType(), dto.reviewId);
		disclosureSlot.add(viewLink);
		disclosureSlot.setCellHorizontalAlignment(viewLink, HasHorizontalAlignment.ALIGN_RIGHT);
		disclosureSlot.add(content);
	}	
	
	public DisclosurePanel getSavePanel(ReviewDTO dto)
	{
		VerticalPanel content = new VerticalPanel();
		HTML header;
		
		StringBuilder headerStr = new StringBuilder();
		headerStr.append("<div style='border: 1px solid black'>");
		dto.storeHeaderStr(headerStr);
	    headerStr.append("</div>");
		header = new HTML(headerStr.toString());
		header.setWidth("70%");

		if(dto.reviewee == null && dto.alloweRevieweeEdits)
			revieweeBox = new EmployeeSuggestAllBox(dto.name);
		
		
		HorizontalPanel buttonPanel = new HorizontalPanel();
		save = new Button("Save Progress");
		lock = new Button("Submit Review");
		save.addClickHandler(this);
		lock.addClickHandler(this);
		buttonPanel.setSpacing(10);
		buttonPanel.add(save);
		buttonPanel.add(lock);
		content.add(header);
		content.setWidth("100%");
		
		content.setCellHorizontalAlignment(header, HorizontalPanel.ALIGN_CENTER);
		
		if(revieweeBox != null)
			content.add(revieweeBox);
		content.add(buttonPanel);
		content.setCellHorizontalAlignment(buttonPanel, HorizontalPanel.ALIGN_CENTER);
		
		//setup disclosure panel
		String openHeader = "Use save button below to save your progress so you can continue editing review later." +
				"Use submit button once you're done with the review. Submitted reviews cannot be edited later";
		String closeHeader = "Click to save " + dto.description;
		return setupDisclosurePanel(openHeader, closeHeader, content);
	}
	
	public DisclosurePanel setupDisclosurePanel(String openTitle, String closeTitle, Widget content)
	{
		final DisclosurePanel dp = new DisclosurePanel();
		dp.setOpen(false);
		final HTML Close = new HTML("<table border='0'><tr><td valign='middle'><img src='" + 
				GUIConstants.DP_CLOSED_IMG + "'/></td><td valign='middle'><b>" + closeTitle + "</b></td></tr></table>");
		final HTML Open = new HTML("<table border='0'><tr><td valign='middle'><img src='" + 
				GUIConstants.DP_OPENED_IMG + "'/></td><td valign='middle'><b>" + openTitle + "</b></td></tr></table>");
		GWT.log(Close.getHTML());
		dp.setHeader(Close);
		dp.add(content);
		
		dp.addCloseHandler(new CloseHandler<DisclosurePanel>() {
			
			@Override
			public void onClose(CloseEvent<DisclosurePanel> event) {
				dp.setHeader(Close);
			}
		});
		
		dp.addOpenHandler(new OpenHandler<DisclosurePanel>() {
			
			@Override
			public void onOpen(OpenEvent<DisclosurePanel> event) {
				dp.setHeader(Open);
			}
		});
		dp.addStyleName("themePaddedBorder");
		dp.setWidth("100%");
		return dp;		
	}
	
	public HashMap<Integer, Integer> getMultiChoiceAnswers()
	{
		HashMap<Integer, Integer> result = new HashMap<Integer, Integer>();
		for(Question quest : reviewChoiceQuestions)
		{
			if(quest.getAnswer() == null)
				errorStrs.add("Multi-choice question number: " + quest.getQuestionNumber() + " has no value selected");
			else
				result.put(quest.getQuestionNumber(), Integer.valueOf(quest.getAnswer()));
		}
		return result;
	}
	
	public HashMap<Integer, String> getTextAnswers()
	{
		HashMap<Integer, String> result = new HashMap<Integer, String>(reviewTextQuestions.size());
		for(Question quest : reviewTextQuestions)
		{
			String ans = quest.getAnswer();
			if(ans == null || ans.length() < 10)
				errorStrs.add("Text question number: " + quest.getQuestionNumber() + " appears to be empty");
			else
				result.put(quest.getQuestionNumber() + PanelServiceConstants.FREE_FORM_MARKER, ans);
		}
		return result;
	}
	
	/* (non-Javadoc)
	 * @see com.google.gwt.event.dom.client.ClickHandler#onClick(com.google.gwt.event.dom.client.ClickEvent)
	 */
	@Override
	public void onClick(ClickEvent event) {
		final HashMap<Integer, Integer> choiceAnswers = getMultiChoiceAnswers();
		final HashMap<Integer, String> textAnswers = getTextAnswers();
		String revieweeId = null;
		String additionalComments = "";
		
		if(revieweeBox != null)
		{
			revieweeId = revieweeBox.getSelectedUser();
			additionalComments= ". Reviewee chosen by reviewer";
			if(revieweeId == null)
				errorStrs.add(revieweeBox.getSuggestLabel() + " field is blank");
		}
		
		if(event.getSource() == save)
			writeService.updateReview(reviewId, revieweeId, choiceAnswers, textAnswers, false, null, saveCallback);
		else
		{
			if(errorStrs.size() > 0)
				errorBox.show("You must fix issues below prior to submitting, though " +
						"you can still save the review now", errorStrs);
			else
			{
				confirmSubmit.show("Are you sure you're ready to submit the review. Once you submit, you will no longer be able to edit the review");
				final String tempId = revieweeId;
				final String tempComments = additionalComments;
				confirmSubmit.setClickHandler(new ClickHandler() {					
					@Override
					public void onClick(ClickEvent event) {
						confirmSubmit.hide();
						writeService.updateReview(reviewId, tempId, choiceAnswers, textAnswers, 
								true, "Locked via reviewer submit" + tempComments, saveCallback);						
					}
				});
				requiresLoad = true;
			}
		}
		errorStrs.clear();
	}	
}
