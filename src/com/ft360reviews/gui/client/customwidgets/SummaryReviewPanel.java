/**
 * 
 */
package com.ft360reviews.gui.client.customwidgets;

import java.util.HashMap;

import com.ft360reviews.gui.client.GUIConstants;
import com.ft360reviews.gui.client.WebAppHelper;
import com.ft360reviews.gui.shared.PanelServiceConstants;
import com.ft360reviews.gui.shared.ReviewDTO;
import com.ft360reviews.gui.shared.ReviewSummaryDTO;
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Anchor;
import com.google.gwt.user.client.ui.DisclosurePanel;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Widget;

/**
 * @author Segun Razaq Sobulo
 *
 */
public class SummaryReviewPanel extends ReviewPanel{
	private static HashMap<String, ReviewPanel> cachedPanels = new HashMap<String, ReviewPanel>();
	private static final Label NOT_LOCKED_DISPLAY = new Label("Reviewer must submit review before it can be viewed as part of summary display");
	static
	{
		NOT_LOCKED_DISPLAY.setWidth("200px");
		NOT_LOCKED_DISPLAY.setWordWrap(true);
	}
	private PanelDialog reviewDisplay;
	
	final AsyncCallback<ReviewSummaryDTO> setupReviewCallback = new AsyncCallback<ReviewSummaryDTO>() {
		@Override
		public void onFailure(Throwable caught) {
			getSetupCallback().onFailure(caught);
		}

		@Override
		public void onSuccess(ReviewSummaryDTO result) {
			getSetupCallback().onSuccess(result);
		}
	};
	
	/**
	 * @param reviewId
	 */
	public SummaryReviewPanel(String reviewId) {
		super(reviewId);
		reviewDisplay = new PanelDialog("Review Details");
	}
	
	@Override
	public DisclosurePanel[] getSupplementalDisclosurePanels(ReviewDTO dto)
	{
		ReviewSummaryDTO dtoSum = (ReviewSummaryDTO) dto;
		DisclosurePanel[] result = new DisclosurePanel[2];
		result[0] = getSummaryDetails(dtoSum);
		result[1] = getReviewers(dtoSum);
		return result;
	}
	
	private DisclosurePanel getSummaryDetails(ReviewSummaryDTO dtoSum)
	{
		String openTextHeader = "Below is a summary of reviews submitted for " + (dtoSum.reviewee == null? dtoSum.name : dtoSum.reviewee);
		HTML content = new HTML(dtoSum.getCalculatedAvgsHTML());
		DisclosurePanel result = setupDisclosurePanel(openTextHeader, "Click for summary/average of multichoice reviews", content);
		return result;		
	}
	
	private Anchor getReviewLink(String reviewId)
	{
		StringBuilder url = new StringBuilder();
		url.append(GWT.getHostPageBaseURL()).append("reports?");
		url.append(PanelServiceConstants.PARAM_SERVICE_TYPE).append("=").append(PanelServiceConstants.PARAM_FETCH_REVIEW_4_SUMMARIZER);
        url.append("&").append(PanelServiceConstants.PARAM_REVIEW_ID).append("=").append(reviewId);
        Anchor viewLink = new Anchor("Open in new window", url.toString());
        viewLink.setTarget("_blank");
        GWT.log("View Link is: " + url.toString());
        return viewLink;		
	}
	
	private DisclosurePanel getReviewers(ReviewSummaryDTO dtoSum)
	{
		String openTextHeader = "Below is a list of people that reviewed " + (dtoSum.reviewee == null? dtoSum.name : dtoSum.reviewee);
        FlexTable content = new FlexTable();
        content.setWidth("80%");
        content.setBorderWidth(1);
        content.addStyleName(GUIConstants.STYLE_STND_TABLE);
        String[] colHeaders = {"Reviewer ID", "Status", "View Review"};

        for(int i = 0; i < colHeaders.length; i++)
        {
            content.setHTML(0, i, "<i>" + colHeaders[i] + "</i>");
            content.getFlexCellFormatter().addStyleName(0, i, GUIConstants.STYLE_STND_TABLE_HEADER);
        }
        
        int rowCount = 1;
		for(String reviewId : dtoSum.childReviewIDs.keySet())
		{
			content.setHTML(rowCount, 0, dtoSum.childReviewIDs.get(reviewId)[0]);
			final boolean locked = dtoSum.childReviewIDs.get(reviewId)[1].equals(PanelServiceConstants.LOCK_INDICATOR);
			String status = null;
			Widget openReview = null;
			if(locked)
			{
				status = "Submitted";
				openReview = getReviewLink(reviewId);
			}
			else
			{
				status = "Pending";
				openReview = new Label("Not yet available");
			}
			
			//setup review display
			content.setHTML(rowCount, 1, status);
			content.getFlexCellFormatter().addStyleName(rowCount, 2, GUIConstants.STYLE_CENTER);
			content.setWidget(rowCount, 2, openReview);
			rowCount++;
		};
		DisclosurePanel result = setupDisclosurePanel(openTextHeader, "Click for reviewer list", content);
		return result;
	}
	
	@Override
	public void loadData()
	{
		WebAppHelper.getReadService().getReviewSummary(reviewId, setupReviewCallback);
	}
	
	public void loadData(boolean setIgnoreLock)
	{
		this.ignoreLock = setIgnoreLock;
		loadData();
	}
	
	protected String getServiceType()
	{
		return PanelServiceConstants.PARAM_FETCH_SUMMARY;
	}
}
