/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ft360reviews.gui.client.customwidgets;

import com.google.gwt.user.client.ui.Widget;

/**
 *
 * @author Administrator
 */
public interface Text2ListBoxInput {
    public String getCustomText();
    public Widget getInputWidget();
    public void disableInput();
    public void enableInput();
    public void reset();
}
