/**
 * 
 */
package com.ft360reviews.gui.client.customwidgets.scrolledtab;

import com.google.gwt.resources.client.ClientBundle;
import com.google.gwt.resources.client.ImageResource;

/**
 * @author Segun Razaq Sobulo
 *
 */
public interface Resources extends ClientBundle
{
	@Source("left_arrow.jpg")
	ImageResource leftArrow();
	
	@Source("right_arrow.jpg")
	ImageResource rightArrow();		
}
