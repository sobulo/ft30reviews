/**
 * 
 */
package com.ft360reviews.gui.client.reviews;

import java.util.HashMap;

import com.ft360reviews.gui.client.WebAppHelper;
import com.ft360reviews.gui.client.customwidgets.SummaryReviewPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.VerticalPanel;

/**
 * @author Segun Razaq Sobulo
 *
 */
public class AdminEditPanel extends SearchSummaryPanel {
	@Override
	protected void populateContent(HashMap<String, String> result)
	{
		VerticalPanel contentArea = getContentArea();
		if(WebAppHelper.isSuperUserLogin())
		{
			SummaryReviewPanel rp = new SummaryReviewPanel(result.keySet().iterator().next());
			contentArea.add(rp);
			rp.loadData(true);			
		}
		else
		{
			contentArea.add(new Label("You do not have sufficient privileges to access this page"));
		}
	}

}
