/**
 * 
 */
package com.ft360reviews.gui.client.reviews;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

import com.ft360reviews.gui.client.GUIConstants;
import com.ft360reviews.gui.client.ReadServiceAsync;
import com.ft360reviews.gui.client.WebAppHelper;
import com.ft360reviews.gui.client.customwidgets.ReviewPanel;
import com.ft360reviews.gui.client.customwidgets.SimpleDialog;
import com.ft360reviews.gui.client.customwidgets.scrolledtab.ScrolledTabLayoutPanel2;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.logical.shared.SelectionEvent;
import com.google.gwt.event.logical.shared.SelectionHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ResizeComposite;
import com.google.gwt.user.client.ui.ScrollPanel;
import com.google.gwt.user.client.ui.TabLayoutPanel;

/**
 * @author Segun Razaq Sobulo
 *
 */
public class GroupedReviewsPanel extends ResizeComposite implements SelectionHandler<Integer>{
	private TabLayoutPanel reviewPanels;
	private ReadServiceAsync readService = WebAppHelper.getReadService();
	private SimpleDialog errorBox;
	private Label messageLabel;

	private final AsyncCallback<HashMap<String, String>> setupReviewCallback = new AsyncCallback<HashMap<String, String>>() {
		@Override
		public void onFailure(Throwable caught) {
			reviewPanels.clear();
			String errMsg = "Request failed: " + caught.getMessage();
			errorBox.show(errMsg);
			messageLabel.setText("Unable to load review data, try refreshing browser. Please contact IT if problem persists");
			reviewPanels.add(messageLabel, "Error");
		}
		
		final class ReviewNameSorter implements Comparable<ReviewNameSorter>
		{
			String name, id;
			ReviewNameSorter(String name, String id)
			{
				this.name = name;
				this.id = id;
			}
			/* (non-Javadoc)
			 * @see java.lang.Comparable#compareTo(java.lang.Object)
			 */
			@Override
			public int compareTo(ReviewNameSorter o) {
				return this.name.compareTo(o.name);
			}
		}

		@Override
		public void onSuccess(HashMap<String, String> result) {
			reviewPanels.clear();
			if(result == null)
			{
				errorBox.show(getEmptyMessage());
				messageLabel.setText("You've not been setup to use this panel");
				reviewPanels.add(messageLabel, "Message");
				return;
			}
			
			
			boolean first = true;
			ArrayList<ReviewNameSorter> sortedReviews= new ArrayList<ReviewNameSorter>(result.size());
			for(String reviewId : result.keySet())
				sortedReviews.add(new ReviewNameSorter(result.get(reviewId), reviewId));
			Collections.sort(sortedReviews);
			for(ReviewNameSorter rev : sortedReviews)
			{
				String[] nameParts = rev.name.split("\\s");
				String htmlParts = nameParts[0];
				if(nameParts.length > 1)
					htmlParts += "<br/>" + nameParts[nameParts.length-1];
				ReviewPanel rp = getReviewPanel(rev.id);
				HTML header = new HTML(htmlParts);
				header.addStyleName("smallFont");
				//header.setWidth("60px");
				//header.setHeight("40px");
				//header.setWordWrap(true);
				reviewPanels.add(new ScrollPanel(rp), header);
				if(first)
				{
					rp.loadData();
					first = false;
				}
			}
			addReviewLoadHandler();
		}
	};		
	
	public String getEmptyMessage()
	{
		return "You have not been selected to review any other employee. " +
		"Please contact HR if you feel this should not be the case";		
	}
	
	public ReviewPanel getReviewPanel(String reviewId)
	{
		return new ReviewPanel(reviewId);
	}
	
	private void addReviewLoadHandler()
	{
		reviewPanels.addSelectionHandler(this);
	}
	
	public GroupedReviewsPanel()
	{
		//reviewPanels = new TabLayoutPanel(GUIConstants.DEFAULT_TAB_HEADER, Unit.PX);
		reviewPanels = new ScrolledTabLayoutPanel2(GUIConstants.DEFAULT_TAB_HEADER, Unit.PX);
		reviewPanels.setHeight(GUIConstants.DEFAULT_TAB_HEIGHT);
		reviewPanels.setWidth(GUIConstants.DEFAULT_TAB_WIDTH);
		initWidget(new ScrollPanel(reviewPanels));
		errorBox = new SimpleDialog("ERROR");
		messageLabel = new Label("Requesting list of reviewees");
		reviewPanels.add(messageLabel, "Message");
		fetchEmployeeNamesToBeReviewed();
	}
	
	public AsyncCallback<HashMap<String, String>> getEmployeeNamesCallBack()
	{
		return setupReviewCallback;
	}
	
	public void fetchEmployeeNamesToBeReviewed()
	{
		readService.getEmployeeNamesToBeReviewed(false, setupReviewCallback);
	}
	
	public void clearSelectedColor()
	{
		for(int i = 0; i < reviewPanels.getWidgetCount(); i++)
			((HTML) reviewPanels.getTabWidget(i)).removeStyleName("selectedTabColor");
	}
	
	public void setSelectedColor(int i)
	{
		((HTML) reviewPanels.getTabWidget(i)).addStyleName("selectedTabColor");
	}

	/* (non-Javadoc)
	 * @see com.google.gwt.event.logical.shared.SelectionHandler#onSelection(com.google.gwt.event.logical.shared.SelectionEvent)
	 */
	@Override
	public void onSelection(SelectionEvent<Integer> event) 
	{	
		clearSelectedColor();
		setSelectedColor(event.getSelectedItem());
		ReviewPanel rp = (ReviewPanel) ((ScrollPanel)reviewPanels.getWidget(event.getSelectedItem())).getWidget();
		rp.loadData();
	}
}
