/**
 * 
 */
package com.ft360reviews.gui.client.reviews;

import com.ft360reviews.gui.client.WebAppHelper;
import com.ft360reviews.gui.client.customwidgets.ReviewPanel;
import com.ft360reviews.gui.client.customwidgets.SummaryReviewPanel;

/**
 * @author Segun Razaq Sobulo
 *
 */
public class GroupedSummaryReviewsPanel extends GroupedReviewsPanel{
	
	@Override
	public void fetchEmployeeNamesToBeReviewed()
	{
		WebAppHelper.getReadService().getEmployeeNamesToBeReviewed(true, getEmployeeNamesCallBack());
	}
	
	@Override
	public ReviewPanel getReviewPanel(String reviewId)
	{
		return new SummaryReviewPanel(reviewId);
	}
	
	@Override
	public String getEmptyMessage()
	{
		return "You have not been selected to write summaries for any other employees. " +
		"Please contact HR if you feel this should not be the case";		
	}	
}
