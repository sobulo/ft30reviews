/**
 * 
 */
package com.ft360reviews.gui.client.reviews;

import java.util.HashMap;

import com.ft360reviews.gui.client.ReadServiceAsync;
import com.ft360reviews.gui.client.WebAppHelper;
import com.ft360reviews.gui.client.customwidgets.EmployeeSuggestAllBox;
import com.ft360reviews.gui.client.customwidgets.SimpleDialog;
import com.ft360reviews.gui.shared.PanelServiceConstants;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Anchor;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;

/**
 * @author Segun Razaq Sobulo
 *
 */
public class SearchSummaryPanel extends Composite implements ClickHandler{
	
	@UiField Button fetchSummary;
	@UiField EmployeeSuggestAllBox employeeBox;
	@UiField VerticalPanel contentArea;
	@UiField Label status;
	
	private SimpleDialog errorBox;
	private ReadServiceAsync readService = WebAppHelper.getReadService();
	
	private final AsyncCallback<HashMap<String, String>> setupSummaryCallback = new AsyncCallback<HashMap<String, String>>() {
		@Override
		public void onFailure(Throwable caught) {
			status.setText(caught.getMessage());
		}
		
		@Override
		public void onSuccess(HashMap<String, String> result) 
		{
			contentArea.setVisible(true);
			populateContent(result);
		}
	};
	
	protected VerticalPanel getContentArea()
	{
		return contentArea;
	}
	
	protected void populateContent(HashMap<String, String> result)
	{
		contentArea.add(new HTML("<B>Review Summary</B>"));
		contentArea.add(new Label("Click on link(s) below to view review summary"));
		for(String key : result.keySet())
			contentArea.add(getViewLink(key, result.get(key)));		
	}
	
	private static SearchSummaryPanelUiBinder uiBinder = GWT
			.create(SearchSummaryPanelUiBinder.class);

	interface SearchSummaryPanelUiBinder extends
			UiBinder<Widget, SearchSummaryPanel> {
	}

	public SearchSummaryPanel() 
	{
		initWidget(uiBinder.createAndBindUi(this));
		errorBox = new SimpleDialog("An error occured");
		contentArea.setWidth("90%");
		contentArea.setSpacing(20);	
		contentArea.setVisible(false);
		fetchSummary.addClickHandler(this);
	}
	

	private Anchor getViewLink(String id, String display)
	{
		StringBuilder url = new StringBuilder();
		url.append(GWT.getHostPageBaseURL()).append("reports?");
		url.append(PanelServiceConstants.PARAM_SERVICE_TYPE).append("=").append(PanelServiceConstants.PARAM_FETCH_SUMMARY);
        url.append("&").append(PanelServiceConstants.PARAM_REVIEW_ID).append("=").append(id);
        Anchor viewLink = new Anchor(display, url.toString());
        viewLink.setTarget("_blank");
        return viewLink;
	}
	
	/* (non-Javadoc)
	 * @see com.google.gwt.event.dom.client.ClickHandler#onClick(com.google.gwt.event.dom.client.ClickEvent)
	 */
	@Override
	public void onClick(ClickEvent event) {
		String loginId = employeeBox.getSelectedUser();
		if(loginId == null)
		{
			errorBox.show("Please enter a valid employee login name");
			return;
		}
		contentArea.clear();
		contentArea.setVisible(false);
		readService.getSummaryIDs(loginId, setupSummaryCallback);
	}	
}
