/**
 * 
 */
package com.ft360reviews.gui.client.reviews;

import java.util.HashMap;

import com.ft360reviews.gui.client.ReadServiceAsync;
import com.ft360reviews.gui.client.WebAppHelper;
import com.ft360reviews.gui.shared.PanelServiceConstants;
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Anchor;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.VerticalPanel;

/**
 * @author Segun Razaq Sobulo
 *
 */
public class ViewQuestionsPanel extends Composite
{
	VerticalPanel content;
	private ReadServiceAsync readService = WebAppHelper.getReadService();
	private final AsyncCallback<HashMap<String, String>> setupQuestionsCallback = new AsyncCallback<HashMap<String, String>>() {
		@Override
		public void onFailure(Throwable caught) {
			content.clear();
			content.add(new Label("Unable to load question sets!!" + caught.getMessage()));
		}
		
		@Override
		public void onSuccess(HashMap<String, String> result) 
		{
			content.clear();
			content.add(new HTML("<B>Review Question Sets</B>"));
			content.add(new Label("Click on link below to view review questions. " +
					"You can use this page for printing paper copies of review questions"));
			for(String key : result.keySet())
				content.add(getViewLink(key, result.get(key)));
		}
	};
	
	public ViewQuestionsPanel()
	{
		content = new VerticalPanel();
		initWidget(content);
		content.add(new Label("Loading question sets ..."));
		content.setSpacing(20);
		readService.getQuestionIDs(setupQuestionsCallback);
	}
	
	private Anchor getViewLink(String id, String display)
	{
		StringBuilder url = new StringBuilder();
		url.append(GWT.getHostPageBaseURL()).append("reports?");
		url.append(PanelServiceConstants.PARAM_SERVICE_TYPE).append("=").append(PanelServiceConstants.PARAM_FETCH_QUESTION);
        url.append("&").append(PanelServiceConstants.PARAM_QUESTION_ID).append("=").append(id);
        Anchor viewLink = new Anchor(display, url.toString());
        viewLink.setTarget("_blank");
        return viewLink;
	}	
}
