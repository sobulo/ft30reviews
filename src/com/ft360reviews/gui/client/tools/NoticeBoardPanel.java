/**
 * 
 */
package com.ft360reviews.gui.client.tools;

import java.util.HashMap;

import com.ft360reviews.gui.client.ReadServiceAsync;
import com.ft360reviews.gui.client.WebAppHelper;
import com.ft360reviews.gui.client.WriteServiceAsync;
import com.ft360reviews.gui.client.customwidgets.SimpleDialog;
import com.ft360reviews.gui.client.customwidgets.richtext.RichTextToolbar;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.RichTextArea;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;

/**
 * @author Segun Razaq Sobulo
 *
 */
public class NoticeBoardPanel extends Composite implements ClickHandler, ChangeHandler{
	
	@UiField ListBox noticeId;
	@UiField SimplePanel contentSlot;
	@UiField TextBox title;
	@UiField Label status;
	@UiField Button saveNotice;
	

	private RichTextArea area;
	private static NoticeBoardPanelUiBinder uiBinder = GWT
			.create(NoticeBoardPanelUiBinder.class);
	
	private ReadServiceAsync readService = WebAppHelper.getReadService();
	private WriteServiceAsync writeService = WebAppHelper.getWriteService();
	private SimpleDialog errorBox, infoBox;

	interface NoticeBoardPanelUiBinder extends
			UiBinder<Widget, NoticeBoardPanel> {
	}

	final AsyncCallback<Void> saveCallback = new AsyncCallback<Void>() {
		@Override
		public void onFailure(Throwable caught) {
			status.setText("Unable to load notice info: " + caught.getMessage());
			errorBox.show("Unable to load notice info: " + caught.getMessage());
			enableWidgets();
		}

		@Override
		public void onSuccess(Void result) {
			enableWidgets();
			status.setText("Succesfully saved notice");
			infoBox.show("Successfully saved notice");
		}
	};	
	
	final AsyncCallback<String[]> noticeCallback = new AsyncCallback<String[]>() {
		@Override
		public void onFailure(Throwable caught) {
			status.setText("Unable to load notice info: " + caught.getMessage());
			errorBox.show("Unable to load notice info: " + caught.getMessage());
			enableWidgets();
		}

		@Override
		public void onSuccess(String[] result) {
			enableWidgets();
			if(result.length != 2)
				return;
			
			title.setText(result[0]);
			area.setHTML(result[1]);
		}
	};	
	
	final AsyncCallback<HashMap<String, String>> idCallback = new AsyncCallback<HashMap<String, String>>() {
		@Override
		public void onFailure(Throwable caught) {
			status.setText("Unable to load notice list: " + caught.getMessage());
			errorBox.show("Unable to load notice list: " + caught.getMessage());
		}

		@Override
		public void onSuccess(HashMap<String, String> result) {
			if(result.size() == 0)
				return;
			
			for(String id : result.keySet())
				noticeId.addItem(result.get(id), id);
			
			noticeId.setSelectedIndex(0);
			readService.getNotice(noticeId.getValue(0), noticeCallback);
		}
	}; 	
	

	public NoticeBoardPanel() {
		initWidget(uiBinder.createAndBindUi(this));
		contentSlot.add(getRichTextArea());
		noticeId.addChangeHandler(this);
		saveNotice.addClickHandler(this);
		infoBox = new SimpleDialog("Info");
		errorBox = new SimpleDialog("Error");
		readService.getNoticeIDs(idCallback);
		disableWidgets();
	}
	
	public Widget getRichTextArea()
	{
		area = new RichTextArea();
	    RichTextToolbar tb = new RichTextToolbar(area, "Enter notice details below");
	    VerticalPanel p = new VerticalPanel();
	    p.add(tb);
	    p.add(area);
	    
	    area.setHeight("14em");
	    area.setWidth("99%");
	    tb.setWidth("100%");
	    p.setWidth("100%");		
	    return p;	
	}

	/* (non-Javadoc)
	 * @see com.google.gwt.event.dom.client.ChangeHandler#onChange(com.google.gwt.event.dom.client.ChangeEvent)
	 */
	@Override
	public void onChange(ChangeEvent event) {
		title.setValue(null);
		area.setHTML("");
		disableWidgets();
		readService.getNotice(noticeId.getValue(noticeId.getSelectedIndex()), noticeCallback);
	}

	/* (non-Javadoc)
	 * @see com.google.gwt.event.dom.client.ClickHandler#onClick(com.google.gwt.event.dom.client.ClickEvent)
	 */
	@Override
	public void onClick(ClickEvent event) {
		String selectedId = noticeId.getValue(noticeId.getSelectedIndex());
		String title = this.title.getValue();
		String content = area.getHTML();
		disableWidgets();
		writeService.updateNotice(selectedId, title, content, saveCallback);
	}
	
	private void disableWidgets()
	{
		saveNotice.setEnabled(false);
		area.setEnabled(false);
		title.setEnabled(false);
	}

	private void enableWidgets()
	{
		saveNotice.setEnabled(true);
		area.setEnabled(true);
		title.setEnabled(true);		
	}
}
