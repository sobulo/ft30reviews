/**
 * 
 */
package com.ft360reviews.gui.client.users;


import com.ft360reviews.gui.client.WebAppHelper;
import com.ft360reviews.gui.client.WriteServiceAsync;
import com.ft360reviews.gui.client.customwidgets.SimpleDialog;
import com.ft360reviews.gui.client.customwidgets.YesNoDialog;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.PasswordTextBox;
import com.google.gwt.user.client.ui.Widget;

/**
 * @author Segun Razaq Sobulo
 *
 */
public class PasswordChangePanel extends Composite implements ClickHandler
{	
	@UiField
	PasswordTextBox oldPassword;
	
	@UiField
	PasswordTextBox newPassword;
	
	@UiField
	PasswordTextBox confirmPassword;
	
	@UiField
	Button change;
	
	@UiField
	Label status;
	
	private YesNoDialog confirmBox;
	private SimpleDialog errorBox, infoBox;
    private WriteServiceAsync writeService = WebAppHelper.getWriteService();
    
	final AsyncCallback<String> changeCallback = new AsyncCallback<String>() {
		@Override
		public void onFailure(Throwable caught) {
			String errMsg = "Request failed: " + caught.getMessage();
			status.setText(errMsg);
			errorBox.show(errMsg);
		}

		@Override
		public void onSuccess(String result) {
			String message = "Password change completed for: " + result;
			infoBox.show(message);
			status.setText(message);
		}
	};    
    
	private static PasswordChangePanelUiBinder uiBinder = GWT
			.create(PasswordChangePanelUiBinder.class);

	interface PasswordChangePanelUiBinder extends
			UiBinder<Widget, PasswordChangePanel> {
	}

	public PasswordChangePanel() {
		initWidget(uiBinder.createAndBindUi(this));
		confirmBox = new YesNoDialog("Confirm password change");
		errorBox = new SimpleDialog("An error occurred");
		change.addClickHandler(this);
	}

	/* (non-Javadoc)
	 * @see com.google.gwt.event.dom.client.ClickHandler#onClick(com.google.gwt.event.dom.client.ClickEvent)
	 */
	@Override
	public void onClick(ClickEvent event) 
	{
		boolean success = validateInputLength(oldPassword.getValue(), "Old password") &&
						  validateInputLength(newPassword.getValue(), "New password") &&
						  validateInputLength(confirmPassword.getValue(), "Confirm password") &&
						  validateNewPassword();
		
		if(success)
		{
			confirmBox.setClickHandler(new ClickHandler() {
				
				@Override
				public void onClick(ClickEvent event) {
					writeService.changePassword(oldPassword.getValue(), 
							newPassword.getValue(), changeCallback);
					confirmBox.hide();
				}
			});		
			confirmBox.show("Are you sure you want to change your password?");
		}
	}
	
	public boolean validateInputLength(String input, String fieldName)
	{
		if(input.length() == 0)
		{
			errorBox.show("Please enter a value for: " + fieldName);
			return false;
		}
		return true;
	}
	
	public boolean validateNewPassword()
	{
		if(newPassword.getValue().equals(confirmPassword.getValue()))
			return true;
		else
		{
			errorBox.show("New/Confirm password fields do not match!");
			return false;
		}
	}

}
