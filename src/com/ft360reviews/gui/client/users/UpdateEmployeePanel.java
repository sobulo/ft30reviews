/**
 * 
 */
package com.ft360reviews.gui.client.users;


import com.ft360reviews.gui.client.ReadServiceAsync;
import com.ft360reviews.gui.client.WebAppHelper;
import com.ft360reviews.gui.client.WriteServiceAsync;
import com.ft360reviews.gui.client.customwidgets.EmployeePanel;
import com.ft360reviews.gui.client.customwidgets.MultipleMessageDialog;
import com.ft360reviews.gui.client.customwidgets.SimpleDialog;
import com.ft360reviews.gui.shared.TableMessage;
import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;

/**
 * @author Segun Razaq Sobulo
 *
 */
public class UpdateEmployeePanel extends Composite
{	
	@UiField
	EmployeePanel userInfo;
	
	private MultipleMessageDialog errorBox;
	private SimpleDialog messageBox;
	
	private static UpdateEmployeePanelUiBinder uiBinder = GWT
			.create(UpdateEmployeePanelUiBinder.class);

	interface UpdateEmployeePanelUiBinder extends
			UiBinder<Widget, UpdateEmployeePanel> {
	}

	private ReadServiceAsync readService = WebAppHelper.getReadService();
	private WriteServiceAsync writeService = WebAppHelper.getWriteService();

	final AsyncCallback<TableMessage> infoCallback = new AsyncCallback<TableMessage>() {
		@Override
		public void onFailure(Throwable caught) {
			String[] errorStr = {"Request failed: " + caught.getMessage()};
			errorBox.show("The following error occured: ", errorStr);
		}

		@Override
		public void onSuccess(TableMessage result) {
			userInfo.setFirstName(result.getText(1));
			userInfo.setLastName(result.getText(2));
		}
	};
	
	final AsyncCallback<Void> updateCallback = new AsyncCallback<Void>() {
		@Override
		public void onFailure(Throwable caught) {
			String[] errorStr = {"Request failed: " + caught.getMessage()};
			errorBox.show("The following error occured: ", errorStr);
		}

		@Override
		public void onSuccess(Void result) {
			messageBox.show("Successfully updated information");
		}
	};	
	
	public UpdateEmployeePanel() {
		initWidget(uiBinder.createAndBindUi(this));
		setupPanel();
		errorBox = new MultipleMessageDialog("ERROR");
		messageBox = new SimpleDialog("INFO");
	}

	private void setupPanel() {
		userInfo.disableNameEdits();
		readService.getEmployeeInfo(infoCallback);
	}
}
