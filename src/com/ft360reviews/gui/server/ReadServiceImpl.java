package com.ft360reviews.gui.server;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;

import com.ft360reviews.entities.GAEPrimaryEntity;
import com.ft360reviews.entities.InetConstants;
import com.ft360reviews.entities.InetDAO4CommonReads;
import com.ft360reviews.entities.InetEmployee;
import com.ft360reviews.entities.InetMultiAnswers;
import com.ft360reviews.entities.InetNotice;
import com.ft360reviews.entities.InetQuestions;
import com.ft360reviews.entities.InetReview;
import com.ft360reviews.entities.InetReviewSummary;
import com.ft360reviews.gui.client.ReadService;
import com.ft360reviews.gui.shared.LoginInfo;
import com.ft360reviews.gui.shared.PanelServiceConstants;
import com.ft360reviews.gui.shared.ReviewDTO;
import com.ft360reviews.gui.shared.ReviewSummaryDTO;
import com.ft360reviews.gui.shared.TableMessage;
import com.ft360reviews.gui.shared.exceptions.DuplicateEntitiesException;
import com.ft360reviews.gui.shared.exceptions.LoginValidationException;
import com.ft360reviews.gui.shared.exceptions.MissingEntitiesException;
import com.ft360reviews.login.LoginHelper;
import com.ft360reviews.login.LoginPortal;
import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.Objectify;
import com.googlecode.objectify.ObjectifyOpts;
import com.googlecode.objectify.ObjectifyService;

/**
 * The server side implementation of the RPC service.
 */
@SuppressWarnings("serial")
public class ReadServiceImpl extends RemoteServiceServlet implements
ReadService {
	private static final Logger log = Logger.getLogger(ReadServiceImpl.class.getName());

	@Override
	public LoginInfo login(String requestUri) throws MissingEntitiesException, LoginValidationException {
		HttpServletRequest req = this.getThreadLocalRequest();
		return LoginHelper.brokeredLogin(requestUri, req);
	}

	private static TableMessage getEmployeeInfo(Objectify ofy, Key<InetEmployee> empKey)
	throws MissingEntitiesException 
	{
		InetEmployee user = ofy.find(empKey);
		if (user == null) {
			String message = "Unable to generate employee info(%s) as non existent";
			log.severe(String.format(message, empKey.toString()));
			throw new MissingEntitiesException(String.format(message,
					empKey.getName()));
		}
		int numOfPhones = 0;
		if(user.getPhoneNumbers() != null)
			numOfPhones = user.getPhoneNumbers().size();
		TableMessage result = new TableMessage(4 + numOfPhones, 0, 1);
		result.setText(0, user.getLoginName());
		result.setText(1, user.getFname());
		result.setText(2, user.getLname());
		result.setText(3, user.getEmail());

		// append phonenumbers
		int i = 4;
		HashSet<String> phoneNums = user.getPhoneNumbers();

		if (phoneNums != null) {
			for (String num : phoneNums)
				result.setText(i++, num);
		}

		return result;
	}

	/* (non-Javadoc)
	 * @see com.ft360reviews.gui.client.ReadService#getEmployeeInfo()
	 */
	@Override
	public TableMessage getEmployeeInfo() throws MissingEntitiesException,
			LoginValidationException {
		Objectify ofy = ObjectifyService.begin();
		Key<InetEmployee> userKey= LoginPortal.getEmployeeKey(getThreadLocalRequest());
		return getEmployeeInfo(ofy, userKey);
	}

	/* (non-Javadoc)
	 * @see com.ft360reviews.gui.client.ReadService#getEmployeeNamesToBeReviewed()
	 */
	@Override
	public HashMap<String, String> getEmployeeNamesToBeReviewed(boolean isSummary)
			throws LoginValidationException, MissingEntitiesException {
		Key<InetEmployee> reviewer = LoginPortal.getEmployeeKey(getThreadLocalRequest());
		return getEmployeeNamesToBeReviewed(reviewer, isSummary);
	}   
	
	public static HashMap<String, String> getEmployeeNamesToBeReviewed(Key<InetEmployee> reviewer, boolean isSummary) throws MissingEntitiesException
	{
		Objectify ofy = ObjectifyService.begin();
		HashMap<String, String> pref = InetDAO4CommonReads.getCompanyPreferences(ofy).getPreferences();
		String reviewCycle = pref.get(InetConstants.COMPANY_PREFERENCES_CYCLE);
		
		List<? extends InetReview> reviews;
		
		if(isSummary)
			reviews = InetDAO4CommonReads.getReviews(ofy, reviewCycle, 
					InetReview.QUERY_PARAM_REVIEWER, reviewer, InetReviewSummary.class);
		else
			reviews = InetDAO4CommonReads.getReviews(ofy, reviewCycle, 
					InetReview.QUERY_PARAM_REVIEWER, reviewer, InetReview.class);
		
		if(reviews == null || reviews.size() == 0) return null;
		
		HashMap<String, String> result = new HashMap<String, String>(reviews.size());
		HashSet<Key<InetEmployee>> revieweeKeys = new HashSet<Key<InetEmployee>>(reviews.size());
		for(InetReview rev : reviews)
		{
			if(rev.getReviewee() != null)
				revieweeKeys.add(rev.getReviewee());
			else if(rev.getName() == null)
			{
				String msgFmt = "Reviews are expected to be intialized with reviewee info or a review name: ";
				log.severe(msgFmt + rev.getKey());
				throw new MissingEntitiesException(msgFmt + rev.getKey().getName());
			}
		}
		
		Map<Key<InetEmployee>, InetEmployee> reviewees = InetDAO4CommonReads.getEntities(revieweeKeys, ofy);
		String resultKey, resultVal;
		for(InetReview rev : reviews)
		{
			if(rev.getReviewee() == null )
				resultVal = rev.getName();
			else
			{
				InetEmployee empl = reviewees.get(rev.getReviewee());
				resultVal = empl.getFname() + " " + empl.getLname();
			}
			
			resultKey = ofy.getFactory().keyToString(rev.getKey());		
			result.put(resultKey, resultVal);
		}
		return result;
	}
	
	private static void embedReview(GAEPrimaryEntity[] revObjs, Objectify ofy, ReviewDTO result) throws MissingEntitiesException
	{
		InetReview rev = (InetReview) revObjs[InetDAO4CommonReads.REVIEW_IDX];
		InetQuestions quest = (InetQuestions) revObjs[InetDAO4CommonReads.QUESTION_SET_IDX];
		InetMultiAnswers ans =  (InetMultiAnswers) revObjs[InetDAO4CommonReads.ANS_SET_IDX];
		ArrayList<Key<InetEmployee>> employeeKeys = new ArrayList<Key<InetEmployee>>(3);
		employeeKeys.add(rev.getReviewer());
		
		
		if(rev.getReviewee()!= null && !rev.getReviewer().equals(rev.getReviewee())) 
			employeeKeys.add(rev.getReviewee());
		
		if(rev.getWriter() != null) employeeKeys.add(rev.getWriter());	//TODO, when writer fn done in GUI, protect from being reviewee or reviewer
		
		Map<Key<InetEmployee>, InetEmployee> revEmplInfo = InetDAO4CommonReads.getEntities(employeeKeys, ofy);
		
		HashMap<Integer, String> textAnswers = null;
		if(rev.getTextAnswers() != null)
		{
			textAnswers = new HashMap<Integer, String>(rev.getTextAnswers().size());
			for(int i : rev.getTextAnswers().keySet())
				textAnswers.put(i, rev.getTextAnswers().get(i).getValue());
		}
		
		result.questionSetName = quest.getKey().getName();
		result.reviewId = ofy.getFactory().keyToString(rev.getKey());
		result.lastUpdated = rev.getLastUpdated();
		result.multichoiceAnswers = rev.getMulitchoiceAnswers();	
		result.textAnswers = textAnswers;
		result.locked = rev.isLocked();
		result.reviewCycle = rev.getReviewCycle();
		result.reviewer = revEmplInfo.get(rev.getReviewer()).getFname() + " " + revEmplInfo.get(rev.getReviewer()).getLname();
		result.reviewee = rev.getReviewee() == null ? null : revEmplInfo.get(rev.getReviewee()).getFname() + " " + 
							revEmplInfo.get(rev.getReviewee()).getLname();
		result.questionSet = quest.getQuestionSet();
		result.answerMapping = ans.getAnswerMapping();
		result.name = rev.getName();
		result.alloweRevieweeEdits = rev.isAllowRevieweeEdit();
		result.description = "review";
		
		if(rev.getWriter() != null)
			result.writer = revEmplInfo.get(employeeKeys.get(2)).getFname() + " " + revEmplInfo.get(employeeKeys.get(2)).getLname();
		
	}
	
	/* (non-Javadoc)
	 * @see com.ft360reviews.gui.client.ReadService#getReview(java.lang.String)
	 */
	@Override
	public ReviewDTO getReview(String reviewId) throws MissingEntitiesException {
		ObjectifyOpts opts = new ObjectifyOpts().setSessionCache(true);
		Objectify ofy = ObjectifyService.begin(opts);
		Key<? extends InetReview> reviewKey = ofy.getFactory().stringToKey(reviewId);
		return getReview(reviewKey, ofy);
	}
	
	public static ReviewDTO getReview(Key<? extends InetReview> reviewKey, Objectify ofy) 
		throws MissingEntitiesException
	{
		GAEPrimaryEntity[] revObjs = InetDAO4CommonReads.getReviewObjects(ofy, reviewKey);
		ReviewDTO result = new ReviewDTO();
		embedReview(revObjs, ofy, result);
		return result;		
	}

	/* (non-Javadoc)
	 * @see com.ft360reviews.gui.client.ReadService#getAllEmployeeIDs()
	 */
	@Override
	public HashMap<String, String> getAllEmployeeIDs() {	
		Objectify ofy = ObjectifyService.begin();
		List<InetEmployee> employees = InetDAO4CommonReads.fetchAllEmployees(ofy);
		HashMap<String, String> result = new HashMap<String, String>(employees.size());
		for (InetEmployee empl : employees) 
		{
			String val = empl.getFname() + " " + empl.getLname() + ","
					+ empl.getLoginName();
			result.put(empl.getLoginName(), val);
		}
		return result;
	}

	/* (non-Javadoc)
	 * @see com.ft360reviews.gui.client.ReadService#getReviewSummary(java.lang.String)
	 */
	@Override
	public ReviewSummaryDTO getReviewSummary(String summaryId)
			throws MissingEntitiesException, DuplicateEntitiesException {
		ObjectifyOpts opts = new ObjectifyOpts().setSessionCache(true);
		Objectify ofy = ObjectifyService.begin(opts);
		Key<InetReviewSummary> reviewKey = ofy.getFactory().stringToKey(summaryId);
		return getReviewSummary(reviewKey, ofy);
	}
	
	public static ReviewSummaryDTO getReviewSummary(Key<InetReviewSummary> reviewKey, Objectify ofy) 
		throws MissingEntitiesException, DuplicateEntitiesException
	{
		GAEPrimaryEntity[] revObjs = InetDAO4CommonReads.getReviewObjects(ofy, reviewKey);
		InetReviewSummary rev = (InetReviewSummary) revObjs[InetDAO4CommonReads.REVIEW_IDX];

		
		ReviewSummaryDTO result = new ReviewSummaryDTO();
		//TODO, not enough to query on reviewee ... we have some reviews that need to be queried by name
		HashMap<String, String> pref = InetDAO4CommonReads.getCompanyPreferences(ofy).getPreferences();
		String reviewCycle = pref.get(InetConstants.COMPANY_PREFERENCES_CYCLE);
		
		List<InetReview> reviews;
		if(rev.getReviewee() != null)
			reviews = InetDAO4CommonReads.getReviews(ofy, reviewCycle, 
				InetReview.QUERY_PARAM_REVIEWEE, rev.getReviewee(), InetReview.class);
		else
			reviews = InetDAO4CommonReads.getReviews(ofy, reviewCycle, InetReview.QUERY_PARAM_NAME, rev.getName(), InetReview.class);
					
		//get stored values
		embedReview(revObjs, ofy, result);
		//add calculated values to result
		embeddChoiceReviewAverages(reviews, ofy, result);
		return result;		
	}
	
	private static void embeddChoiceReviewAverages(List<InetReview> reviews, Objectify ofy, ReviewSummaryDTO dto) throws MissingEntitiesException, DuplicateEntitiesException
	{
		if(reviews.size() == 0) return; //nothing to do
		dto.description="summary";
		dto.totalNumOfReviews = reviews.size();
		
		//first lets go compile keys for all the things we'll need to fetch
		HashSet<Key<InetQuestions>> revQuestKeys = new HashSet<Key<InetQuestions>>(reviews.size() * 2);
		for(InetReview rev : reviews)
			revQuestKeys.add(rev.getQuestionSetKey());
		Map<Key<InetQuestions>, InetQuestions> revQuests = InetDAO4CommonReads.getEntities(revQuestKeys, ofy);
		
		HashSet<Key<InetMultiAnswers>> ansMapKeys = new HashSet<Key<InetMultiAnswers>>(revQuests.size());
		for(InetQuestions quest : revQuests.values())
			ansMapKeys.add(quest.getAnswerKey());
		Map<Key<InetMultiAnswers>, InetMultiAnswers> revAnsMaps = InetDAO4CommonReads.getEntities(ansMapKeys, ofy);
		
		//init dto field values
		dto.choiceQuestionSets = new HashMap<String, HashMap<Integer,String>>(revQuests.size());
		dto.choiceAnswermappings = new HashMap<String, HashMap<Integer,String>>(revQuests.size());
		dto.selfchoiceAnswers = new HashMap<String, HashMap<Integer, Integer>>(revQuests.size());
		dto.choicetotaledAnswers = new HashMap<String, HashMap<Integer, Integer>>(revQuests.size());
		dto.reviewerCounts = new HashMap<String, Integer>(revQuests.size());
		dto.childReviewIDs = new HashMap<String, String[]>();
		int submittedCount = 0;
		
		HashMap<String, HashMap<Integer, String>> selfQuestionSets = new HashMap<String, HashMap<Integer,String>>(revQuests.size());
		HashMap<String, HashMap<Integer,String>> selfAnswermappings = new HashMap<String, HashMap<Integer,String>>(revQuests.size());
		
		for(InetReview rev : reviews)
		{
			String revKey = ofy.getFactory().keyToString(rev.getKey());
			if(rev.isLocked())
			{
				String[] temp = {rev.getReviewer().getName(), PanelServiceConstants.LOCK_INDICATOR};
				dto.childReviewIDs.put(revKey, temp);
			}
			else
			{
				String[] temp = {rev.getReviewer().getName(), ""};
				dto.childReviewIDs.put(revKey, temp);				
				continue;
			}
			
			submittedCount++;
			String questionKeyName = rev.getQuestionSetKey().getName();
			log.warning("review: " + rev.getKey());
			log.warning("question key" + questionKeyName);
			
			if(rev.getReviewer().equals(rev.getReviewee()))
			{
				if(dto.selfchoiceAnswers.containsKey(questionKeyName))
				{
					String msgFmt = "Only 1 self %s review expected per question set %s. Found another review %s";
					log.severe(String.format(msgFmt, rev.getReviewee(), rev.getQuestionSetKey(), rev.getKey()));
					throw new DuplicateEntitiesException(String.format(msgFmt, rev.getReviewee().getName(), 
							rev.getQuestionSetKey().getName(), rev.getKey().getName()));
				}
				dto.selfchoiceAnswers.put(questionKeyName, rev.getMulitchoiceAnswers());
				
				InetQuestions quest = revQuests.get(rev.getQuestionSetKey());
				selfQuestionSets.put(questionKeyName, quest.getQuestionSet());
				selfAnswermappings.put(questionKeyName, 
						revAnsMaps.get(quest.getAnswerKey()).getAnswerMapping());				
			}			
			else
			{
				if(dto.choiceQuestionSets.containsKey(questionKeyName))
				{
					//exclude self from totals
					HashMap<Integer, Integer> revChoiceAnswers = rev.getMulitchoiceAnswers();
					HashMap<Integer, Integer> totalChoiceAnswers = dto.choicetotaledAnswers.get(questionKeyName);
	
					for(int questNum : revChoiceAnswers.keySet())
						totalChoiceAnswers.put(questNum, revChoiceAnswers.get(questNum) + (totalChoiceAnswers.get(questNum) == null ? 0 : totalChoiceAnswers.get(questNum)));
					dto.choicetotaledAnswers.put(questionKeyName, totalChoiceAnswers);
					dto.reviewerCounts.put(questionKeyName, (dto.reviewerCounts.get(questionKeyName) == null ? 0 : dto.reviewerCounts.get(questionKeyName)) + 1);
				}
				else
				{
					InetQuestions quest = revQuests.get(rev.getQuestionSetKey());
					HashMap<Integer, Integer> revChoiceAnswers = rev.getMulitchoiceAnswers();
					dto.choiceQuestionSets.put(questionKeyName, quest.getQuestionSet());
					dto.choiceAnswermappings.put(questionKeyName, 
							revAnsMaps.get(quest.getAnswerKey()).getAnswerMapping());
					
					HashMap<Integer, Integer> totalChoiceAnswers = new HashMap<Integer, Integer>(revChoiceAnswers.size());
					for(int questNum : revChoiceAnswers.keySet())
						totalChoiceAnswers.put(questNum, revChoiceAnswers.get(questNum));					
					log.warning("initializing reviewer count with: " + questionKeyName);
					dto.choicetotaledAnswers.put(questionKeyName, totalChoiceAnswers);
					dto.reviewerCounts.put(questionKeyName, 1);
				}
			}
		}
		//check to see we don't have any cases where questionset only belongs to a self-review
		for(String qName : selfQuestionSets.keySet())
		{
			if(!dto.choiceQuestionSets.containsKey(qName))
			{
				log.warning("Found summary comprised of only a self review, question set is => " + qName);
				dto.choiceQuestionSets.put(qName, selfQuestionSets.get(qName));
				dto.choiceAnswermappings.put(qName, selfAnswermappings.get(qName));					
			}
		}		
		dto.totalNumOfLockedReviews = submittedCount;		
	}

	/* (non-Javadoc)
	 * @see com.ft360reviews.gui.client.ReadService#getQuestionIDs()
	 */
	@Override
	public HashMap<String, String> getQuestionIDs() {
		Objectify ofy = ObjectifyService.begin();
		return InetDAO4CommonReads.fetchAllQuestions(ofy);
	}

	/* (non-Javadoc)
	 * @see com.ft360reviews.gui.client.ReadService#getSummaryIDs(java.lang.String)
	 */
	@Override
	public HashMap<String, String> getSummaryIDs(String loginName) 
	{
		Key<InetEmployee> employeeKey = new Key<InetEmployee>(InetEmployee.class, loginName);
		Objectify ofy = ObjectifyService.begin();
		
		HashSet<Key<InetReviewSummary>> summaryKeys = InetDAO4CommonReads.getReviewKeys(ofy, InetReview.QUERY_PARAM_REVIEWEE, employeeKey, InetReviewSummary.class);
		HashMap<String, String> result = new HashMap<String, String>(summaryKeys.size());
		for(Key<InetReviewSummary> sk : summaryKeys)
			result.put(ofy.getFactory().keyToString(sk), sk.getName());
		return result;
	}

	/* (non-Javadoc)
	 * @see com.ft360reviews.gui.client.ReadService#isAdmin()
	 */
	@Override
	public boolean isAdmin() throws LoginValidationException {
		// TODO Auto-generated method stub
		Objectify ofy = ObjectifyService.begin();
		Key<InetEmployee> emplKey = LoginPortal.getEmployeeKey(getThreadLocalRequest());
		HashSet<String> admins = LoginHelper.getAdminUsers(this.getThreadLocalRequest(), ofy);
		return admins.contains(emplKey.getName());
	}

	/* (non-Javadoc)
	 * @see com.ft360reviews.gui.client.ReadService#getNoticeIDs()
	 */
	@Override
	public HashMap<String, String> getNoticeIDs() {
		HashMap<String, String> result = new HashMap<String, String>();
		ArrayList<Key<InetNotice>> noticeKeys = new ArrayList<Key<InetNotice>>(2);
		noticeKeys.add(new Key<InetNotice>(InetNotice.class, InetConstants.LOGGEDIN_NOTICE)); 
		noticeKeys.add(new Key<InetNotice>(InetNotice.class, InetConstants.LOGGEDOUT_NOTICE)); 
		Map<Key<InetNotice>, InetNotice> noticeInfo = ObjectifyService.begin().get(noticeKeys);
		for(InetNotice note : noticeInfo.values())
			result.put(note.getKey().getName(), note.getTitle());
		return result;
	}

	/* (non-Javadoc)
	 * @see com.ft360reviews.gui.client.ReadService#getNotice(java.lang.String)
	 */
	@Override
	public String[] getNotice(String id) throws MissingEntitiesException {
		Objectify ofy = ObjectifyService.begin();
		InetNotice notice = ofy.find(InetNotice.class, id);
		if(notice == null)
		{
			String msgFmt = "Unable to load notice: " + id;
			log.severe(msgFmt + id);
			throw new MissingEntitiesException(msgFmt);
		}
		String[] result = new String[2];
		result[0] = notice.getTitle();
		result[1] = notice.getContent();
		return result;
	}
}
