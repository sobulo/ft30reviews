/**
 * 
 */
package com.ft360reviews.gui.server;


import java.util.HashMap;
import java.util.HashSet;
import java.util.logging.Logger;

import com.ft360reviews.entities.InetDAO4CommonWrites;
import com.ft360reviews.entities.InetEmployee;
import com.ft360reviews.entities.InetNotice;
import com.ft360reviews.entities.InetReview;
import com.ft360reviews.gui.client.WriteService;
import com.ft360reviews.gui.shared.exceptions.DuplicateEntitiesException;
import com.ft360reviews.gui.shared.exceptions.LoginValidationException;
import com.ft360reviews.gui.shared.exceptions.MissingEntitiesException;
import com.ft360reviews.login.BCrypt;
import com.ft360reviews.login.LoginPortal;
import com.google.appengine.api.datastore.Text;
import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.Objectify;
import com.googlecode.objectify.ObjectifyService;


/**
 * @author Segun Razaq Sobulo
 *
 */
public class WriteServiceImpl extends RemoteServiceServlet implements WriteService{
	private static final Logger log = Logger.getLogger(WriteServiceImpl.class.getName());
	
	/* (non-Javadoc)
	 * @see com.ft360reviews.gui.client.WriteService#updateEmployee(java.lang.String, java.lang.String, java.lang.String, java.util.HashSet)
	 */
	@Override
	public void updateEmployee(String fname, String lname, String email,
			HashSet<String> phoneNums) throws MissingEntitiesException,
			LoginValidationException {
		Key<InetEmployee> empKey = LoginPortal.getEmployeeKey(getThreadLocalRequest());
		InetDAO4CommonWrites.updateEmployee(fname, lname, email, phoneNums, empKey);
		log.warning("UPDATE user info for " + empKey + " --by user(self)");		
	}
	
	@Override
	public String changePassword(String oldPassword, String newPassword)
			throws MissingEntitiesException, LoginValidationException {
		Objectify ofy = ObjectifyService.begin();
		Key<InetEmployee> empKey = LoginPortal.getEmployeeKey(getThreadLocalRequest()); 
		InetEmployee user = ofy.find(empKey);
		
		if(user == null)
		{
			String msgFmt = "Unable to change password for %s as user does not exist";
			log.severe(String.format(msgFmt, empKey.toString()));
			throw new MissingEntitiesException(String.format(msgFmt, empKey.getName()));
		}
		else if(!BCrypt.checkpw(oldPassword, user.getPassword()))
		{
			String msgFmt = "Unable to change password for %s. Invalid current password specified";
			log.severe(String.format(msgFmt, empKey.toString()));
			throw new LoginValidationException(String.format(msgFmt, empKey.getName()));			
		}
		InetDAO4CommonWrites.changePassword(empKey, newPassword);
		log.warning("UPDATED password for " + empKey + " --by user(self)");
		return user.getFname() + " " + user.getLname();
	}

	/* (non-Javadoc)
	 * @see com.ft360reviews.gui.client.WriteService#updateReview(java.lang.String, java.util.HashMap, java.util.HashMap)
	 */
	@Override
	public String updateReview(String reviewId, String revieweeId, HashMap<Integer, Integer> multiChoiceAns, 
			HashMap<Integer, String> stringAns, boolean locked, String comments) 
			throws MissingEntitiesException, LoginValidationException, DuplicateEntitiesException 
	{
		
		Key<InetEmployee> updater = LoginPortal.getEmployeeKey(getThreadLocalRequest());
		Objectify ofy = ObjectifyService.begin();
		Key<? extends InetReview> revKey = ofy.getFactory().stringToKey(reviewId);
		HashMap<Integer, Text> textAnswers =  new HashMap<Integer, Text>(stringAns.size());
		for(int i : stringAns.keySet())
			textAnswers.put(i, new Text(stringAns.get(i)));
		Key<InetEmployee> revieweeKey = null;
		if(revieweeId != null)
			revieweeKey = new Key<InetEmployee>(InetEmployee.class, revieweeId);
		InetReview updatedReview = InetDAO4CommonWrites.updateReview(revKey, revieweeKey, multiChoiceAns, textAnswers, locked, comments);
		String status = "Updated employee " + updatedReview.getReviewer().getName() + " review of " 
			+ (updatedReview.getReviewee() == null ? updatedReview.getName() : updatedReview.getReviewee().getName() +
					" for review cycle " + updatedReview.getReviewCycle());
		log.warning(status + " --review key " + updatedReview.getKey() + " --by user " + updater);
		return status;
	}

	/* (non-Javadoc)
	 * @see com.ft360reviews.gui.client.WriteService#updateNotice(java.lang.String, java.lang.String, java.lang.String)
	 */
	@Override
	public void updateNotice(String id, String title, String content) throws MissingEntitiesException {
		Objectify ofy = ObjectifyService.beginTransaction();
		try
		{
			InetNotice notice = ofy.find(InetNotice.class, id);
			if(notice == null)
			{
				String msgFmt = "Unable to load notice: " + id;
				log.severe(msgFmt + id);
				throw new MissingEntitiesException(msgFmt);				
			}
			notice.setTitle(title);
			notice.setContent(content);
			ofy.put(notice);
			ofy.getTxn().commit();
		}
		finally
		{
			if(ofy.getTxn().isActive())
				ofy.getTxn().rollback();
		}
	}	
}
