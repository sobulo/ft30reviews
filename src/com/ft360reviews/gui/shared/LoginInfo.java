/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ft360reviews.gui.shared;
import java.io.Serializable;
import java.util.HashMap;

/**
 *
 * @author Segun Razaq Sobulo
 */

public class LoginInfo implements Serializable {
    private boolean loggedIn = false;
    private boolean admin = false;
	private HashMap<String, String> loginUrl;
    private String logoutUrl;
    private String name;
    private String loginID;
    private String companyName;
    private String companyAccronym;
    private String message;
    
    public boolean isAdmin() {
		return admin;
	}

	public void setAdmin(boolean admin) {
		this.admin = admin;
	}
	
    public String getCompanyAccronym() {
        return companyAccronym;
    }

    public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public void setCompanyAcrronym(String accronym) {
        this.companyAccronym = accronym;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String schoolName) {
        this.companyName = schoolName;
    }

    public boolean isLoggedIn() {
        return loggedIn;
    }

    public void setLoggedIn(boolean loggedIn) {
        this.loggedIn = loggedIn;
    }

    public HashMap<String, String> getLoginUrl() {
        return loginUrl;
    }

    public void setLoginUrl(HashMap<String, String> loginUrl) {
        this.loginUrl = loginUrl;
    }

    public String getLogoutUrl() {
        return logoutUrl;
    }

    public void setLogoutUrl(String logoutUrl) {
        this.logoutUrl = logoutUrl;
    }

    public String getName() {
        return name;
    }

    public void setName(String displayName) {
        this.name = displayName;
    }

    public String getLoginID() {
        return loginID;
    }

    public void setLoginID(String userKey) {
        this.loginID = userKey;
    }
}
