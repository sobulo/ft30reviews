/**
 * 
 */
package com.ft360reviews.gui.shared;

import java.util.HashMap;

/**
 * @author Segun Razaq Sobulo
 *
 */
public class PanelServiceConstants {
	public final static String PROVIDER_FT360 = "FT360";
	public final static String PROVIDER_GOOGLE = "google";
	public final static int FREE_FORM_MARKER = 1000;
	public final static HashMap<String, String> IMAGES_NAMES = new HashMap<String, String>(4);
	static
	{
		IMAGES_NAMES.put(PROVIDER_FT360, "keys_icon.png");
		IMAGES_NAMES.put(PROVIDER_GOOGLE, "google.gif");
		IMAGES_NAMES.put("fieldco", "fieldco.jpg");
	}
    //data validation constants
    public final static String REGEX_EMAIL = "^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,4}$";
    public final static String REGEX_NUMS_ONLY = "^\\d{6,}$";
    public final static String REGEX_PHONE_REPLACE = "-|\\(|\\)";
    public final static String LOCK_INDICATOR = "L";
    
    //request parameter names
    public final static String PARAM_SERVICE_TYPE = "se";
    public final static String PARAM_REVIEW_ID = "rd";
    public final static String PARAM_FETCH_REVIEW = "fhrw";
    public final static String PARAM_FETCH_SUMMARY = "fhrs";
    public final static String PARAM_FETCH_REVIEW_4_SUMMARIZER = "fhrw4rs";
    public final static String PARAM_FETCH_QUESTION = "fhqn";
    public final static String PARAM_QUESTION_ID = "qd";
    
	public final static String LOGGEDIN_NOTICE = "loggedin-note";
	public final static String LOGGEDOUT_NOTICE = "loggedout-note";    
}
