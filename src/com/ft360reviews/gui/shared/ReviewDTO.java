/**
 * 
 */
package com.ft360reviews.gui.shared;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;


/**
 * @author Segun Razaq Sobulo
 *
 */
public class ReviewDTO implements Serializable{
	
	public ReviewDTO(){}
	
	public String reviewer;	
	public String reviewee;
	public String writer;
	public String reviewCycle;
	public HashMap<Integer, Integer> multichoiceAnswers;
	public HashMap<Integer, String> textAnswers;
	public HashMap<Integer, String> questionSet;
	public HashMap<Integer, String> answerMapping;
	public boolean locked;
	public Date lastUpdated;
	public String reviewId;
	public String name;
	public boolean alloweRevieweeEdits;
	public String description;
	public String questionSetName;
	
	public String getHTML()
	{
		if(!locked) return null;
		StringBuilder result = new StringBuilder();
		//build header
		storeHeaderStr(result);
		result.append("<hr width='70%'/>");
		
		
		//build questions
		result.append("<p><b>Multi-choice survey</b><br/><ul>");
		ArrayList<Integer> sortedKeys = new ArrayList<Integer>(multichoiceAnswers.size());
		sortedKeys.addAll(multichoiceAnswers.keySet());
		Collections.sort(sortedKeys);
		for(int i : sortedKeys)
		{
			result.append("<li>").append(i).append(". ").append(questionSet.get(i));
			result.append(" ").append("<br/><b>").append(answerMapping.get(multichoiceAnswers.get(i))).append("</b>");
		}
		result.append("</ul>");
		
		sortedKeys = new ArrayList<Integer>(textAnswers.size());
		sortedKeys.addAll(textAnswers.keySet());
		Collections.sort(sortedKeys);
		result.append("<p><b>Text input survey: </b><br/><ul>");
		for(int i: sortedKeys)
		{
			result.append("<li>").append(i - PanelServiceConstants.FREE_FORM_MARKER).
					append(". ").append(questionSet.get(i)).append("<br/>").append(textAnswers.get(i));
		}
		result.append("</ul>");
		return result.toString();
	}
	
	public void storeHeaderStr(StringBuilder result)
	{
		result.append("<table style='margin-left: auto; margin-right: auto; border-spacing: 20px 1px'><tr><th align='left'>Review Of</th><td>");
		result.append(reviewee==null?name:reviewee).append("</td></tr><tr><th align='left'>Review Cycle</th><td>");
		result.append(reviewCycle).append("</td></tr><tr><th align='left'>Question Set Type</th><td>").append(questionSetName).
		append(reviewer.equals(reviewee)?" -- Self Review": "").append("</td></tr><tr><th align='left'>Reviewer</th><td>");
		result.append(reviewer).append("</td></tr><tr><th align='left'>Last Updated</th><td>").append(lastUpdated.toString());
		result.append("</td></tr></table>");		
	}
}
