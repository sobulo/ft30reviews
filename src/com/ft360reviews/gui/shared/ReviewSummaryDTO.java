/**
 * 
 */
package com.ft360reviews.gui.shared;

import java.util.HashMap;


/**
 * @author Segun Razaq Sobulo
 *
 */
public class ReviewSummaryDTO extends ReviewDTO
{	
	//for storing calculated averages and displaying (calcs done on server side)
	public HashMap<String, Integer> reviewerCounts;
	public HashMap<String, HashMap<Integer, String>> choiceQuestionSets;
	public HashMap<String, HashMap<Integer, Integer>> choicetotaledAnswers;
	public HashMap<String, HashMap<Integer, Integer>> selfchoiceAnswers;
	public HashMap<String, HashMap<Integer, String>> choiceAnswermappings;
	public HashMap<String, String[]> childReviewIDs;
	public int totalNumOfReviews;
	public int totalNumOfLockedReviews;
	
	
	public String getCalculatedAvgsHTML()
	{
		StringBuilder result = new StringBuilder();
		result.append("<p>Found ").append(totalNumOfLockedReviews).append(" submitted reviews out of a total of ").append(totalNumOfReviews);
		result.append(" requested reviews. Summary below includes only submitted reviews</p>");
		//GWT.log("Choice Quest Set: " + choiceQuestionSets);
		for(String questionSetName : choiceQuestionSets.keySet())
		{
			HashMap<Integer, String> questions = choiceQuestionSets.get(questionSetName);
			HashMap<Integer, Integer> selfAns = selfchoiceAnswers.get(questionSetName);
			HashMap<Integer, Integer> totalAns = choicetotaledAnswers.get(questionSetName);
			HashMap<Integer, String> ansMap = choiceAnswermappings.get(questionSetName);
			int numOfReviewers = reviewerCounts.get(questionSetName);
			String othersTitle = numOfReviewers + " Others";
			int selfTotal = 0;
			int selfCount = 0;
			float avgTotal = 0;
			int avgCount = 0;
			
			result.append("<table border='1' cellspacing='5' cellpadding='0'><tr><th colspan='5' align='center'>").
					append(questionSetName).append("</th></tr>");
			result.append("<tr><th>Question</th><th>").append(othersTitle).append(" Avg</th><th>").
				append(othersTitle).append(" Rank</th><th>Self Avg</th><th>Self Rank</th></tr>");
			for(Integer questNum : questions.keySet())
			{
				Integer selfAnsVal = selfAns == null ? null : selfAns.get(questNum);
				Integer totalAnsVal = totalAns == null ? null : totalAns.get(questNum);
				
				float avgAnsVal;
				int avgAnsValChoice;
				if(selfAnsVal == null && totalAnsVal == null)
					continue;
				result.append("<tr><td>").append(questNum).append(". ").append(questions.get(questNum)).append("</td><td>");
				if(selfAnsVal != null && totalAnsVal != null)
				{
					avgAnsVal = totalAnsVal / (float) numOfReviewers;
					avgAnsValChoice = Math.round(avgAnsVal);
					avgCount++;
					avgTotal += avgAnsVal;
					selfCount++;
					selfTotal += selfAnsVal;
					
					result.append(avgAnsVal).append("</td><td>").append(ansMap.get(avgAnsValChoice)).
					append("</td><td>").append(selfAnsVal).append("</td><td>").append(ansMap.get(selfAnsVal)).append("</td></tr>");
				}
				else if(selfAnsVal == null && totalAnsVal != null)
				{
					avgAnsVal = totalAnsVal / (float) numOfReviewers;
					avgAnsValChoice = Math.round(avgAnsVal);
					avgCount++;
					avgTotal += avgAnsVal;
					
					result.append(avgAnsVal).append("</td><td>").append(ansMap.get(avgAnsValChoice)).
					append("</td><td> </td><td> </td></tr>");					
				}
				else
				{
					selfCount++;
					selfTotal += selfAnsVal;					
					result.append(" </td><td> </td><td>").append(selfAnsVal).
						append("</td><td>").append(ansMap.get(selfAnsVal)).append("</td></tr>");					
				}
			}
			result.append("<tr><th>Total Average</th><th>");
			if(selfCount != 0 && avgCount != 0)
			{
				float selfAvg = selfTotal/(float) selfCount;
				int selfAvgChoice = Math.round(selfAvg);
				float avgAvg = avgTotal / (float) avgCount;
				int avgAvgChoice = Math.round(avgAvg);
				
				result.append(avgAvg).append("</th><th>").append(ansMap.get(avgAvgChoice)).
				append("</th><th>").append(selfAvg).append("</th><th>").append(ansMap.get(selfAvgChoice)).append("</th></tr>");
			}
			else if(selfCount == 0 && avgCount != 0)
			{
				float avgAvg = avgTotal / (float) avgCount;
				int avgAvgChoice = Math.round(avgAvg);
				
				result.append(avgAvg).append("</th><th>").append(ansMap.get(avgAvgChoice)).
				append("</th><th> </th><th> </th></tr>");				
			}
			else if(selfCount != 0 && avgCount == 0)
			{
				float selfAvg = selfTotal/(float) selfCount;
				int selfAvgChoice = Math.round(selfAvg);
				
				result.append(" </th><th> </th><th>").append(selfAvg).append("</th><th>").append(ansMap.get(selfAvgChoice)).append("</th></tr>");			
			}
			else
			{	
				result.append(" </th><th> </th><th> </th><th> </th></tr>");			
			}
			result.append("</table>");
		}
		return result.toString();
	}
	
	/* (non-Javadoc)
	 * @see com.ft360reviews.gui.shared.ReviewDTO#getHTML()
	 */
	@Override
	public String getHTML() {
		// TODO Auto-generated method stub
		StringBuilder result = new StringBuilder();
		result.append(super.getHTML());
		result.append(getCalculatedAvgsHTML());
		return result.toString();
	}
	//TODO, html function for printing calculated
	//TODO, html function for printing all
	//TODO, function to convert to table messages for use with PSCrollTable
}
