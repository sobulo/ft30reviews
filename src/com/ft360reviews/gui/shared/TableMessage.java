package com.ft360reviews.gui.shared;

import java.io.Serializable;
import java.util.Date;

/**
 * A simple dto model object
 *
 * ArrayList for each type supported, user is expected to know index containing
 * corresponding to requested and send it as parameter to appropiate getter/setter
 * for the requested type
 */
public class TableMessage implements Serializable{
    private String[] textFields;
    private Double[] numberFields;
    private Date[] dateFields;
    private String messageId;


    public TableMessage(){}

    /**
     * @param numOfTextFields
     * @param numOfDoubleFields
     * @param numOfDateFields
     */
    public TableMessage(int numOfTextFields, int numOfDoubleFields,
            int numOfDateFields)
    {
        if (numOfTextFields > 0) {
            textFields = new String[numOfTextFields];
        }
        if (numOfDoubleFields > 0) {
            numberFields = new Double[numOfDoubleFields];
        }
        if (numOfDateFields > 0) {
            dateFields = new Date[numOfDateFields];
        }
    }

    public String getMessageId() {
		return messageId;
	}

	public void setMessageId(String messageId) {
		this.messageId = messageId;
	}

	public int getNumberOfDateFields()
    {
        return( dateFields == null ? 0 : dateFields.length);
    }

    public int getNumberOfTextFields()
    {
        return( textFields == null ? 0 : textFields.length);
    }

    public int getNumberOfDoubleFields()
    {
        return( numberFields == null ? 0 : numberFields.length);
    }

    public String getText(int index) {
        return textFields[index];
    }

    public void setText(int index, String val) {
        textFields[index] = val;
    }

    public Double getNumber(int index) {
        return numberFields[index];
    }

    public void setNumber(int index, Number val) {
    	Double set = val == null? null : val.doubleValue();
        numberFields[index] = set;
    }

    public Date getDate(int index) {
        return dateFields[index];
    }

    public void setDate(int index, Date val) {
        dateFields[index] = val;
    }

    @Override
    public String toString() {
        String result = "Text: ";
        if (textFields == null) {
            result += "None ";
        } else {
            for (int i = 0; i < textFields.length; i++) {
                result += i + "->" + getText(i) + " ";
            }
        }
        result += "\nNumber: ";
        if (numberFields == null) {
            result += "None ";
        } else {
            for (int i = 0; i < numberFields.length; i++) {
                result += i + "->" + getNumber(i) + " ";
            }
        }
        result += "\nDate: ";
        if (dateFields == null) {
            result += "None ";
        } else {
            for (int i = 0; i < dateFields.length; i++) {
                result += i + "->" + getDate(i) + " ";
            }
        }
        return result;
    }
}
