/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ft360reviews.gui.shared.exceptions;



/**
 *
 * @author Segun Razaq Sobulo
 */
public class DuplicateEntitiesException extends Exception{

    public DuplicateEntitiesException(){}

    public DuplicateEntitiesException(String s) {
        super(s);
    }
}
