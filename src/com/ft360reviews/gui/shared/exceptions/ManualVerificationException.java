package com.ft360reviews.gui.shared.exceptions;

@SuppressWarnings("serial")
public class ManualVerificationException extends Exception {
	public ManualVerificationException(){}
	
	public ManualVerificationException(String s)
	{
		super(s);
	}
}
