/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ft360reviews.gui.shared.exceptions;

/**
 *
 * @author Segun Razaq Sobulo
 */
public class MultipleEntitiesException extends Exception{
    public MultipleEntitiesException(){}

    public MultipleEntitiesException(String s)
    {
        super(s);
    }
}
