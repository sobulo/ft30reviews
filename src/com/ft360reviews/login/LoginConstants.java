/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ft360reviews.login;
import java.util.Arrays;
import java.util.HashMap;

/**
 *
 * @author Segun Razaq Sobulo
 */
public class LoginConstants
{
    public final static String DEVELOPMENT_NAMESPACE = "test";

    public final static HashMap<String, String> ALLOWED_NAMESPACES = new HashMap<String, String>();

    /**
     * super user login list
     */
    public final static String[] SUPER_USERS =
    {
    	"ajibola.abudu@fieldcolimited.com", "oluyemi.adeosun@fieldcolimited.com","admin@fieldcolimited.com", "admin@fertiletech.com", "sobulo@fertiletech.com", "damola@fertiletech.com"
    };

    //set init state of constants
    static
    {
        Arrays.sort(SUPER_USERS);
        
        //Fieldco namespaces
        ALLOWED_NAMESPACES.put("360review.fieldcolimited.com", "fieldco360"); //first cycle in 360
        ALLOWED_NAMESPACES.put("360kpi.appspot.com", "demo");
    }
    
    final static String PROVIDER_SESSION = "ft360.provider";
    final static String LOGIN_URL_SESSION = "ft360.loginurl";
    final static String LOGOUT_URL_SESSION = "ft360.logouturl";
    final static String CACHED_LOGIN_INFO_SESSION = "ft360.loginconfirmed";
    final static String TYPE = "service";
    final static String TYPE_SIGN_OUT = "signout";
    final static String TYPE_SIGN_IN = "signin";
    final static String USER_SESSION = "ft360.login";
    final static String ADMIN_SESSION ="ft360.admins";
}
