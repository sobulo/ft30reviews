/**
 * 
 */
package com.ft360reviews.login;

import java.io.Serializable;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.ft360reviews.entities.InetCompany;
import com.ft360reviews.entities.InetConstants;
import com.ft360reviews.entities.InetDAO4CommonReads;
import com.ft360reviews.entities.InetEmployee;
import com.ft360reviews.entities.InetPreferences;
import com.ft360reviews.gui.shared.LoginInfo;
import com.ft360reviews.gui.shared.PanelServiceConstants;
import com.ft360reviews.gui.shared.exceptions.LoginValidationException;
import com.ft360reviews.gui.shared.exceptions.MissingEntitiesException;
import com.google.appengine.api.users.User;
import com.google.appengine.api.users.UserServiceFactory;
import com.googlecode.objectify.NotFoundException;
import com.googlecode.objectify.Objectify;
import com.googlecode.objectify.ObjectifyOpts;
import com.googlecode.objectify.ObjectifyService;

/**
 * @author Segun Razaq Sobulo
 *
 */
public class LoginHelper {
	private static final Logger log = Logger.getLogger(LoginHelper.class.getName());
	private static LoginProvider[] registeredProviders;	
    
    public static LoginInfo brokeredLogin(String requestUri, HttpServletRequest req) throws MissingEntitiesException, LoginValidationException {
        HttpSession session = req.getSession();
        log.warning("Received a brokered login request");
        LoginProvider provider = (LoginProvider) session.getAttribute(LoginConstants.PROVIDER_SESSION);

        if (provider != null && provider.obtainedToken(req)) {
            //try getting previously stored login calc
            LoginInfo loginInfo = (LoginInfo) session.getAttribute(LoginConstants.CACHED_LOGIN_INFO_SESSION);

            //cached object still valid?
            if (loginInfo != null) {
                return loginInfo;
            }

            //no valid cached object so recalculate, i.e. check passwords etc
            session.removeAttribute(LoginConstants.CACHED_LOGIN_INFO_SESSION);
            loginInfo = new LoginInfo();
            Objectify ofy = ObjectifyService.begin(new ObjectifyOpts().
                    setSessionCache(true));
            //throws exception if not valid, also sets user key
            try 
            {
                provider.confirmToken(loginInfo, req, ofy);
            }
            catch (Exception ex)
            {
                clearAllSessions(req); //clear state if we can't validate
                throw new LoginValidationException(ex.getMessage()); //rethrow
            }

            //done setting provider specific fields, now set general fields
            loginInfo.setLoggedIn(true);
            loginInfo.setLogoutUrl(createLougoutUrl(requestUri, provider, req));

            //cache login obj
            session.setAttribute(LoginConstants.CACHED_LOGIN_INFO_SESSION, loginInfo);

            return loginInfo;
        } else 
        {
            LoginInfo loginInfo = new LoginInfo();
            loginInfo.setLoggedIn(false);
            loginInfo.setMessage("Login using one of the links below");
            loginInfo.setLoginUrl(createLoginUrls(requestUri, getRegisteredProviders(), req));
            Objectify ofy = ObjectifyService.begin();
            fillInCompanyInfo(loginInfo, ofy);
            return loginInfo;
        }
    }
    
    public static void clearAllSessions(HttpServletRequest req)
    {
        HttpSession session = req.getSession();
        session.removeAttribute(LoginConstants.USER_SESSION);
        session.removeAttribute(LoginConstants.PROVIDER_SESSION);
        session.removeAttribute(LoginConstants.CACHED_LOGIN_INFO_SESSION);
        session.removeAttribute(LoginConstants.LOGIN_URL_SESSION);
        session.removeAttribute(LoginConstants.ADMIN_SESSION);
    }
    
    public static HashSet<String> getAdminUsers(HttpServletRequest req, Objectify ofy)
    {
    	HttpSession session = req.getSession();
    	Object temp = session.getAttribute(LoginConstants.ADMIN_SESSION);
    	
    	if(temp != null)
    		return (HashSet<String>) temp;
    	
    	HashSet<String> result = new HashSet<String>();
    	

    	InetPreferences companyPref = ofy.find(InetPreferences.class, InetConstants.COMPANY_PREFERENCES_ID);
    	if(companyPref == null)
    	{
    		log.warning("Unable to find company preferences: " + InetConstants.COMPANY_PREFERENCES_ID);
    		return result;
    	}
    	
    	String adminStr = companyPref.getPreferences().get(InetConstants.COMPANY_ADMIN_USERS);
    	
    	if(adminStr == null)
    	{
    		log.warning("Unable to find company preferences: " + InetConstants.COMPANY_PREFERENCES_ID);
    		return result;    		
    	}
    	
    	String[] adminIDs = adminStr.split(",");
    	
    	for(String admin : adminIDs)
    		result.add(admin);
    	
    	//add super users
    	for(String admin : LoginConstants.SUPER_USERS)
    		result.add(admin);
    	
    	session.setAttribute(LoginConstants.ADMIN_SESSION, result);
    	return result;
    }

    private static String createLougoutUrl(String url, LoginProvider provider,
            HttpServletRequest req)
    {
        HttpSession session = req.getSession();

        //store actual checkout url
        session.setAttribute(LoginConstants.LOGOUT_URL_SESSION, provider.getCheckoutUrl(url));

        //return url that we'll use to determine chosen checkout provider
        return "http://" + req.getHeader("Host") + "/login?" + LoginConstants.TYPE + "=" +
                LoginConstants.TYPE_SIGN_OUT + "&continue=" + provider.getProviderName();
    }

    private static HashMap<String, String> createLoginUrls(String url, LoginProvider[] providers,
            HttpServletRequest req) {
        HttpSession session = req.getSession();
        HashMap<String, String> savedUrls =
                (HashMap<String, String>) session.getAttribute(LoginConstants.LOGIN_URL_SESSION);

        if (savedUrls == null) {
            savedUrls = new HashMap<String, String>(providers.length);
        }

        //build brokered urls
        HashMap<String, String> result = new HashMap<String, String>(providers.length);
        for (LoginProvider provider : providers) {
            //build url that will use to delegate to provider appropiate provider
            result.put(provider.getProviderName(), "http://" + req.getHeader("Host") + "/login?" +
                    LoginConstants.TYPE + "=" + LoginConstants.TYPE_SIGN_IN + "&continue=" + provider.getProviderName());

            //store actual checkin url
            savedUrls.put(provider.getProviderName(), provider.getCheckInUrl(url));
        }
        session.setAttribute(LoginConstants.LOGIN_URL_SESSION, savedUrls);
        return result;
    }    
    public static LoginProvider[] getRegisteredProviders() {
        if (registeredProviders == null) {
            LoginProvider[] loginProviders = new LoginProvider[1];
            //loginProviders[0] = new GREPLoginProvider();
            loginProviders[0] = new GoogleLoginProvider();
            registeredProviders = loginProviders;
        }
        return registeredProviders;
    }
    
    public static void fillInCompanyInfo(LoginInfo loginInfo, Objectify ofy)
    {      
    	try
    	{
    		InetCompany c = InetDAO4CommonReads.getCompany(ofy);
            loginInfo.setCompanyAcrronym(c.getAccronym());
            loginInfo.setCompanyName(c.getCompanyName());      		
    	}
    	catch(NotFoundException ex)
    	{
    		//school hasn't been created yet
            loginInfo.setCompanyAcrronym("NAY");
            loginInfo.setCompanyName("Not Applicable Yet");
    	}          	
    }
    
    private static void fillInLoginUserInfo( LoginInfo loginInfo, InetEmployee employee, Objectify ofy)
    {
        loginInfo.setLoginID(employee.getKey().getName());
        loginInfo.setName(new StringBuilder(employee.getFname()).append(" ").
                append(employee.getLname()).toString());
        loginInfo.setAdmin(false);
        LoginHelper.fillInCompanyInfo(loginInfo, ofy);
    }

    private static class GREPLoginProvider implements LoginProvider, Serializable {

        @Override
        public String getCheckoutUrl(String url) {
            return url;
        }

        @Override
        public String getCheckInUrl(String url) {
            return url;
        }

        @Override
        public String getProviderName() {
            return PanelServiceConstants.PROVIDER_FT360;
        }

        @Override
        public boolean obtainedToken(HttpServletRequest req) {
            return getUser(req) != null;
        }

        @Override
        public void confirmToken(LoginInfo loginInfo, HttpServletRequest req,
                Objectify ofy) throws LoginValidationException {
            //get loginname and school to log into
            UserSessionObj user = getUser(req);
            String loginName = user.getUserName();

            //search through user entities
            InetEmployee persistedUser = ofy.find(InetEmployee.class, loginName);

            if (persistedUser != null) {
                validateUser(user, persistedUser);
                fillInLoginUserInfo(loginInfo, persistedUser, ofy);
                return;
            }
            else
            	throw new LoginValidationException("Invalid login name");
        }

        private void validateUser(UserSessionObj userLogin,
                InetEmployee persistedUser) throws LoginValidationException
        {
            if(!BCrypt.checkpw(userLogin.getPassword(), persistedUser.getPassword()))
                throw new LoginValidationException("Invalid password");
        }

        public UserSessionObj getUser(HttpServletRequest req) {
            return (UserSessionObj) req.getSession().getAttribute(LoginConstants.USER_SESSION);
        }
    }

    private static class GoogleLoginProvider implements LoginProvider, Serializable {

        @Override
        public String getCheckoutUrl(String url) {
            return UserServiceFactory.getUserService().createLogoutURL(url);
        }

        @Override
        public String getCheckInUrl(String url) {
            return UserServiceFactory.getUserService().createLoginURL(url);
        }

        @Override
        public String getProviderName() {
            return PanelServiceConstants.PROVIDER_GOOGLE;
        }

        @Override
        public boolean obtainedToken(HttpServletRequest req) {
            return UserServiceFactory.getUserService().isUserLoggedIn();
        }

        @Override
        public void confirmToken(LoginInfo loginInfo, HttpServletRequest req, Objectify ofy) throws LoginValidationException {
            User user = UserServiceFactory.getUserService().getCurrentUser();
            String email = user.getEmail().toLowerCase();
            log.warning ("Attempting to find super user: [" + email + "]");
            if (Arrays.binarySearch(LoginConstants.SUPER_USERS, email) >= 0)
            {
            	LoginHelper.fillInCompanyInfo(loginInfo, ofy);
                loginInfo.setName(user.getNickname());
                loginInfo.setLoginID(user.getEmail());
                loginInfo.setAdmin(true);
                return;
            }
            log.warning("Attempting to find user: [" + email + "]");
            try
            {
                    InetEmployee persistedUser = ofy.find(InetEmployee.class, email);
                    if (persistedUser != null) {
                        fillInLoginUserInfo(loginInfo, persistedUser, ofy);
                        return;
                    }
                

                //else couldn't find the loginname
                String msg = "This account [" + email + "] is not" +
                        " registered on the review system. Contact technology@fertiletech.com if you're a FieldCo Employee";
                log.severe(msg);
                throw new LoginValidationException(msg);
            }
            catch(RuntimeException ex)
            {
                throw new LoginValidationException(ex.getMessage());
            }
        }
    }    
    
    static interface LoginProvider {

        String getCheckoutUrl(String url);

        String getCheckInUrl(String url);

        String getProviderName();

        boolean obtainedToken(HttpServletRequest req);

        void confirmToken(LoginInfo loginInfo, HttpServletRequest req,
                Objectify ofy) throws LoginValidationException;
    }    
    
}
