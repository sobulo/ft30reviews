/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ft360reviews.login;


import java.io.IOException;
import java.util.HashMap;
import java.util.logging.Logger;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.ft360reviews.entities.InetEmployee;
import com.ft360reviews.gui.shared.LoginInfo;
import com.ft360reviews.gui.shared.PanelServiceConstants;
import com.ft360reviews.gui.shared.exceptions.LoginValidationException;
import com.ft360reviews.login.LoginHelper.LoginProvider;
import com.googlecode.objectify.Key;

/**
 *
 * @author Segun Razaq Sobulo
 *
 * Logic in here is hack after hack. Do not modify this file without extensive
 * code reviews. Ideally this should all be replaced by OpenID mechanism.
 */
public class LoginPortal extends HttpServlet{
   
    private static final Logger log = Logger.getLogger(LoginPortal.class.getName());

    @Override
    public void doPost(HttpServletRequest req, HttpServletResponse resp)
            throws IOException {
        String user = req.getParameter("user");
        String password = req.getParameter("password");
        String signinDestination = req.getParameter("continue");

        if (user == null) {
            resp.setContentType("text/html");
            resp.getOutputStream().println(displayError("Invalid Username"));
            return;
        }

        if (password == null) {
            resp.setContentType("text/html");
            resp.getOutputStream().println(displayError("Invalid Password"));
            return;
        }

        if (signinDestination == null) {
            resp.setContentType("text/html");
            resp.getOutputStream().println(displayError("Invalid Signin Destination"));
            return;
        }

        //TODO, store hash of password not actual password
        HttpSession session = req.getSession();
        UserSessionObj sessionObj = new UserSessionObj();
        sessionObj.setUserName(user);
        sessionObj.setPassword(password);
        session.setAttribute(LoginConstants.USER_SESSION, sessionObj);

        //redirect to continue url
        resp.sendRedirect(signinDestination);
    }



    @Override
    public void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws IOException {
        String output = "";
        String type = req.getParameter(LoginConstants.TYPE);
        String provider = req.getParameter("continue");

        if (provider == null) {
            resp.setContentType("text/html");
            resp.getOutputStream().println(displayError("Invalid URL"));
            return;
        }

        if (type == null) {
            output = displayError("Unable to determine access type");
        } else if (type.equals(LoginConstants.TYPE_SIGN_IN)) {
            HttpSession session = req.getSession();

            //signout if we're already signed in, we do not remove login_url_session
            //as it is needed during login process
            session.removeAttribute(LoginConstants.USER_SESSION);
            session.removeAttribute(LoginConstants.CACHED_LOGIN_INFO_SESSION);
            session.removeAttribute(LoginConstants.PROVIDER_SESSION);

            HashMap<String, String> whereTO = (HashMap<String, String>) session.getAttribute(LoginConstants.LOGIN_URL_SESSION);

            if (whereTO == null || !whereTO.containsKey(provider)) {
                resp.setContentType("text/html");
                resp.getOutputStream().println(displayError("Invalid Login Provider"));
                return;
            }


            //store provider so we know what type of logut url to generate
            LoginProvider[] loginProviders = LoginHelper.getRegisteredProviders();
            boolean found = false;
            for (int i = 0; i < loginProviders.length; i++) {
                if (provider.equals(loginProviders[i].getProviderName())) {
                    session.setAttribute(LoginConstants.PROVIDER_SESSION, loginProviders[i]);
                    found = true;
                    break;
                }
            }

            if (found == false) {
                resp.setContentType("text/html");
                //unexpected cuz this shouldn't happen since whereTO check above passed
                //if we get this error most likely a bug has been introduced by a recent
                //code change
                resp.getOutputStream().println(displayError("Unexpected Invalid Login Provider"));
                return;
            }

            //route request
            String url = whereTO.get(provider);
            if (provider.equals(PanelServiceConstants.PROVIDER_GOOGLE)) {
                resp.sendRedirect(url);
            } /*else if (provider.equals(PanelServiceConstants.PROVIDER_FT360)) {
                output = drawForm(url);
            }*/ else {
                resp.setContentType("text/html");
                resp.getOutputStream().println(displayError("Improper setup of" +
                        " login provider"));
                return;
            }
        } else if (type.equals(LoginConstants.TYPE_SIGN_OUT)) {
            LoginHelper.clearAllSessions(req);
            HttpSession session = req.getSession();
            String logouturl = (String) session.getAttribute(LoginConstants.LOGOUT_URL_SESSION);
            if (logouturl == null) {
                output = displayError("Logout only partially completed, " +
                        "close browser to ensure complete logout");
            } else {
                //redirect to signout url
                resp.sendRedirect(logouturl);
            }
        } else {
            output = displayError("Unauthorized Access");
        }

        //display result to client
        resp.setContentType("text/html");
        resp.getOutputStream().println(output);
    }

    private String drawxForm(String url) {
        String oldReturn = "<html><body><center><table><tr><td>" +
        		"<img src='ft360reviews/images/keys_icon.png' /></td><td valign='middle'>" + 
        		"<form name='username' action='/login' method='post'><table bgcolor='#fdf69d'>\n" +
                "<tr><td>Username</td><td><input type='text' name='user'/></td></tr>\n" +
                "<tr><td>Password</td><td><input type='password' name='password'/></td></tr>\n" +
                "<tr><td colspan='2' align='center'><input type='submit' value ='Login' /></td></tr>" +
                "</table><input type='hidden' name='continue' value='" + url +
                "'/></form></td></tr></table>" +
                "</center></body></html>";
        return "<html><body>Custom login has been disabled. Please contact technology@fertiletech.com to enable</body></html>";
    }

    private String displayError(String error) {
        return "<html><body><center><b>" + error + "</b></center></body></html>";
    }

    public static void checkBrokeredLogin(HttpServletRequest req) throws LoginValidationException {
        HttpSession session = req.getSession();
        LoginInfo loginInfo = (LoginInfo) session.getAttribute(LoginConstants.CACHED_LOGIN_INFO_SESSION);

        if (loginInfo == null) {
            throw new LoginValidationException("Not logged in");
        }
    }

    public static LoginInfo getLoginInfo(HttpServletRequest req) throws LoginValidationException
    {
        HttpSession session = req.getSession();
        LoginProvider provider = (LoginProvider) session.getAttribute(LoginConstants.PROVIDER_SESSION);

        if (provider != null) {
            //try getting previously stored login calc
            LoginInfo loginInfo = (LoginInfo) session.getAttribute(LoginConstants.CACHED_LOGIN_INFO_SESSION);

            //if provider session still valid
            if (provider.obtainedToken(req) && loginInfo != null) {
                return loginInfo;
            }
        }
        throw new LoginValidationException("User not logged in");
    }
    
    public static Key<InetEmployee> getEmployeeKey(HttpServletRequest req) throws LoginValidationException
    {
        LoginInfo info = LoginPortal.getLoginInfo(req);
        return new Key<InetEmployee>(InetEmployee.class, info.getLoginID());
    }    
}

