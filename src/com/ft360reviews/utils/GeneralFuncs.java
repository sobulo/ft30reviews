/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ft360reviews.utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.logging.Logger;

import com.googlecode.objectify.Key;

/**
 *
 * @author Segun Razaq Sobulo
 */
public class GeneralFuncs 
{
    public static Date getDate(int year, int month, int day)
    {
        //offset month by 1 as gregorian calendar represents months from 0-11
        return (new GregorianCalendar(year, month - 1, day)).getTime();
    }
    
	public static void printStackTrace(Exception ex, Logger log, boolean showAll)
	{
		StackTraceElement[] trace = ex.getStackTrace();
		for(StackTraceElement elem : trace)
		{
			String exMsg = elem.toString();
			if(showAll || exMsg.startsWith("com.ft360"))	
				log.warning(exMsg);
		}
	}    

    /**
     *
     * @param <T>
     * @param a - first array
     * @param b - second array to compare with first
     * @param intersect
     * @param aOnly - elements found in aOnly
     * @param bOnly - elements found in bOnly
     */
    public static <T> void arrayDiff(Key<T>[] a, Key<T>[] b,
            ArrayList<Key<T>> intersect, ArrayList<Key<T>> aOnly, ArrayList<Key<T>> bOnly )
    {
        Arrays.sort(a);
        Arrays.sort(b);
        int i = 0;
        int j = 0;
        while(i < a.length || j < b.length)
        {
            if(i == a.length)
            {
                bOnly.add(b[j]);
                j++;
                continue;
            }

            if(j == b.length)
            {
                aOnly.add(a[i]);
                i++;
                continue;
            }

            int comp = a[i].compareTo(b[j]);
            if(comp == 0)
            {
                intersect.add(a[i]);
                i++;
                j++;
            }
            else if(comp < 0 )
            {
                aOnly.add(a[i]);
                i++;
            }
            else
            {
                bOnly.add(b[j]);
                j++;
            }
        }

    }

}
