/**
 * 
 */
package com.ft360reviews.utils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.logging.Logger;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.ft360reviews.entities.InetConstants;
import com.ft360reviews.entities.InetDAO4CommonReads;
import com.ft360reviews.entities.InetEmployee;
import com.ft360reviews.entities.InetMultiAnswers;
import com.ft360reviews.entities.InetQuestions;
import com.ft360reviews.entities.InetReview;
import com.ft360reviews.entities.InetReviewSummary;
import com.ft360reviews.gui.server.ReadServiceImpl;
import com.ft360reviews.gui.shared.PanelServiceConstants;
import com.ft360reviews.gui.shared.ReviewDTO;
import com.ft360reviews.gui.shared.exceptions.DuplicateEntitiesException;
import com.ft360reviews.gui.shared.exceptions.LoginValidationException;
import com.ft360reviews.gui.shared.exceptions.MissingEntitiesException;
import com.ft360reviews.login.LoginHelper;
import com.ft360reviews.login.LoginPortal;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.Objectify;
import com.googlecode.objectify.ObjectifyService;

/**
 * @author Segun Razaq Sobulo
 *
 */
public class ReportGenerator extends HttpServlet{
	private static final Logger log = Logger.getLogger(ReportGenerator.class.getName());
	
	private <T extends InetReview> boolean checkEligibility(Key<InetEmployee> reviewer, 
			Key<T> revKey, Objectify ofy, HttpServletRequest req, boolean isSummary) throws MissingEntitiesException
	{
		HashMap<String, String> pref = InetDAO4CommonReads.getCompanyPreferences(ofy).getPreferences();
		String reviewCycle = pref.get(InetConstants.COMPANY_PREFERENCES_CYCLE);
	
		boolean result = InetDAO4CommonReads.getReviewKeys(ofy, reviewCycle, InetReview.QUERY_PARAM_REVIEWER, 
				reviewer, isSummary?InetReviewSummary.class : InetReview.class).contains(revKey);
		
		if(result)
			return true;
		//else if(isSummary)
		else
		{
			//for summaries before returning false, lets check whether this user is an admin
			HashSet<String> admins = LoginHelper.getAdminUsers(req, ofy);
			return admins.contains(reviewer.getName());
		}
		//return false;
	}
	
	private boolean checkSummarizerEligibility(Key<InetEmployee> summarizer, Key<InetReview> revKey, Objectify ofy)
	{
		//fetch the review, then get reviewee and cycle. afterwards fetch the summary then see if it's reviewer equals the summarizer
		InetReview rev = ofy.get(revKey);
		String summaryPrefix = rev.getReviewee() == null? rev.getName() : rev.getReviewee().getName();
		String summaryKeyName = InetReviewSummary.getKeyName(summaryPrefix, rev.getReviewCycle());
		InetReviewSummary summary = ofy.get(InetReviewSummary.class, summaryKeyName);
		return summary.getReviewer().equals(summarizer);
	}

	@Override
	public void doGet(HttpServletRequest req, HttpServletResponse res) throws IOException
	{

		String serviceType = req.getParameter(PanelServiceConstants.PARAM_SERVICE_TYPE);
		if(serviceType == null)
			throw new IllegalArgumentException("Unable to determine service type, aborting request");
		Objectify ofy = ObjectifyService.begin();
		res.setContentType("text/html");
		res.getOutputStream().println("<html><body><div width='80%'>");

		try
		{
				
			if(serviceType.equals(PanelServiceConstants.PARAM_FETCH_REVIEW))
			{
				String reviewId = req.getParameter(PanelServiceConstants.PARAM_REVIEW_ID);
				
				if(reviewId == null)
					throw new IllegalArgumentException("Unable to determine review id, aborting request");				
				
				Key<InetReview> reviewKey = ofy.getFactory().stringToKey(reviewId);
			
				Key<InetEmployee> empKey = LoginPortal.getEmployeeKey(req);
	            if(!checkEligibility(empKey, reviewKey, ofy, req, false))
	            	throw new LoginValidationException("You're not entitled to view this review");
				ReviewDTO dto = ReadServiceImpl.getReview(reviewKey, ofy);
				if(dto.locked)
					res.getOutputStream().println(dto.getHTML());
				else
					throw new IllegalStateException("Attempted generating report for an unlocked review");			
			}
			else if(serviceType.equals(PanelServiceConstants.PARAM_FETCH_SUMMARY))
			{
				String summaryId = req.getParameter(PanelServiceConstants.PARAM_REVIEW_ID);
				
				if(summaryId == null)
					throw new IllegalArgumentException("Unable to determine review type, aborting request");				
				
				Key<InetReviewSummary> summaryKey = ofy.getFactory().stringToKey(summaryId);
				
				Key<InetEmployee> empKey = LoginPortal.getEmployeeKey(req);
	            if(!checkEligibility(empKey, summaryKey, ofy, req, true))
	            	throw new LoginValidationException("You're not entitled to view this summary");
				ReviewDTO dto = ReadServiceImpl.getReviewSummary(summaryKey, ofy);
				if(dto.locked)
					res.getOutputStream().println(dto.getHTML());
				else
					throw new IllegalStateException("Only a submitted summary can be viewed in this panel. Attempted generating report for an" +
							" unlocked summary: " + summaryKey.getName());
				res.flushBuffer();			
			}
			else if(serviceType.equals(PanelServiceConstants.PARAM_FETCH_REVIEW_4_SUMMARIZER))
			{
				String reviewId = req.getParameter(PanelServiceConstants.PARAM_REVIEW_ID);
				
				if(reviewId == null)
					throw new IllegalArgumentException("Unable to determine review id, aborting request");				
				
				Key<InetReview> reviewKey = ofy.getFactory().stringToKey(reviewId);
			
				Key<InetEmployee> empKey = LoginPortal.getEmployeeKey(req);
				boolean isSuperUser = LoginPortal.getLoginInfo(req).isAdmin();
	            if(!isSuperUser && !checkSummarizerEligibility(empKey, reviewKey, ofy))
	            	throw new LoginValidationException("You're not entitled to view this review as a summarizer");
				ReviewDTO dto = ReadServiceImpl.getReview(reviewKey, ofy);
				if(dto.locked)
					res.getOutputStream().println(dto.getHTML());
				else
					throw new IllegalStateException("Attempted generating report for an unlocked review");			
			}
			else if(serviceType.equals(PanelServiceConstants.PARAM_FETCH_QUESTION))
			{
				String questionId = req.getParameter(PanelServiceConstants.PARAM_QUESTION_ID);
				Key<InetQuestions> questKey = ofy.getFactory().stringToKey(questionId);
				LoginPortal.getEmployeeKey(req); //validate user logged in
				InetQuestions quest = ofy.find(questKey);
				if(quest == null)
				{
					String msgFmt = "Unable to find review questions for: ";
					log.severe(msgFmt + questKey);
					throw new MissingEntitiesException(msgFmt + questKey.getName()); 
				}
				
				InetMultiAnswers ans = ofy.find(quest.getAnswerKey());
				if(ans == null)
				{
					String msgFmt = "Unable to find answer mapping for: ";
					log.severe(msgFmt + quest.getAnswerKey());
					throw new MissingEntitiesException(msgFmt + quest.getAnswerKey().getName()); 
				}
				
				ArrayList<Integer> sortedQuestionKeySet = new ArrayList<Integer>();
				HashMap<Integer, String> questSet = quest.getQuestionSet();
				sortedQuestionKeySet.addAll(questSet.keySet());
				Collections.sort(sortedQuestionKeySet);
				
				ArrayList<Integer>sortedAnswerKeySet = new ArrayList<Integer>();
				HashMap<Integer, String> ansMap = ans.getAnswerMapping();
				sortedAnswerKeySet.addAll(ansMap.keySet());
				Collections.sort(sortedAnswerKeySet);
					
					
				//build answer div
				StringBuilder output = new StringBuilder();
				output.append("<tr>");
				for(int i : sortedAnswerKeySet)
					output.append("<td style='border: 1px solid black; border-spacing: 10px 2px'>").append(ansMap.get(i)).append("</td>");
				output.append("</tr>");
				String ansString = output.toString();
				
				
				//build questions
				output = new StringBuilder();
				boolean firstText = true;
				output.append("<H3>").append(questKey.getName()).append("</H3><HR>");
				output.append("<b><p>Review Of:</p><p>Reviewer:</p><p>Written for reviewer by: </p></b>");
				output.append("<hr><i>Multichoice Questions</i>");
				for(int i : sortedQuestionKeySet)
				{
					if(i <= PanelServiceConstants.FREE_FORM_MARKER)
					{
						output.append("<table style='margin-top: 10px' width='70%'><tr><td colspan='").append(ansMap.size()).append("'><b>").
							append(i).append(". ").append(questSet.get(i)).append("</b></td></tr>");
						output.append(ansString).append("</table>");
					}
					else
					{
						if(firstText)
						{
							firstText = false;
							output.append("<hr><i>Text Questions</i>");
							output.append("<table>");
						}
						output.append("<tr><td style='border-spacing: 1px 75px'><b>").
						append(i - PanelServiceConstants.FREE_FORM_MARKER).append(". ").append(questSet.get(i)).append("</b></td></tr>");
					}
				}
				if(!firstText)
					output.append("</table>");
				res.getOutputStream().println(output.toString());			
			}
		}
		catch(Exception ex)
		{
			res.getOutputStream().println("<center><H3>An Error Occurred</H3></center><HR><font color='red'><b>" + 
					ex.getMessage() + "</b></font>");
			log.warning(ex.getMessage());
			boolean showAll = (ex instanceof LoginValidationException || ex instanceof MissingEntitiesException ||
					           ex instanceof DuplicateEntitiesException) ? false : true;
			GeneralFuncs.printStackTrace(ex, log, showAll);
		}
		res.getOutputStream().println("</div></body></html>");
	}
}
