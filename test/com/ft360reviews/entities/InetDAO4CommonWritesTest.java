package com.ft360reviews.entities;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.HashMap;
import java.util.HashSet;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import com.ft360reviews.gui.shared.PanelServiceConstants;
import com.ft360reviews.gui.shared.exceptions.DuplicateEntitiesException;
import com.ft360reviews.gui.shared.exceptions.MissingEntitiesException;
import com.google.appengine.api.datastore.Text;
import com.google.appengine.tools.development.testing.LocalDatastoreServiceTestConfig;
import com.google.appengine.tools.development.testing.LocalServiceTestHelper;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.Objectify;
import com.googlecode.objectify.ObjectifyService;

public class InetDAO4CommonWritesTest {
    private static final LocalServiceTestHelper helper = new LocalServiceTestHelper(
            new LocalDatastoreServiceTestConfig());
    private static Objectify ofy;
    
    static Key<InetMultiAnswers> ansKey;
    static Key<InetQuestions> questKey;
    static Key<InetEmployee> empl1, empl2;
    
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
        helper.setUp();
        ofy = ObjectifyService.begin();
        InetDAO4CommonReads.registerClassesWithObjectify();
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		helper.tearDown();
	}

	@Test
	public void testCreateCompany() throws DuplicateEntitiesException
	{		
		HashSet<String> phoneNumbers = new HashSet<String>();
		phoneNumbers.add("01234890");
		phoneNumbers.add("89023455");
		InetCompany c = InetDAO4CommonWrites.createCompany("ABC Industries", "ABC", "info@bac.com", "bac.com", "new street boiz", phoneNumbers);
		assertTrue(c != null);
	}
	
	@Test(expected=DuplicateEntitiesException.class)
	public void testCreateCompany2() throws DuplicateEntitiesException
	{
		InetCompany d = InetDAO4CommonWrites.createCompany("Yankee Software", "YS", null, null, "", null);
	}	
	
	@Test
	public void testCreateEmployee() throws DuplicateEntitiesException
	{
		InetEmployee e = InetDAO4CommonWrites.createEmployee("segzy", "sobzy", "seg.sob@now.com", null, 
				"employee213", "12345");
		empl1 = e.getKey();
		assertTrue(e != null);
	}
	
	@Test(expected=DuplicateEntitiesException.class)
	public void testCreateEmployee2() throws DuplicateEntitiesException
	{
		InetEmployee e2 = InetDAO4CommonWrites.createEmployee("nice", "guy", "n.guy@now.com", null, "emp123", "3456");
		empl2 = e2.getKey();
		InetEmployee e = InetDAO4CommonWrites.createEmployee("dami", "luzy", "d.luz@now.com", null, 
				"employee213", "12345");
	}	
	
	@Test
	public void testReadData()
	{
		InetCompany c = ofy.find(InetCompany.class, InetConstants.COMPANY_ID);
		InetEmployee e = ofy.find(InetEmployee.class, "employee213");
		assertTrue(c != null);
		assertTrue(e != null);
	}
	
	@Test
	public void testCreateMultiAnswer() throws DuplicateEntitiesException
	{
		HashMap<Integer, String> answerMapping = new HashMap<Integer, String>();
		answerMapping.put(1, "poor");
		answerMapping.put(2, "average");
		answerMapping.put(3, "great");
		InetMultiAnswers ans = InetDAO4CommonWrites.createMultiAnswers("abcranking", answerMapping);
		assertTrue(ans != null);
	}
	
	@Test
	public void testReadMultiAnswer()
	{
		InetMultiAnswers a = ofy.find(new Key<InetMultiAnswers>(InetMultiAnswers.class, "abcranking"));
		assertTrue(a != null);
		assertTrue(a.getAnswerMapping().containsKey(2));
		assertTrue(!a.getAnswerMapping().containsKey(5));
		ansKey = a.getKey();
	}
	
	@Test
	public void testCreateQuestions() throws DuplicateEntitiesException
	{
		HashMap<Integer, String> questionSet = new HashMap<Integer, String>();
		questionSet.put(1, "Does employee fall asleep at desk?");
		questionSet.put(2, "How loudly does employee snore");
		questionSet.put(PanelServiceConstants.FREE_FORM_MARKER + 1, "Please explain how employee falls asleep");
		InetQuestions q = InetDAO4CommonWrites.createQuestions("abcquestions", questionSet, ansKey);
		questKey = q.getKey();
		assertTrue(questKey != null);
	}
	
	@Test
	public void testReadQuestions()
	{
		System.out.println("quest key is: " + questKey);
		InetQuestions q = ofy.find(questKey);
		assertTrue(q != null);
		assertTrue(q.getQuestionSet().containsKey(PanelServiceConstants.FREE_FORM_MARKER + 1));
		assertEquals(q.getAnswerKey(), ansKey);
	}
	
	static Key<InetReview> reviewKey;
	@Test
	public void testCreateReview() throws DuplicateEntitiesException
	{
		InetReview r = InetDAO4CommonWrites.createReview("review 2010", empl1, empl2, questKey);
		assertTrue(r != null); 
		reviewKey = (Key<InetReview>) r.getKey();
	}
	
	@Test
	public void updateReview() throws MissingEntitiesException, DuplicateEntitiesException
	{
		HashMap<Integer, Integer> multichoiceAnswers = new HashMap<Integer, Integer>();
		multichoiceAnswers.put(1, 1);
		multichoiceAnswers.put(2, 1);
		HashMap<Integer, Text> textAnswers= new HashMap<Integer, Text>();
		textAnswers.put(PanelServiceConstants.FREE_FORM_MARKER + 1, new Text("Cool dude rocks yo"));
		InetDAO4CommonWrites.updateReview(reviewKey, null, multichoiceAnswers, textAnswers, false, "Pending");
	}
	
	@Test
	public void readReview()
	{
		InetReview r = ofy.find(reviewKey);
		assertTrue(r != null);
		assertTrue(!r.isLocked());
		HashMap<Integer, Integer> ans = r.getMulitchoiceAnswers();
		assertTrue(ans.containsKey(2));
		assertTrue(r.getTextAnswers().containsKey(PanelServiceConstants.FREE_FORM_MARKER + 1));
	}
}
